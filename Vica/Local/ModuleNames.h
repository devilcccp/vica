#pragma once

#include <Lib/Include/Names.h>
#include <LibV/Include/ModuleNames.h>

DefineModuleName(Portal, portal);
DefineModuleName(Reporter, report);
DefineModuleName(Unite, unite);

DefineModuleName(VideoA, va);

#define DAEMONS_NAMES_LIST QStringList() << kServerDaemon << kUpdateDaemon
#define DAEMONS_EXE_LIST QStringList() << kServerDaemonExe << kUpdateDaemonExe

inline const QString GetUpdateViewName()    { return QString::fromUtf8("VICA Обновления"); }
inline const QString GetUpdateDescription() { return QString::fromUtf8("Сервис VICA, осуществляющий контроль работы компонент системы обновлений"); }

inline const QString GetArmViewName()    { return QString::fromUtf8("АРМ VICA"); }
inline const QString GetArmDescription() { return QString::fromUtf8("Сервис VICA, осуществляющий контроль работы компонент АРМ"); }
inline const QString GetAdminName()      { return QString::fromUtf8("АРМ администратора VICA"); }

inline const QString GetServerViewName()    { return QString::fromUtf8("VICA Сервер"); }
inline const QString GetServerDescription() { return QString::fromUtf8("Сервис VICA, осуществляющий контроль работы компонент интеллектуального видеосервера"); }
