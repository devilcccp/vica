-- AUTO generated script --

-- Table: event_type

-- DROP TABLE event_type;

CREATE TABLE event_type
(
  _id serial NOT NULL,
  name text NOT NULL,
  descr text NOT NULL,
  icon text,
  flag integer NOT NULL DEFAULT 1,
  CONSTRAINT event_type_pkey PRIMARY KEY (_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_type
  OWNER TO su;

-- Index: event_type_name_idx

-- DROP INDEX event_type_name_idx;

CREATE INDEX event_type_name_idx
  ON event_type
  USING btree
  (name COLLATE pg_catalog."default");

-- Constraint: event_type_name_key

-- ALTER TABLE event_type DROP CONSTRAINT event_type_name_key;

ALTER TABLE event_type
  ADD CONSTRAINT event_type_name_key UNIQUE(name);
-- Table: object_state_type

-- DROP TABLE object_state_type;

CREATE TABLE object_state_type
(
  _id serial NOT NULL,
  name text NOT NULL,
  descr text,
  CONSTRAINT object_state_type_pkey PRIMARY KEY (_id),
  CONSTRAINT object_state_type_name_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_state_type
  OWNER TO su;

-- Index: object_state_type_name_idx

-- DROP INDEX object_state_type_name_idx;

CREATE INDEX object_state_type_name_idx
  ON object_state_type
  USING btree
  (name COLLATE pg_catalog."default");

GRANT ALL ON TABLE object_state_type TO su;
GRANT SELECT ON TABLE object_state_type TO usr;

-- Constraint: object_state_type_name_key

-- ALTER TABLE object_state_type DROP CONSTRAINT object_state_type_name_key;

-- ALTER TABLE object_state_type
--  ADD CONSTRAINT object_state_type_name_key UNIQUE(name);
-- Table: object_state_values

-- DROP TABLE object_state_values;

CREATE TABLE object_state_values
(
  _id serial NOT NULL,
  _ostype integer NOT NULL,
  state integer NOT NULL,
  descr text NOT NULL,
  color text NOT NULL,
  CONSTRAINT object_state_values_pkey PRIMARY KEY (_id),
  CONSTRAINT object_state_values__ostype_fkey FOREIGN KEY (_ostype)
      REFERENCES object_state_type (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_state_values
  OWNER TO su;

-- Index: object_state_values_name_idx

-- DROP INDEX object_state_values_name_idx;

CREATE INDEX object_state_values_name_idx
  ON object_state_values
  USING btree
  (_ostype, state);

GRANT ALL ON TABLE object_state_values TO su;
GRANT SELECT ON TABLE object_state_values TO usr;
-- Table: object_type

-- DROP TABLE object_type;

CREATE TABLE object_type
(
  _id serial NOT NULL,
  _odefault integer,
  name text NOT NULL,
  descr text,
  CONSTRAINT object_type_pkey PRIMARY KEY (_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_type
  OWNER TO su;

-- Index: object_type_name_idx

-- DROP INDEX object_type_name_idx;

CREATE INDEX object_type_name_idx
  ON object_type
  USING btree
  (name COLLATE pg_catalog."default");

GRANT ALL ON TABLE object_type TO su;
GRANT SELECT ON TABLE object_type TO usr;

-- Constraint: object_type_name_key

-- ALTER TABLE object_type DROP CONSTRAINT object_type_name_key;

ALTER TABLE object_type
  ADD CONSTRAINT object_type_name_key UNIQUE(name);

-- Table: object

-- DROP TABLE object CASCADE;

CREATE TABLE object
(
  _id serial NOT NULL,
  _otype integer NOT NULL,
  _parent integer,
  guid text NOT NULL,
  name text NOT NULL,
  descr text,
  version text,
  revision integer DEFAULT 1,
  uri text,
  status integer DEFAULT 0,
  CONSTRAINT object_pkey PRIMARY KEY (_id),
  CONSTRAINT object__otype_fkey FOREIGN KEY (_otype)
      REFERENCES object_type (_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT object__parent_fkey FOREIGN KEY (_parent)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT object__otype_guid_key UNIQUE (guid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object
  OWNER TO su;

-- Index: object__otype_idx

-- DROP INDEX object__otype_idx;

CREATE INDEX object__otype_idx
  ON object
  USING btree
  (_otype);

-- Index: object_guid_idx

-- DROP INDEX object_guid_idx;

CREATE INDEX object_guid_idx
  ON object
  USING btree
  (guid COLLATE pg_catalog."default");

ALTER TABLE object_type ADD
CONSTRAINT object_type__odefault_fkey FOREIGN KEY (_odefault)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

GRANT ALL ON TABLE object TO su;
GRANT SELECT ON TABLE object TO usr;
-- Table: event

-- DROP TABLE event;

CREATE TABLE event
(
  _id serial NOT NULL,
  _object integer NOT NULL,
  _etype integer NOT NULL,
  CONSTRAINT event_pkey PRIMARY KEY (_id),
  CONSTRAINT event__etype_fkey FOREIGN KEY (_etype)
      REFERENCES event_type (_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT event__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event
  OWNER TO su;

-- Index: event__object_idx

-- DROP INDEX event__object_idx;

CREATE INDEX event__object_idx
  ON event
  USING btree
  (_object);

-- Table: event_log

-- DROP TABLE event_log;

CREATE TABLE event_log
(
  _id bigserial NOT NULL,
  _event integer NOT NULL,
  triggered_time timestamp with time zone NOT NULL,
  value real NOT NULL DEFAULT 1,
  info text,
  CONSTRAINT event_log_pkey PRIMARY KEY (_id),
  CONSTRAINT event_log__event_fkey FOREIGN KEY (_event)
      REFERENCES event (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_log
  OWNER TO su;

-- Index: event_log__event_triggered_time_idx

-- DROP INDEX event_log__event_triggered_time_idx;

CREATE INDEX event_log__event_triggered_time_idx
  ON event_log
  USING btree
  (_event, triggered_time);

-- Index: event_log_triggered_time_idx

-- DROP INDEX event_log_triggered_time_idx;

CREATE INDEX event_log_triggered_time_idx
  ON event_log
  USING btree
  (triggered_time);

-- Table: event_log_hours

-- DROP TABLE event_log_hours;

CREATE TABLE event_log_hours
(
  _id bigserial NOT NULL,
  _event integer NOT NULL,
  triggered_hour timestamp with time zone NOT NULL,
  value real NOT NULL DEFAULT 0,
  CONSTRAINT event_log_hours_pkey PRIMARY KEY (_id),
  CONSTRAINT event_log_hours__event_fkey FOREIGN KEY (_event)
      REFERENCES event (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_log_hours
  OWNER TO su;

-- Index: event_log_hours__event_triggered_hour_idx

-- DROP INDEX event_log_hours__event_triggered_hour_idx;

CREATE INDEX event_log_hours__event_triggered_hour_idx
  ON event_log_hours
  USING btree
  (_event, triggered_hour);

-- Index: event_log_hours_triggered_hour_idx

-- DROP INDEX event_log_hours_triggered_hour_idx;

CREATE INDEX event_log_hours_triggered_hour_idx
  ON event_log_hours
  USING btree
  (triggered_hour);

CREATE OR REPLACE FUNCTION event_log_insert_post() RETURNS trigger AS $BODY$
DECLARE
  triggered_hour_ timestamp with time zone;
  id_ bigint;
BEGIN
  triggered_hour_ := date_trunc('hours', NEW.triggered_time);
  SELECT _id FROM event_log_hours WHERE triggered_hour = triggered_hour_ AND _event = NEW._event INTO id_;
  IF (id_ IS NULL) THEN
    INSERT INTO event_log_hours (_event, triggered_hour, value)
      VALUES (NEW._event, triggered_hour_, NEW.value);
  ELSE
    UPDATE event_log_hours SET value = value + NEW.value WHERE _id = id_;
  END IF;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION event_log_insert_post()
  OWNER TO su;
---
CREATE TRIGGER event_log_insert_post AFTER INSERT ON event_log
  FOR EACH ROW EXECUTE PROCEDURE event_log_insert_post();
-- Table: event_stat

-- DROP TABLE event_stat;

CREATE TABLE event_stat
(
  _id serial NOT NULL,
  _event integer NOT NULL,
  triggered_time timestamp with time zone NOT NULL DEFAULT now(),
  value real NOT NULL DEFAULT 0,
  period integer NOT NULL DEFAULT 0,
  CONSTRAINT event_stat_pkey PRIMARY KEY (_id),
  CONSTRAINT event_stat__event_fkey FOREIGN KEY (_event)
      REFERENCES event (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT event_stat__event_key UNIQUE (_event)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_stat
  OWNER TO su;

-- Index: event_stat__event_triggered_time_idx

-- DROP INDEX event_stat__event_triggered_time_idx;

CREATE INDEX event_stat__event_triggered_time_idx
  ON event_stat
  USING btree
  (_event, triggered_time);

-- Index: event_stat_triggered_time_idx

-- DROP INDEX event_stat_triggered_time_idx;

CREATE INDEX event_stat_triggered_time_idx
  ON event_stat
  USING btree
  (triggered_time);

-- Table: event_stat_hours

-- DROP TABLE event_stat_hours;

CREATE TABLE event_stat_hours
(
  _id bigserial NOT NULL,
  _event integer NOT NULL,
  triggered_hour timestamp with time zone NOT NULL,
  value real NOT NULL DEFAULT 0,
  period integer NOT NULL DEFAULT 0,
  CONSTRAINT event_stat_hours_pkey PRIMARY KEY (_id),
  CONSTRAINT event_stat_hours__event_fkey FOREIGN KEY (_event)
      REFERENCES event (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_stat_hours
  OWNER TO su;

-- Index: event_stat_hours__event_triggered_hour_idx

-- DROP INDEX event_stat_hours__event_triggered_hour_idx;

CREATE INDEX event_stat_hours__event_triggered_hour_idx
  ON event_stat_hours
  USING btree
  (_event, triggered_hour);

-- Index: event_stat_hours_triggered_hour_idx

-- DROP INDEX event_stat_hours_triggered_hour_idx;

CREATE INDEX event_stat_hours_triggered_hour_idx
  ON event_stat_hours
  USING btree
  (triggered_hour);

CREATE OR REPLACE FUNCTION event_stat_update_post() RETURNS trigger AS $BODY$
DECLARE
  triggered_hour_ timestamp with time zone;
  id_ bigint;
BEGIN
  triggered_hour_ := date_trunc('hours', NEW.triggered_time);
  SELECT _id FROM event_stat_hours WHERE triggered_hour = triggered_hour_ AND _event = NEW._event INTO id_;
  IF (id_ IS NULL) THEN
    INSERT INTO event_stat_hours (_event, triggered_hour, value, period)
      VALUES (NEW._event, triggered_hour_, NEW.value, NEW.period);
  ELSE
    UPDATE event_stat_hours SET value = value + NEW.value, period = period + NEW.period 
      WHERE _id = id_;
  END IF;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION event_stat_update_post()
  OWNER TO su;
---
CREATE TRIGGER event_stat_update_post AFTER UPDATE ON event_stat
  FOR EACH ROW EXECUTE PROCEDURE event_stat_update_post();
-- Table: files

-- DROP TABLE files;

CREATE TABLE files
(
  _id bigserial NOT NULL,
  _object integer,
  name text,
  mime_type text,
  data bytea,
  CONSTRAINT files_pkey PRIMARY KEY (_id),
  CONSTRAINT files__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE files
  OWNER TO su;

-- Index: files__object_idx

-- DROP INDEX files__object_idx;

CREATE INDEX files__object_idx
  ON files
  USING btree
  (_object);

-- Table: object_connection

-- DROP TABLE object_connection;

CREATE TABLE object_connection
(
  _id serial NOT NULL,
  _omaster integer,
  _oslave integer,
  type integer,
  CONSTRAINT object_connection_pkey PRIMARY KEY (_id),
  CONSTRAINT object_connection__omaster_fkey FOREIGN KEY (_omaster)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT object_connection__oslave_fkey FOREIGN KEY (_oslave)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_connection
  OWNER TO su;

-- Index: object_connection__omaster_idx

-- DROP INDEX object_connection__omaster_idx;

CREATE INDEX object_connection__omaster_idx
  ON object_connection
  USING btree
  (_omaster);

-- Index: object_connection__oslave_idx

-- DROP INDEX object_connection__oslave_idx;

CREATE INDEX object_connection__oslave_idx
  ON object_connection
  USING btree
  (_oslave);

GRANT ALL ON TABLE object_connection TO su;
GRANT SELECT ON TABLE object_connection TO usr;
CREATE OR REPLACE FUNCTION object_connection_change_post() RETURNS trigger AS $BODY$
BEGIN
  IF NEW._omaster = NEW._oslave THEN
    RAISE EXCEPTION 'object_connection recursion not allowed';
  END IF;
  UPDATE object o SET revision = o.revision + 1 WHERE o._id = NEW._omaster;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_connection_change_post()
  OWNER TO su;
---
CREATE TRIGGER object_connection_change_post AFTER INSERT OR UPDATE ON object_connection
  FOR EACH ROW EXECUTE PROCEDURE object_connection_change_post();

---
CREATE OR REPLACE FUNCTION object_connection_delete_post() RETURNS trigger AS $BODY$
BEGIN
  UPDATE object o SET revision = o.revision + 1 WHERE o._id = OLD._omaster;
  RETURN OLD;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_connection_delete_post()
  OWNER TO su;
---
CREATE TRIGGER object_connection_delete_post AFTER DELETE ON object_connection
  FOR EACH ROW EXECUTE PROCEDURE object_connection_delete_post();
-- Table: object_log

-- DROP TABLE IF EXISTS object_log;

CREATE TABLE object_log
(
  _id bigserial,
  _object integer NOT NULL,
  period_start timestamp with time zone NOT NULL,
  period_end timestamp with time zone NOT NULL,
  thread_name text,
  work_name text,
  total_time integer DEFAULT 1,
  circles integer DEFAULT 0,
  work_time integer DEFAULT 0,
  longest_work integer DEFAULT 0,
  CONSTRAINT object_log_pkey PRIMARY KEY (_id),
  CONSTRAINT object_log__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_log
  OWNER TO su;
GRANT ALL ON TABLE object_log TO su;
GRANT SELECT, UPDATE ON TABLE object_log TO usr;

-- Index: object_log__object_period_start_idx

-- DROP INDEX object_log__object_period_start_idx;

CREATE INDEX object_log__object_period_start_idx
  ON object_log
  USING btree
  (_object, period_start);
-- Table: object_log_hours

-- DROP TABLE IF EXISTS object_log_hours;

CREATE TABLE object_log_hours
(
  _id bigserial,
  _object integer NOT NULL,
  period_hour timestamp with time zone NOT NULL,
  thread_name text,
  work_name text,
  total_time integer DEFAULT 1,
  circles integer DEFAULT 0,
  work_time integer DEFAULT 0,
  longest_work integer DEFAULT 0,
  log_count integer DEFAULT 1,
  CONSTRAINT object_log_hours_pkey PRIMARY KEY (_id),
  CONSTRAINT object_log_hours__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_log_hours
  OWNER TO su;
GRANT ALL ON TABLE object_log_hours TO su;
GRANT SELECT, UPDATE ON TABLE object_log_hours TO usr;

-- Index: object_log_hours__object_period_hour_idx

-- DROP INDEX object_log_hours__object_period_hour_idx;

CREATE INDEX object_log_hours__object_period_hour_idx
  ON object_log_hours
  USING btree
  (_object, period_hour);
-- Table: object_log_info

-- DROP TABLE IF EXISTS object_log_info;

CREATE TABLE object_log_info
(
  _id bigserial,
  _object integer NOT NULL,
  hours_start timestamp with time zone NOT NULL,
  hours_end timestamp with time zone NOT NULL,
  last_trunc timestamp with time zone,
  last_clean timestamp with time zone,
  CONSTRAINT object_log_info_pkey PRIMARY KEY (_id),
  CONSTRAINT object_log_info__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT object_log_info__object_key UNIQUE (_object)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_log_info
  OWNER TO su;
GRANT ALL ON TABLE object_log_info TO su;
GRANT SELECT, UPDATE ON TABLE object_log_info TO usr;

-- Index: object_log_info__object_idx

-- DROP INDEX object_log_info__object_idx;

CREATE INDEX object_log_info__object_idx
  ON object_log_info
  USING btree
  (_object);

-- Index: public.object_log_info_last_clean_idx

-- DROP INDEX public.object_log_info_last_clean_idx;

CREATE INDEX object_log_info_last_clean_idx
  ON public.object_log_info
  USING btree
  (last_clean);

-- Index: public.object_log_info_last_trunc_idx

-- DROP INDEX public.object_log_info_last_trunc_idx;

CREATE INDEX object_log_info_last_trunc_idx
  ON public.object_log_info
  USING btree
  (last_trunc);-- Table: object_settings

-- DROP TABLE object_settings;

CREATE TABLE object_settings
(
  _id serial NOT NULL,
  _object integer NOT NULL,
  key text NOT NULL,
  value text,
  CONSTRAINT object_settings_pkey PRIMARY KEY (_id),
  CONSTRAINT object_settings__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_settings
  OWNER TO su;

-- Index: object_settings__object_idx
-- DROP INDEX object_settings__object_idx;
CREATE INDEX object_settings__object_idx
  ON object_settings
  USING btree
  (_object, key);

GRANT ALL ON TABLE object_settings TO su;
GRANT SELECT ON TABLE object_settings TO usr;
CREATE OR REPLACE FUNCTION object_settings_change_post() RETURNS trigger AS $BODY$
BEGIN
  UPDATE object SET revision = revision + 1 WHERE _id = NEW._object;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_settings_change_post()
  OWNER TO su;
---
CREATE TRIGGER object_settings_change_post AFTER INSERT OR UPDATE ON object_settings
  FOR EACH ROW EXECUTE PROCEDURE object_settings_change_post();
-- Table: object_settings_type

-- DROP TABLE object_settings_type;

CREATE TABLE object_settings_type
(
  _id serial NOT NULL,
  _otype integer,
  key text,
  name text,
  descr text,
  type text,
  min_value text,
  max_value text,
  CONSTRAINT object_settings_type_pkey PRIMARY KEY (_id),
  CONSTRAINT object_settings_type__otype_fkey FOREIGN KEY (_otype)
      REFERENCES object_type (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_settings_type
  OWNER TO su;

-- Index: object_settings_type__otype_key_idx

-- DROP INDEX object_settings_type__otype_key_idx;

CREATE INDEX object_settings_type__otype_key_idx
  ON object_settings_type
  USING btree
  (_otype, key COLLATE pg_catalog."default");

GRANT ALL ON TABLE object_settings_type TO su;
GRANT SELECT ON TABLE object_settings_type TO usr;
-- Table: object_state

-- DROP TABLE object_state CASCADE;

CREATE TABLE object_state
(
  _id serial NOT NULL,
  _object integer NOT NULL,
  _ostype integer NOT NULL,
  state integer DEFAULT 0,
  change_time timestamp with time zone NOT NULL,
  CONSTRAINT object_state_pkey PRIMARY KEY (_id),
  CONSTRAINT object_state__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT object_state__ostype_fkey FOREIGN KEY (_ostype)
      REFERENCES object_state_type (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_state
  OWNER TO su;

-- Index: object_state__object__ostype_idx

-- DROP INDEX object_state__object__ostype_idx;

CREATE INDEX object_state__object__ostype_idx
  ON object_state
  USING btree
  (_object, _ostype);

GRANT ALL ON TABLE object_state TO su;
GRANT SELECT, UPDATE ON TABLE object_state TO usr;
-- Table: object_state_log

-- DROP TABLE object_state_log;

CREATE TABLE object_state_log
(
  _id bigserial NOT NULL,
  _ostate integer NOT NULL,
  old_state integer NOT NULL,
  new_state integer NOT NULL,
  change_time timestamp with time zone NOT NULL,
  CONSTRAINT object_state_log_pkey PRIMARY KEY (_id),
  CONSTRAINT object_state_log__ostate_fkey FOREIGN KEY (_ostate)
      REFERENCES object_state (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_state_log
  OWNER TO su;

-- Index: object_state_log__ostate_change_time_idx

-- DROP INDEX object_state_log__ostate_change_time_idx;

CREATE INDEX object_state_log__ostate_change_time_idx
  ON object_state_log
  USING btree
  (_ostate, change_time);


-- Index: object_state_log_change_time_idx

-- DROP INDEX object_state_log_change_time_idx;

CREATE INDEX object_state_log_change_time_idx
  ON object_state_log
  USING btree
  (change_time);

GRANT ALL ON TABLE object_state_log TO su;
GRANT SELECT, UPDATE, INSERT ON TABLE object_state_log TO usr;
--DROP TRIGGER IF EXISTS object_state_update_post ON object_state;
--DROP FUNCTION object_state_update_post();
---
CREATE OR REPLACE FUNCTION object_state_update_post() RETURNS trigger AS $BODY$
BEGIN
  IF (OLD.state <> NEW.state) THEN
    INSERT INTO object_state_log (_ostate, old_state, new_state, change_time)
     VALUES (NEW._id, OLD.state, NEW.state, NEW.change_time);
  END IF;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_state_update_post()
  OWNER TO su;
--GRANT EXECUTE ON FUNCTION object_state_update_post(integer, integer, integer, integer) TO usr;
---
CREATE TRIGGER object_state_update_post AFTER UPDATE ON object_state
  FOR EACH ROW EXECUTE PROCEDURE object_state_update_post();
---
/*---
DROP TRIGGER IF EXISTS object_state_add_post ON object_state;
DROP FUNCTION object_state_add_post();
---
CREATE OR REPLACE FUNCTION object_state_add_post() RETURNS trigger AS $BODY$
BEGIN
  INSERT INTO object_state_log (_ostate, old_state, new_state, change_time)
   VALUES (NEW._id, NEW.state, NEW.state, NOW());
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_state_add_post()
  OWNER TO su;
---
CREATE TRIGGER object_state_add_post AFTER INSERT ON object_state
  FOR EACH ROW EXECUTE PROCEDURE object_state_add_post();
---
*/
CREATE OR REPLACE FUNCTION object_change_post() RETURNS trigger AS $BODY$
BEGIN
  UPDATE object o SET revision = o.revision + 1
    FROM object_connection c
    WHERE o._id = c._omaster AND c._oslave = NEW._id;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_change_post()
  OWNER TO su;
---
CREATE TRIGGER object_change_post AFTER INSERT OR UPDATE ON object
  FOR EACH ROW EXECUTE PROCEDURE object_change_post();
-- Table: report

-- DROP TABLE report;

CREATE TABLE report
(
  _id bigserial NOT NULL,
  _object integer NOT NULL,
  type integer,
  period_begin timestamp with time zone,
  period_end timestamp with time zone,
  data bytea,
  CONSTRAINT report_pkey PRIMARY KEY (_id),
  CONSTRAINT report__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE report
  OWNER TO su;

-- Index: report__object_idx

-- DROP INDEX report__object_idx;

CREATE INDEX report__object_idx
  ON report
  USING btree
  (_object);

-- Index: report__object_type_period_begin_idx

-- DROP INDEX report__object_type_period_begin_idx;

CREATE INDEX report__object_type_period_begin_idx
  ON report
  USING btree
  (_object, type, period_begin);

-- Table: report_files

-- DROP TABLE report_files;

CREATE TABLE report_files
(
  _id bigserial NOT NULL,
  _report bigint,
  _files bigint,
  CONSTRAINT report_files_pkey PRIMARY KEY (_id),
  CONSTRAINT report_files__report_fkey FOREIGN KEY (_report)
      REFERENCES report (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT report_files__files_fkey FOREIGN KEY (_files)
      REFERENCES files (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE report_files
  OWNER TO su;


CREATE INDEX report_files__report_idx
  ON report_files
  USING btree
  (_report);

CREATE INDEX report_files__files_idx
  ON report_files
  USING btree
  (_files);

-- Table: report_send

-- DROP TABLE report_send;

CREATE TABLE report_send
(
  _id bigserial NOT NULL,
  _oto integer NOT NULL,
  _last_report integer NOT NULL,
  send_time timestamp with time zone,
  CONSTRAINT report_send_pkey PRIMARY KEY (_id),
  CONSTRAINT report_send__oto_fkey FOREIGN KEY (_oto)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT report_send__last_report_fkey FOREIGN KEY (_last_report)
      REFERENCES report (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT report_send__oto_key UNIQUE (_oto)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE report_send
  OWNER TO su;


-- Index: report_send__oto_idx

-- DROP INDEX report_send__oto_idx;

CREATE INDEX report_send__oto_idx
  ON report_send
  USING btree
  (_oto);

-- Table: variables

-- DROP TABLE variables;

CREATE TABLE variables
(
  _id bigserial NOT NULL,
  _object integer,
  key text,
  value text,
  CONSTRAINT variables_pkey PRIMARY KEY (_id),
  CONSTRAINT variables__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE variables
  OWNER TO su;
GRANT ALL ON TABLE variables TO su;
GRANT SELECT ON TABLE variables TO usr;

-- Index: variables__null_key_key

-- DROP INDEX variables__null_key_key;

CREATE UNIQUE INDEX variables__null_key_key
  ON variables
  USING btree
  (key COLLATE pg_catalog."default")
  WHERE _object IS NULL;

-- Index: variables__object_key_key

-- DROP INDEX variables__object_key_key;

CREATE UNIQUE INDEX variables__object_key_key
  ON variables
  USING btree
  (_object, key COLLATE pg_catalog."default")
  WHERE _object IS NOT NULL;

-- Table: va_stat

-- DROP TABLE va_stat;

CREATE TABLE va_stat
(
  _id serial NOT NULL,
  _object integer NOT NULL,
  _vstype integer NOT NULL,
  CONSTRAINT va_stat_pkey PRIMARY KEY (_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE va_stat
  OWNER TO su;


-- Index: va_stat__vstype__object_idx

-- DROP INDEX va_stat__vstype__object_idx;

CREATE INDEX va_stat__vstype__object_idx
  ON va_stat
  USING btree
  (_object, _vstype);

-- Table: va_stat_days

-- DROP TABLE va_stat_days;

CREATE TABLE va_stat_days
(
  _id bigserial NOT NULL,
  _vstat integer NOT NULL,
  _fimage bigint NOT NULL,
  day timestamp with time zone NOT NULL,
  CONSTRAINT va_stat_days_pkey PRIMARY KEY (_id),
  CONSTRAINT va_stat_days__vstat_fkey FOREIGN KEY (_vstat)
      REFERENCES va_stat (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT va_stat_days__fimage_fkey FOREIGN KEY (_fimage)
      REFERENCES files (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE va_stat_days
  OWNER TO su;

-- Index: va_stat_days__object_day_idx

-- DROP INDEX va_stat_days__object_day_idx;

CREATE INDEX va_stat_days__object_day_idx
  ON va_stat_days
  USING btree
  (_vstat, day);

-- Index: va_stat_days_day_idx

-- DROP INDEX va_stat_days_day_idx;

CREATE INDEX va_stat_days_day_idx
  ON va_stat_days
  USING btree
  (day);

-- Table: va_stat_hours

-- DROP TABLE va_stat_hours;

CREATE TABLE va_stat_hours
(
  _id bigserial NOT NULL,
  _vstat integer NOT NULL,
  _fimage bigint NOT NULL,
  hour timestamp with time zone NOT NULL,
  CONSTRAINT va_stat_hours_pkey PRIMARY KEY (_id),
  CONSTRAINT va_stat_hours__vstat_fkey FOREIGN KEY (_vstat)
      REFERENCES va_stat (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT va_stat_hours__fimage_fkey FOREIGN KEY (_fimage)
      REFERENCES files (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE va_stat_hours
  OWNER TO su;

-- Index: va_stat_hours__object_hour_idx

-- DROP INDEX va_stat_hours__object_hour_idx;

CREATE INDEX va_stat_hours__object_hour_idx
  ON va_stat_hours
  USING btree
  (_vstat, hour);

-- Index: va_stat_hours_hour_idx

-- DROP INDEX va_stat_hours_hour_idx;

CREATE INDEX va_stat_hours_hour_idx
  ON va_stat_hours
  USING btree
  (hour);

-- Table: va_stat_type

-- DROP TABLE va_stat_type;

CREATE TABLE va_stat_type
(
  _id serial NOT NULL,
  abbr text NOT NULL,
  name text NOT NULL,
  descr text NOT NULL,
  CONSTRAINT va_stat_type_pkey PRIMARY KEY (_id),
  CONSTRAINT va_stat_type_abbr_key UNIQUE (abbr)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE va_stat_type
  OWNER TO su;

-- Index: va_stat_type_abbr_idx

-- DROP INDEX va_stat_type_abbr_idx;

CREATE INDEX va_stat_type_abbr_idx
  ON va_stat_type
  USING btree
  (abbr COLLATE pg_catalog."default");


