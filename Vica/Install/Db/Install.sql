-- AUTO generated Install script --

BEGIN TRANSACTION;

-- AUTO generated script --

-- Function: event_init(integer, integer)

-- DROP FUNCTION event_init(integer, integer);

CREATE OR REPLACE FUNCTION event_init(object_ integer, etype_ integer)
  RETURNS integer AS
$BODY$
DECLARE
  id_ integer;
BEGIN
  LOCK TABLE event;
  SELECT _id FROM event WHERE _object = object_ AND _etype = etype_ INTO id_;
  IF id_ IS NULL THEN
    INSERT INTO event (_object, _etype) VALUES(object_, etype_) RETURNING _id INTO id_;
  END IF;
  RETURN id_;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION event_init(integer, integer)
  OWNER TO su;
-- Function: event_stat_init(integer, integer)

-- DROP FUNCTION event_stat_init(integer, integer);

CREATE OR REPLACE FUNCTION event_stat_init(object_ integer, etype_ integer)
  RETURNS integer AS
$BODY$
DECLARE
  event_ integer;
  event_stat_ integer;
BEGIN
  SELECT _id FROM event WHERE _object = object_ AND _etype = etype_ INTO event_;
  IF event_ IS NULL THEN
    INSERT INTO event (_object, _etype) VALUES(object_, etype_) RETURNING _id INTO event_;
  END IF;
  SELECT _id FROM event_stat WHERE _event = event_ INTO event_stat_;
  IF event_stat_ IS NULL THEN
    INSERT INTO event_stat (_event) VALUES(event_) RETURNING _id INTO event_stat_;
  END IF;
  RETURN event_stat_;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION event_stat_init(integer, integer)
  OWNER TO su;
-- Function: object_complete(integer, integer)

-- DROP FUNCTION object_complete(integer, integer);

CREATE OR REPLACE FUNCTION object_complete(object_ integer, omaster_type_ integer)
  RETURNS boolean AS
$BODY$
DECLARE
  omaster_ integer;
  oslave_type_ integer;
  status_ integer;
BEGIN
  SELECT status, _otype FROM object WHERE _id = object_ INTO status_, oslave_type_;
  IF status_ IS NULL THEN
    RAISE EXCEPTION 'object_complete: object not exists';
  ELSIF status_ = 0 THEN
    RETURN false;
  ELSE
    UPDATE object SET status = 0 WHERE _id = object_;
    SELECT o._id FROM object_connection c
     INNER JOIN object o ON o._id = c._omaster
     WHERE _oslave = object_ AND o._otype = omaster_type_ INTO omaster_;
    IF omaster_ IS NOT NULL THEN
      SELECT SUM(ABS(o.status)) FROM object o INNER JOIN object_connection c ON c._oslave = o._id
       WHERE c._omaster = omaster_ AND o._otype = oslave_type_ INTO status_;
      IF status_ = 0 THEN
        UPDATE object SET status = 0 WHERE _id = omaster_;
      END IF;
    END IF;
    RETURN true;
  END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION object_complete(integer, integer)
  OWNER TO su;
--
-- Function: object_create(integer, text, text, text)

-- DROP FUNCTION object_create(integer, text, text, text);

CREATE OR REPLACE FUNCTION object_create(otemplate_ integer, guid_ text, name_ text, descr_ text)
  RETURNS integer AS
$BODY$
DECLARE
  otype_ integer;
  object_ integer;
  uri_ text;
  status_ integer;
BEGIN
  SELECT _otype, uri, status FROM object WHERE otemplate_ = _id INTO otype_, uri_, status_;
  IF otype_ IS NULL THEN
    RAISE EXCEPTION 'object_create: template not defined';
  END IF;
  INSERT INTO object(_otype, guid, name, descr, uri, status) VALUES (otype_, guid_, name_, descr_, uri_, status_) RETURNING _id INTO object_;

  INSERT INTO object_settings(_object, key, value) 
    (SELECT object_, key, value FROM object_settings WHERE _object = otemplate_ ORDER BY _id);
  RETURN object_;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION object_create(integer, text, text, text)
  OWNER TO su;
--
-- Function: object_create_slave(integer, integer, text, text, text)

-- DROP FUNCTION object_create_slave(integer, integer, text, text, text);

CREATE OR REPLACE FUNCTION object_create_slave(omaster_ integer, otemplate_ integer, guid_ text, name_ text, descr_ text)
  RETURNS integer AS
$BODY$
DECLARE
  new_id_ integer;
BEGIN
  SELECT object_create(otemplate_, guid_, name_, descr_) INTO new_id_;
  INSERT INTO object_connection(_omaster, _oslave) VALUES (omaster_, new_id_);
  UPDATE object SET _parent=omaster_ WHERE _id=new_id_;
  RETURN new_id_;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION object_create_slave(integer, integer, text, text, text)
  OWNER TO su;
--
-- Function: object_log_clean_hours(integer, integer)

-- DROP FUNCTION object_log_clean_hours(integer, integer);

CREATE OR REPLACE FUNCTION object_log_clean_hours(
    object_ integer,
    period_hours_ integer)
  RETURNS void AS
$BODY$
DECLARE
  start_period_time_ timestamp with time zone;
  end_period_time_ timestamp with time zone;
BEGIN

  SELECT hours_start FROM object_log_info 
    WHERE _object=object_ INTO start_period_time_;
  end_period_time_ := date_trunc('hours', now() - interval '1 hour' * period_hours_);

  IF (start_period_time_ IS NOT NULL) THEN
   DELETE FROM object_log_hours 
     WHERE _object=object_ AND period_hour >= start_period_time_ AND period_hour < end_period_time_;
   UPDATE object_log_info SET hours_start=end_period_time_, last_clean = now()
     WHERE _object=object_;
  END IF;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 1000;
ALTER FUNCTION object_log_clean_hours(integer, integer)
  OWNER TO su;
-- Function: object_log_trunc_hours(integer, integer)

-- DROP FUNCTION object_log_trunc_hours(integer, integer);

CREATE OR REPLACE FUNCTION object_log_trunc_hours(
    object_ integer,
    period_hours_ integer)
  RETURNS void AS
$BODY$
DECLARE
  start_period_time_ timestamp with time zone;
  end_period_time_ timestamp with time zone;
BEGIN

  SELECT hours_end FROM object_log_info 
    WHERE _object=object_ INTO start_period_time_;
  end_period_time_ := date_trunc('hours', now() - interval '1 hour' * period_hours_);

  IF (start_period_time_ IS NOT NULL) THEN
   INSERT INTO object_log_hours(_object, period_hour, thread_name, work_name, 
     total_time, circles, work_time, longest_work, log_count)
   (SELECT _object, date_trunc('hours', period_start) AS period_hour, thread_name, work_name, 
     SUM(total_time)/12, SUM(circles)/12, SUM(work_time)/12, MAX(longest_work), COUNT(_id)
     FROM object_log 
     WHERE _object=object_ AND period_start >= start_period_time_ AND period_start < end_period_time_ 
     GROUP BY _object, period_hour, thread_name, work_name
     ORDER BY period_hour, thread_name, work_name
   );
   DELETE FROM object_log 
     WHERE _object=object_ AND period_start >= start_period_time_ AND period_start < end_period_time_;
   UPDATE object_log_info 
     SET hours_end=end_period_time_, last_trunc = now() 
     WHERE _object=object_;
  ELSE
   INSERT INTO object_log_hours(_object, period_hour, thread_name, work_name, 
     total_time, circles, work_time, longest_work, log_count)
   (SELECT _object, date_trunc('hours', period_start) AS period_hour, thread_name, work_name, 
     SUM(total_time)/12, SUM(circles)/12, SUM(work_time)/12, MAX(longest_work), COUNT(_id)
     FROM object_log 
     WHERE _object=object_ AND period_start < end_period_time_ 
     GROUP BY _object, period_hour, thread_name, work_name
     ORDER BY period_hour, thread_name, work_name
   );
   DELETE FROM object_log WHERE _object=object_ AND period_start < end_period_time_;
   INSERT INTO object_log_info(_object, hours_start, hours_end)
     (SELECT _object, MIN(period_hour), MAX(period_hour) FROM object_log_hours 
        WHERE _object=object_ GROUP BY _object
     );
   UPDATE object_log_info 
     SET hours_end=end_period_time_, last_trunc = now()
     WHERE _object=object_;
  END IF;

END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 1000;
ALTER FUNCTION object_log_trunc_hours(integer, integer)
  OWNER TO su;
-- Function: object_state_init(integer, integer, integer, integer);

-- DROP FUNCTION object_state_init(integer, integer, integer, integer);

CREATE OR REPLACE FUNCTION object_state_init(object_ integer, ostype_ integer, old_state_ integer, new_state_ integer)
  RETURNS integer AS
$BODY$
DECLARE
  id_ integer;
  real_state_ integer;
  change_time_ timestamp with time zone;
BEGIN
  LOCK TABLE object_state;
  SELECT _id, state FROM object_state WHERE _object = object_ AND _ostype = ostype_ INTO id_, real_state_;
  change_time_ = NOW();
  IF id_ IS NULL THEN
    INSERT INTO object_state (_object, _ostype, state, change_time) VALUES(object_, ostype_, new_state_, change_time_) RETURNING _id INTO id_;
    INSERT INTO object_state_log (_ostate, old_state, new_state, change_time)
     VALUES (id_, old_state_, new_state_, change_time_);
  ELSE
    IF real_state_ <> old_state_ THEN
      UPDATE object_state SET state = old_state_ WHERE _id = id_;
    END IF;
    UPDATE object_state SET state = new_state_, change_time = change_time_ WHERE _id = id_;
  END IF;
  RETURN id_;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION object_state_init(integer, integer, integer, integer)
  OWNER TO su;
--
GRANT EXECUTE ON FUNCTION object_state_init(integer, integer, integer, integer) TO usr;
--
-- Function: objects_add_setting(integer, text, text)

-- DROP FUNCTION objects_add_setting(integer, text, text);

CREATE OR REPLACE FUNCTION objects_add_setting(otype_ integer, key_ text, value_ text)
  RETURNS void AS
$BODY$
BEGIN
  INSERT INTO object_settings(_object, key, value) (SELECT _id, key_, value_ FROM object WHERE _otype = otype_);
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION objects_add_setting(integer, text, text)
  OWNER TO su;
--
-- Function: version_is(text)

-- DROP FUNCTION version_is(text);

CREATE OR REPLACE FUNCTION version_is(name_ text)
  RETURNS integer AS
$BODY$
DECLARE
  ver_ integer;
BEGIN
  SELECT value::integer FROM variables WHERE key=name_||'Ver' AND _object IS NULL INTO ver_;
  IF ver_ IS NULL THEN
    INSERT INTO variables (_object, key, value) VALUES (NULL, name_||'Ver', 0);
    RETURN 0;
  ELSE
    RETURN ver_;
  END IF;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION version_is(text)
  OWNER TO su;

-- Function: version_set(text, integer)

-- DROP FUNCTION version_set(text, integer);

CREATE OR REPLACE FUNCTION version_set(name_ text, ver_ integer)
  RETURNS void AS
$BODY$
BEGIN
  UPDATE variables SET value = ver_::text WHERE key=name_||'Ver' AND _object IS NULL;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;

ALTER FUNCTION version_set(text, integer)
  OWNER TO su;

-- AUTO generated script --

-- Table: event_type

-- DROP TABLE event_type;

CREATE TABLE event_type
(
  _id serial NOT NULL,
  name text NOT NULL,
  descr text NOT NULL,
  icon text,
  flag integer NOT NULL DEFAULT 1,
  CONSTRAINT event_type_pkey PRIMARY KEY (_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_type
  OWNER TO su;

-- Index: event_type_name_idx

-- DROP INDEX event_type_name_idx;

CREATE INDEX event_type_name_idx
  ON event_type
  USING btree
  (name COLLATE pg_catalog."default");

-- Constraint: event_type_name_key

-- ALTER TABLE event_type DROP CONSTRAINT event_type_name_key;

ALTER TABLE event_type
  ADD CONSTRAINT event_type_name_key UNIQUE(name);
-- Table: object_state_type

-- DROP TABLE object_state_type;

CREATE TABLE object_state_type
(
  _id serial NOT NULL,
  name text NOT NULL,
  descr text,
  CONSTRAINT object_state_type_pkey PRIMARY KEY (_id),
  CONSTRAINT object_state_type_name_key UNIQUE (name)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_state_type
  OWNER TO su;

-- Index: object_state_type_name_idx

-- DROP INDEX object_state_type_name_idx;

CREATE INDEX object_state_type_name_idx
  ON object_state_type
  USING btree
  (name COLLATE pg_catalog."default");

GRANT ALL ON TABLE object_state_type TO su;
GRANT SELECT ON TABLE object_state_type TO usr;

-- Constraint: object_state_type_name_key

-- ALTER TABLE object_state_type DROP CONSTRAINT object_state_type_name_key;

-- ALTER TABLE object_state_type
--  ADD CONSTRAINT object_state_type_name_key UNIQUE(name);
-- Table: object_state_values

-- DROP TABLE object_state_values;

CREATE TABLE object_state_values
(
  _id serial NOT NULL,
  _ostype integer NOT NULL,
  state integer NOT NULL,
  descr text NOT NULL,
  color text NOT NULL,
  CONSTRAINT object_state_values_pkey PRIMARY KEY (_id),
  CONSTRAINT object_state_values__ostype_fkey FOREIGN KEY (_ostype)
      REFERENCES object_state_type (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_state_values
  OWNER TO su;

-- Index: object_state_values_name_idx

-- DROP INDEX object_state_values_name_idx;

CREATE INDEX object_state_values_name_idx
  ON object_state_values
  USING btree
  (_ostype, state);

GRANT ALL ON TABLE object_state_values TO su;
GRANT SELECT ON TABLE object_state_values TO usr;
-- Table: object_type

-- DROP TABLE object_type;

CREATE TABLE object_type
(
  _id serial NOT NULL,
  _odefault integer,
  name text NOT NULL,
  descr text,
  CONSTRAINT object_type_pkey PRIMARY KEY (_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_type
  OWNER TO su;

-- Index: object_type_name_idx

-- DROP INDEX object_type_name_idx;

CREATE INDEX object_type_name_idx
  ON object_type
  USING btree
  (name COLLATE pg_catalog."default");

GRANT ALL ON TABLE object_type TO su;
GRANT SELECT ON TABLE object_type TO usr;

-- Constraint: object_type_name_key

-- ALTER TABLE object_type DROP CONSTRAINT object_type_name_key;

ALTER TABLE object_type
  ADD CONSTRAINT object_type_name_key UNIQUE(name);

-- Table: object

-- DROP TABLE object CASCADE;

CREATE TABLE object
(
  _id serial NOT NULL,
  _otype integer NOT NULL,
  _parent integer,
  guid text NOT NULL,
  name text NOT NULL,
  descr text,
  version text,
  revision integer DEFAULT 1,
  uri text,
  status integer DEFAULT 0,
  CONSTRAINT object_pkey PRIMARY KEY (_id),
  CONSTRAINT object__otype_fkey FOREIGN KEY (_otype)
      REFERENCES object_type (_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT object__parent_fkey FOREIGN KEY (_parent)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT object__otype_guid_key UNIQUE (guid)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object
  OWNER TO su;

-- Index: object__otype_idx

-- DROP INDEX object__otype_idx;

CREATE INDEX object__otype_idx
  ON object
  USING btree
  (_otype);

-- Index: object_guid_idx

-- DROP INDEX object_guid_idx;

CREATE INDEX object_guid_idx
  ON object
  USING btree
  (guid COLLATE pg_catalog."default");

ALTER TABLE object_type ADD
CONSTRAINT object_type__odefault_fkey FOREIGN KEY (_odefault)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION;

GRANT ALL ON TABLE object TO su;
GRANT SELECT ON TABLE object TO usr;
-- Table: event

-- DROP TABLE event;

CREATE TABLE event
(
  _id serial NOT NULL,
  _object integer NOT NULL,
  _etype integer NOT NULL,
  CONSTRAINT event_pkey PRIMARY KEY (_id),
  CONSTRAINT event__etype_fkey FOREIGN KEY (_etype)
      REFERENCES event_type (_id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT event__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event
  OWNER TO su;

-- Index: event__object_idx

-- DROP INDEX event__object_idx;

CREATE INDEX event__object_idx
  ON event
  USING btree
  (_object);

-- Table: event_log

-- DROP TABLE event_log;

CREATE TABLE event_log
(
  _id bigserial NOT NULL,
  _event integer NOT NULL,
  triggered_time timestamp with time zone NOT NULL,
  value real NOT NULL DEFAULT 1,
  info text,
  CONSTRAINT event_log_pkey PRIMARY KEY (_id),
  CONSTRAINT event_log__event_fkey FOREIGN KEY (_event)
      REFERENCES event (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_log
  OWNER TO su;

-- Index: event_log__event_triggered_time_idx

-- DROP INDEX event_log__event_triggered_time_idx;

CREATE INDEX event_log__event_triggered_time_idx
  ON event_log
  USING btree
  (_event, triggered_time);

-- Index: event_log_triggered_time_idx

-- DROP INDEX event_log_triggered_time_idx;

CREATE INDEX event_log_triggered_time_idx
  ON event_log
  USING btree
  (triggered_time);

-- Table: event_log_hours

-- DROP TABLE event_log_hours;

CREATE TABLE event_log_hours
(
  _id bigserial NOT NULL,
  _event integer NOT NULL,
  triggered_hour timestamp with time zone NOT NULL,
  value real NOT NULL DEFAULT 0,
  CONSTRAINT event_log_hours_pkey PRIMARY KEY (_id),
  CONSTRAINT event_log_hours__event_fkey FOREIGN KEY (_event)
      REFERENCES event (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_log_hours
  OWNER TO su;

-- Index: event_log_hours__event_triggered_hour_idx

-- DROP INDEX event_log_hours__event_triggered_hour_idx;

CREATE INDEX event_log_hours__event_triggered_hour_idx
  ON event_log_hours
  USING btree
  (_event, triggered_hour);

-- Index: event_log_hours_triggered_hour_idx

-- DROP INDEX event_log_hours_triggered_hour_idx;

CREATE INDEX event_log_hours_triggered_hour_idx
  ON event_log_hours
  USING btree
  (triggered_hour);

CREATE OR REPLACE FUNCTION event_log_insert_post() RETURNS trigger AS $BODY$
DECLARE
  triggered_hour_ timestamp with time zone;
  id_ bigint;
BEGIN
  triggered_hour_ := date_trunc('hours', NEW.triggered_time);
  SELECT _id FROM event_log_hours WHERE triggered_hour = triggered_hour_ AND _event = NEW._event INTO id_;
  IF (id_ IS NULL) THEN
    INSERT INTO event_log_hours (_event, triggered_hour, value)
      VALUES (NEW._event, triggered_hour_, NEW.value);
  ELSE
    UPDATE event_log_hours SET value = value + NEW.value WHERE _id = id_;
  END IF;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION event_log_insert_post()
  OWNER TO su;
---
CREATE TRIGGER event_log_insert_post AFTER INSERT ON event_log
  FOR EACH ROW EXECUTE PROCEDURE event_log_insert_post();
-- Table: event_stat

-- DROP TABLE event_stat;

CREATE TABLE event_stat
(
  _id serial NOT NULL,
  _event integer NOT NULL,
  triggered_time timestamp with time zone NOT NULL DEFAULT now(),
  value real NOT NULL DEFAULT 0,
  period integer NOT NULL DEFAULT 0,
  CONSTRAINT event_stat_pkey PRIMARY KEY (_id),
  CONSTRAINT event_stat__event_fkey FOREIGN KEY (_event)
      REFERENCES event (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT event_stat__event_key UNIQUE (_event)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_stat
  OWNER TO su;

-- Index: event_stat__event_triggered_time_idx

-- DROP INDEX event_stat__event_triggered_time_idx;

CREATE INDEX event_stat__event_triggered_time_idx
  ON event_stat
  USING btree
  (_event, triggered_time);

-- Index: event_stat_triggered_time_idx

-- DROP INDEX event_stat_triggered_time_idx;

CREATE INDEX event_stat_triggered_time_idx
  ON event_stat
  USING btree
  (triggered_time);

-- Table: event_stat_hours

-- DROP TABLE event_stat_hours;

CREATE TABLE event_stat_hours
(
  _id bigserial NOT NULL,
  _event integer NOT NULL,
  triggered_hour timestamp with time zone NOT NULL,
  value real NOT NULL DEFAULT 0,
  period integer NOT NULL DEFAULT 0,
  CONSTRAINT event_stat_hours_pkey PRIMARY KEY (_id),
  CONSTRAINT event_stat_hours__event_fkey FOREIGN KEY (_event)
      REFERENCES event (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE event_stat_hours
  OWNER TO su;

-- Index: event_stat_hours__event_triggered_hour_idx

-- DROP INDEX event_stat_hours__event_triggered_hour_idx;

CREATE INDEX event_stat_hours__event_triggered_hour_idx
  ON event_stat_hours
  USING btree
  (_event, triggered_hour);

-- Index: event_stat_hours_triggered_hour_idx

-- DROP INDEX event_stat_hours_triggered_hour_idx;

CREATE INDEX event_stat_hours_triggered_hour_idx
  ON event_stat_hours
  USING btree
  (triggered_hour);

CREATE OR REPLACE FUNCTION event_stat_update_post() RETURNS trigger AS $BODY$
DECLARE
  triggered_hour_ timestamp with time zone;
  id_ bigint;
BEGIN
  triggered_hour_ := date_trunc('hours', NEW.triggered_time);
  SELECT _id FROM event_stat_hours WHERE triggered_hour = triggered_hour_ AND _event = NEW._event INTO id_;
  IF (id_ IS NULL) THEN
    INSERT INTO event_stat_hours (_event, triggered_hour, value, period)
      VALUES (NEW._event, triggered_hour_, NEW.value, NEW.period);
  ELSE
    UPDATE event_stat_hours SET value = value + NEW.value, period = period + NEW.period 
      WHERE _id = id_;
  END IF;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION event_stat_update_post()
  OWNER TO su;
---
CREATE TRIGGER event_stat_update_post AFTER UPDATE ON event_stat
  FOR EACH ROW EXECUTE PROCEDURE event_stat_update_post();
-- Table: files

-- DROP TABLE files;

CREATE TABLE files
(
  _id bigserial NOT NULL,
  _object integer,
  name text,
  mime_type text,
  data bytea,
  CONSTRAINT files_pkey PRIMARY KEY (_id),
  CONSTRAINT files__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE files
  OWNER TO su;

-- Index: files__object_idx

-- DROP INDEX files__object_idx;

CREATE INDEX files__object_idx
  ON files
  USING btree
  (_object);

-- Table: object_connection

-- DROP TABLE object_connection;

CREATE TABLE object_connection
(
  _id serial NOT NULL,
  _omaster integer,
  _oslave integer,
  type integer,
  CONSTRAINT object_connection_pkey PRIMARY KEY (_id),
  CONSTRAINT object_connection__omaster_fkey FOREIGN KEY (_omaster)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT object_connection__oslave_fkey FOREIGN KEY (_oslave)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_connection
  OWNER TO su;

-- Index: object_connection__omaster_idx

-- DROP INDEX object_connection__omaster_idx;

CREATE INDEX object_connection__omaster_idx
  ON object_connection
  USING btree
  (_omaster);

-- Index: object_connection__oslave_idx

-- DROP INDEX object_connection__oslave_idx;

CREATE INDEX object_connection__oslave_idx
  ON object_connection
  USING btree
  (_oslave);

GRANT ALL ON TABLE object_connection TO su;
GRANT SELECT ON TABLE object_connection TO usr;
CREATE OR REPLACE FUNCTION object_connection_change_post() RETURNS trigger AS $BODY$
BEGIN
  IF NEW._omaster = NEW._oslave THEN
    RAISE EXCEPTION 'object_connection recursion not allowed';
  END IF;
  UPDATE object o SET revision = o.revision + 1 WHERE o._id = NEW._omaster;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_connection_change_post()
  OWNER TO su;
---
CREATE TRIGGER object_connection_change_post AFTER INSERT OR UPDATE ON object_connection
  FOR EACH ROW EXECUTE PROCEDURE object_connection_change_post();

---
CREATE OR REPLACE FUNCTION object_connection_delete_post() RETURNS trigger AS $BODY$
BEGIN
  UPDATE object o SET revision = o.revision + 1 WHERE o._id = OLD._omaster;
  RETURN OLD;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_connection_delete_post()
  OWNER TO su;
---
CREATE TRIGGER object_connection_delete_post AFTER DELETE ON object_connection
  FOR EACH ROW EXECUTE PROCEDURE object_connection_delete_post();
-- Table: object_log

-- DROP TABLE IF EXISTS object_log;

CREATE TABLE object_log
(
  _id bigserial,
  _object integer NOT NULL,
  period_start timestamp with time zone NOT NULL,
  period_end timestamp with time zone NOT NULL,
  thread_name text,
  work_name text,
  total_time integer DEFAULT 1,
  circles integer DEFAULT 0,
  work_time integer DEFAULT 0,
  longest_work integer DEFAULT 0,
  CONSTRAINT object_log_pkey PRIMARY KEY (_id),
  CONSTRAINT object_log__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_log
  OWNER TO su;
GRANT ALL ON TABLE object_log TO su;
GRANT SELECT, UPDATE ON TABLE object_log TO usr;

-- Index: object_log__object_period_start_idx

-- DROP INDEX object_log__object_period_start_idx;

CREATE INDEX object_log__object_period_start_idx
  ON object_log
  USING btree
  (_object, period_start);
-- Table: object_log_hours

-- DROP TABLE IF EXISTS object_log_hours;

CREATE TABLE object_log_hours
(
  _id bigserial,
  _object integer NOT NULL,
  period_hour timestamp with time zone NOT NULL,
  thread_name text,
  work_name text,
  total_time integer DEFAULT 1,
  circles integer DEFAULT 0,
  work_time integer DEFAULT 0,
  longest_work integer DEFAULT 0,
  log_count integer DEFAULT 1,
  CONSTRAINT object_log_hours_pkey PRIMARY KEY (_id),
  CONSTRAINT object_log_hours__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_log_hours
  OWNER TO su;
GRANT ALL ON TABLE object_log_hours TO su;
GRANT SELECT, UPDATE ON TABLE object_log_hours TO usr;

-- Index: object_log_hours__object_period_hour_idx

-- DROP INDEX object_log_hours__object_period_hour_idx;

CREATE INDEX object_log_hours__object_period_hour_idx
  ON object_log_hours
  USING btree
  (_object, period_hour);
-- Table: object_log_info

-- DROP TABLE IF EXISTS object_log_info;

CREATE TABLE object_log_info
(
  _id bigserial,
  _object integer NOT NULL,
  hours_start timestamp with time zone NOT NULL,
  hours_end timestamp with time zone NOT NULL,
  last_trunc timestamp with time zone,
  last_clean timestamp with time zone,
  CONSTRAINT object_log_info_pkey PRIMARY KEY (_id),
  CONSTRAINT object_log_info__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT object_log_info__object_key UNIQUE (_object)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_log_info
  OWNER TO su;
GRANT ALL ON TABLE object_log_info TO su;
GRANT SELECT, UPDATE ON TABLE object_log_info TO usr;

-- Index: object_log_info__object_idx

-- DROP INDEX object_log_info__object_idx;

CREATE INDEX object_log_info__object_idx
  ON object_log_info
  USING btree
  (_object);

-- Index: public.object_log_info_last_clean_idx

-- DROP INDEX public.object_log_info_last_clean_idx;

CREATE INDEX object_log_info_last_clean_idx
  ON public.object_log_info
  USING btree
  (last_clean);

-- Index: public.object_log_info_last_trunc_idx

-- DROP INDEX public.object_log_info_last_trunc_idx;

CREATE INDEX object_log_info_last_trunc_idx
  ON public.object_log_info
  USING btree
  (last_trunc);-- Table: object_settings

-- DROP TABLE object_settings;

CREATE TABLE object_settings
(
  _id serial NOT NULL,
  _object integer NOT NULL,
  key text NOT NULL,
  value text,
  CONSTRAINT object_settings_pkey PRIMARY KEY (_id),
  CONSTRAINT object_settings__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_settings
  OWNER TO su;

-- Index: object_settings__object_idx
-- DROP INDEX object_settings__object_idx;
CREATE INDEX object_settings__object_idx
  ON object_settings
  USING btree
  (_object, key);

GRANT ALL ON TABLE object_settings TO su;
GRANT SELECT ON TABLE object_settings TO usr;
CREATE OR REPLACE FUNCTION object_settings_change_post() RETURNS trigger AS $BODY$
BEGIN
  UPDATE object SET revision = revision + 1 WHERE _id = NEW._object;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_settings_change_post()
  OWNER TO su;
---
CREATE TRIGGER object_settings_change_post AFTER INSERT OR UPDATE ON object_settings
  FOR EACH ROW EXECUTE PROCEDURE object_settings_change_post();
-- Table: object_settings_type

-- DROP TABLE object_settings_type;

CREATE TABLE object_settings_type
(
  _id serial NOT NULL,
  _otype integer,
  key text,
  name text,
  descr text,
  type text,
  min_value text,
  max_value text,
  CONSTRAINT object_settings_type_pkey PRIMARY KEY (_id),
  CONSTRAINT object_settings_type__otype_fkey FOREIGN KEY (_otype)
      REFERENCES object_type (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_settings_type
  OWNER TO su;

-- Index: object_settings_type__otype_key_idx

-- DROP INDEX object_settings_type__otype_key_idx;

CREATE INDEX object_settings_type__otype_key_idx
  ON object_settings_type
  USING btree
  (_otype, key COLLATE pg_catalog."default");

GRANT ALL ON TABLE object_settings_type TO su;
GRANT SELECT ON TABLE object_settings_type TO usr;
-- Table: object_state

-- DROP TABLE object_state CASCADE;

CREATE TABLE object_state
(
  _id serial NOT NULL,
  _object integer NOT NULL,
  _ostype integer NOT NULL,
  state integer DEFAULT 0,
  change_time timestamp with time zone NOT NULL,
  CONSTRAINT object_state_pkey PRIMARY KEY (_id),
  CONSTRAINT object_state__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT object_state__ostype_fkey FOREIGN KEY (_ostype)
      REFERENCES object_state_type (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_state
  OWNER TO su;

-- Index: object_state__object__ostype_idx

-- DROP INDEX object_state__object__ostype_idx;

CREATE INDEX object_state__object__ostype_idx
  ON object_state
  USING btree
  (_object, _ostype);

GRANT ALL ON TABLE object_state TO su;
GRANT SELECT, UPDATE ON TABLE object_state TO usr;
-- Table: object_state_log

-- DROP TABLE object_state_log;

CREATE TABLE object_state_log
(
  _id bigserial NOT NULL,
  _ostate integer NOT NULL,
  old_state integer NOT NULL,
  new_state integer NOT NULL,
  change_time timestamp with time zone NOT NULL,
  CONSTRAINT object_state_log_pkey PRIMARY KEY (_id),
  CONSTRAINT object_state_log__ostate_fkey FOREIGN KEY (_ostate)
      REFERENCES object_state (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE object_state_log
  OWNER TO su;

-- Index: object_state_log__ostate_change_time_idx

-- DROP INDEX object_state_log__ostate_change_time_idx;

CREATE INDEX object_state_log__ostate_change_time_idx
  ON object_state_log
  USING btree
  (_ostate, change_time);


-- Index: object_state_log_change_time_idx

-- DROP INDEX object_state_log_change_time_idx;

CREATE INDEX object_state_log_change_time_idx
  ON object_state_log
  USING btree
  (change_time);

GRANT ALL ON TABLE object_state_log TO su;
GRANT SELECT, UPDATE, INSERT ON TABLE object_state_log TO usr;
--DROP TRIGGER IF EXISTS object_state_update_post ON object_state;
--DROP FUNCTION object_state_update_post();
---
CREATE OR REPLACE FUNCTION object_state_update_post() RETURNS trigger AS $BODY$
BEGIN
  IF (OLD.state <> NEW.state) THEN
    INSERT INTO object_state_log (_ostate, old_state, new_state, change_time)
     VALUES (NEW._id, OLD.state, NEW.state, NEW.change_time);
  END IF;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_state_update_post()
  OWNER TO su;
--GRANT EXECUTE ON FUNCTION object_state_update_post(integer, integer, integer, integer) TO usr;
---
CREATE TRIGGER object_state_update_post AFTER UPDATE ON object_state
  FOR EACH ROW EXECUTE PROCEDURE object_state_update_post();
---
/*---
DROP TRIGGER IF EXISTS object_state_add_post ON object_state;
DROP FUNCTION object_state_add_post();
---
CREATE OR REPLACE FUNCTION object_state_add_post() RETURNS trigger AS $BODY$
BEGIN
  INSERT INTO object_state_log (_ostate, old_state, new_state, change_time)
   VALUES (NEW._id, NEW.state, NEW.state, NOW());
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_state_add_post()
  OWNER TO su;
---
CREATE TRIGGER object_state_add_post AFTER INSERT ON object_state
  FOR EACH ROW EXECUTE PROCEDURE object_state_add_post();
---
*/
CREATE OR REPLACE FUNCTION object_change_post() RETURNS trigger AS $BODY$
BEGIN
  UPDATE object o SET revision = o.revision + 1
    FROM object_connection c
    WHERE o._id = c._omaster AND c._oslave = NEW._id;
  RETURN NEW;
END;
$BODY$ LANGUAGE plpgsql VOLATILE SECURITY DEFINER;
ALTER FUNCTION object_change_post()
  OWNER TO su;
---
CREATE TRIGGER object_change_post AFTER INSERT OR UPDATE ON object
  FOR EACH ROW EXECUTE PROCEDURE object_change_post();
-- Table: report

-- DROP TABLE report;

CREATE TABLE report
(
  _id bigserial NOT NULL,
  _object integer NOT NULL,
  type integer,
  period_begin timestamp with time zone,
  period_end timestamp with time zone,
  data bytea,
  CONSTRAINT report_pkey PRIMARY KEY (_id),
  CONSTRAINT report__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE report
  OWNER TO su;

-- Index: report__object_idx

-- DROP INDEX report__object_idx;

CREATE INDEX report__object_idx
  ON report
  USING btree
  (_object);

-- Index: report__object_type_period_begin_idx

-- DROP INDEX report__object_type_period_begin_idx;

CREATE INDEX report__object_type_period_begin_idx
  ON report
  USING btree
  (_object, type, period_begin);

-- Table: report_files

-- DROP TABLE report_files;

CREATE TABLE report_files
(
  _id bigserial NOT NULL,
  _report bigint,
  _files bigint,
  CONSTRAINT report_files_pkey PRIMARY KEY (_id),
  CONSTRAINT report_files__report_fkey FOREIGN KEY (_report)
      REFERENCES report (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT report_files__files_fkey FOREIGN KEY (_files)
      REFERENCES files (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE report_files
  OWNER TO su;


CREATE INDEX report_files__report_idx
  ON report_files
  USING btree
  (_report);

CREATE INDEX report_files__files_idx
  ON report_files
  USING btree
  (_files);

-- Table: report_send

-- DROP TABLE report_send;

CREATE TABLE report_send
(
  _id bigserial NOT NULL,
  _oto integer NOT NULL,
  _last_report integer NOT NULL,
  send_time timestamp with time zone,
  CONSTRAINT report_send_pkey PRIMARY KEY (_id),
  CONSTRAINT report_send__oto_fkey FOREIGN KEY (_oto)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT report_send__last_report_fkey FOREIGN KEY (_last_report)
      REFERENCES report (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE SET NULL,
  CONSTRAINT report_send__oto_key UNIQUE (_oto)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE report_send
  OWNER TO su;


-- Index: report_send__oto_idx

-- DROP INDEX report_send__oto_idx;

CREATE INDEX report_send__oto_idx
  ON report_send
  USING btree
  (_oto);

-- Table: variables

-- DROP TABLE variables;

CREATE TABLE variables
(
  _id bigserial NOT NULL,
  _object integer,
  key text,
  value text,
  CONSTRAINT variables_pkey PRIMARY KEY (_id),
  CONSTRAINT variables__object_fkey FOREIGN KEY (_object)
      REFERENCES object (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE variables
  OWNER TO su;
GRANT ALL ON TABLE variables TO su;
GRANT SELECT ON TABLE variables TO usr;

-- Index: variables__null_key_key

-- DROP INDEX variables__null_key_key;

CREATE UNIQUE INDEX variables__null_key_key
  ON variables
  USING btree
  (key COLLATE pg_catalog."default")
  WHERE _object IS NULL;

-- Index: variables__object_key_key

-- DROP INDEX variables__object_key_key;

CREATE UNIQUE INDEX variables__object_key_key
  ON variables
  USING btree
  (_object, key COLLATE pg_catalog."default")
  WHERE _object IS NOT NULL;

-- Table: va_stat

-- DROP TABLE va_stat;

CREATE TABLE va_stat
(
  _id serial NOT NULL,
  _object integer NOT NULL,
  _vstype integer NOT NULL,
  CONSTRAINT va_stat_pkey PRIMARY KEY (_id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE va_stat
  OWNER TO su;


-- Index: va_stat__vstype__object_idx

-- DROP INDEX va_stat__vstype__object_idx;

CREATE INDEX va_stat__vstype__object_idx
  ON va_stat
  USING btree
  (_object, _vstype);

-- Table: va_stat_days

-- DROP TABLE va_stat_days;

CREATE TABLE va_stat_days
(
  _id bigserial NOT NULL,
  _vstat integer NOT NULL,
  _fimage bigint NOT NULL,
  day timestamp with time zone NOT NULL,
  CONSTRAINT va_stat_days_pkey PRIMARY KEY (_id),
  CONSTRAINT va_stat_days__vstat_fkey FOREIGN KEY (_vstat)
      REFERENCES va_stat (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT va_stat_days__fimage_fkey FOREIGN KEY (_fimage)
      REFERENCES files (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE
)
WITH (
  OIDS=FALSE
);
ALTER TABLE va_stat_days
  OWNER TO su;

-- Index: va_stat_days__object_day_idx

-- DROP INDEX va_stat_days__object_day_idx;

CREATE INDEX va_stat_days__object_day_idx
  ON va_stat_days
  USING btree
  (_vstat, day);

-- Index: va_stat_days_day_idx

-- DROP INDEX va_stat_days_day_idx;

CREATE INDEX va_stat_days_day_idx
  ON va_stat_days
  USING btree
  (day);

-- Table: va_stat_hours

-- DROP TABLE va_stat_hours;

CREATE TABLE va_stat_hours
(
  _id bigserial NOT NULL,
  _vstat integer NOT NULL,
  _fimage bigint NOT NULL,
  hour timestamp with time zone NOT NULL,
  CONSTRAINT va_stat_hours_pkey PRIMARY KEY (_id),
  CONSTRAINT va_stat_hours__vstat_fkey FOREIGN KEY (_vstat)
      REFERENCES va_stat (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE CASCADE,
  CONSTRAINT va_stat_hours__fimage_fkey FOREIGN KEY (_fimage)
      REFERENCES files (_id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE va_stat_hours
  OWNER TO su;

-- Index: va_stat_hours__object_hour_idx

-- DROP INDEX va_stat_hours__object_hour_idx;

CREATE INDEX va_stat_hours__object_hour_idx
  ON va_stat_hours
  USING btree
  (_vstat, hour);

-- Index: va_stat_hours_hour_idx

-- DROP INDEX va_stat_hours_hour_idx;

CREATE INDEX va_stat_hours_hour_idx
  ON va_stat_hours
  USING btree
  (hour);

-- Table: va_stat_type

-- DROP TABLE va_stat_type;

CREATE TABLE va_stat_type
(
  _id serial NOT NULL,
  abbr text NOT NULL,
  name text NOT NULL,
  descr text NOT NULL,
  CONSTRAINT va_stat_type_pkey PRIMARY KEY (_id),
  CONSTRAINT va_stat_type_abbr_key UNIQUE (abbr)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE va_stat_type
  OWNER TO su;

-- Index: va_stat_type_abbr_idx

-- DROP INDEX va_stat_type_abbr_idx;

CREATE INDEX va_stat_type_abbr_idx
  ON va_stat_type
  USING btree
  (abbr COLLATE pg_catalog."default");


-- AUTO generated script --


-- AUTO generated script --

--------------------
--
-- !!! If edit: Update ObjectState.h !!!
--
--------------------
--DELETE FROM object_state;
DELETE FROM object_state_values;
DELETE FROM object_state_type;
--------------------
INSERT INTO object_state_type(_id, name, descr) VALUES (1, 'power', 'Питание');

INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (1, 0, 'Выключено', 'gray');
INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (1, 1, 'Включено', 'green');
INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (1, 2, 'Спящий режим', 'gray');

INSERT INTO object_state_type(_id, name, descr) VALUES (2, 'service', 'Сервис');

INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (2, -2, 'Проблемы', 'red');
INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (2, -1, 'Замечания', 'orange');
INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (2, 0, 'Выключено', 'gray');
INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (2, 1, 'Порядок', 'green');

INSERT INTO object_state_type(_id, name, descr) VALUES (3, 'connect', 'Соединение');

INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (3, -1, 'Недоступно', 'red');
INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (3, 0, 'Выключено', 'gray');
INSERT INTO object_state_values(_ostype, state, descr, color) VALUES (3, 1, 'Доступно', 'green');
INSERT INTO event_type(name, descr, icon, flag) VALUES ('in', 'вход', 'man_in.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('out', 'выход', 'man_out.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('qstart', 'очередь', 'queue_on.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('qend', 'норма', 'queue_off.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('zone', 'в зоне', 'man_zone.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('queue', 'очередь', 'queue_on.png', 2);

INSERT INTO va_stat_type(abbr, name, descr) VALUES ('mov', 'Тепловые карты', 'Тепловые карты показывают области наиболее интенсивного движения. Каждый движущийся объект при перемещении как бы отдаёт тепло, которое накапливается и отображается на карте. Таким образом, по карте можно судить об областях наиболее интенсивного движения. Нужно так же учитывать, что зеркальные поверхности будут собирать отражённое в них движение.');
--------------------
DELETE FROM object;
DELETE FROM object_type;
--------------------
INSERT INTO object_type (_id, name, descr) VALUES (0, 'tmp', 'Шаблон');
INSERT INTO object_type (_id, name, descr) VALUES (1, 'srv', 'Сервер');
INSERT INTO object_type (_id, name, descr) VALUES (20, 'cam', 'Камера');
INSERT INTO object_type (_id, name, descr) VALUES (3, 'rep', 'Хранилище');
INSERT INTO object_type (_id, name, descr) VALUES (5, 'arm', 'АРМ оператора');
INSERT INTO object_type (_id, name, descr) VALUES (6, 'www', 'Web-Портал');
INSERT INTO object_type (_id, name, descr) VALUES (7, 'uni', 'Сервис объединения');
INSERT INTO object_type (_id, name, descr) VALUES (8, 'lis', 'Сервис лицензирования');
INSERT INTO object_type (_id, name, descr) VALUES (9, 'rpt', 'Сервис отчётов');
INSERT INTO object_type (_id, name, descr) VALUES (13, 'smp', 'SMTP аккаунт');
INSERT INTO object_type (_id, name, descr) VALUES (14, 'eml', 'Адресат рассылки');
INSERT INTO object_type (_id, name, descr) VALUES (50, 'sch', 'Расписание');

INSERT INTO object_type (_id, name, descr) VALUES (31, 'vac', 'Видеоаналитика посетителей');
INSERT INTO object_type (_id, name, descr) VALUES (41, 'iod', 'Детектор входа/выхода');
INSERT INTO object_type (_id, name, descr) VALUES (42, 'ioo', 'Область входа/выхода');
INSERT INTO object_type (_id, name, descr) VALUES (43, 'ign', 'Область игнорирования');

INSERT INTO object_type (_id, name, descr) VALUES (11, 'sr_', 'Внешний сервер');
INSERT INTO object_type (_id, name, descr) VALUES (12, 'ca_', 'Внешняя камера');

INSERT INTO object_type (_id, name, descr) VALUES (18, 'usr', 'Пользователь');
INSERT INTO object_type (_id, name, descr) VALUES (19, 'upd', 'Точка обновления');
SELECT setval('object_type__id_seq', 100);
--------------------
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (0, 0, 'tmp', 'Шаблоны', 'Корневой объект для всех шаблонов', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (1, 1, 'srv', 'Сервер', 'Сервер по умолчанию', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (21, 20, 'cam', 'Камера', 'Камера по умолчанию', 0, 'tcp::', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (22, 20, 'usb', 'USB веб-камера', 'Веб-камера по умолчанию', 0, 'tcp::', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (24, 20, 'v4l', 'USB Video4Linux', 'USB или встроенная камера через Video4Linux по умолчанию', 0, 'tcp::', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (23, 20, 'file', 'Видеофайл', 'Видеофайл по умолчанию', 0, 'tcp::', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (3, 3, 'rep', 'Хранилище', 'Хранилище по умолчанию', 0, '', -1);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (5, 5, 'arm', 'АРМ оператора', 'АРМ оператора по умолчанию', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (31, 31, 'vac', 'Аналитика посетителей', 'Анализ видеопотока для выявления и отслеживания людей', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (41, 41, 'iod', 'Счётчик посещений', 'Подсчёт входящих и выходящих посетителей', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (42, 42, 'ioo', 'Область входа/выхода', 'Область, из которой могут выходить и входить объекты наблюдения', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (43, 43, 'ign', 'Область игнорирования', 'Область, которая исключается из анализа', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (7, 7, 'uni', 'Сервис объединения', 'Сервис объединения локальных узлов к центральному', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (6, 6, 'www', 'Web-Портал', 'Web-Портал для взаимодействия с пользователями', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (9, 9, 'rpt', 'Сервис отчётов', 'Сервис формирования и отправки отчётов', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (13, 13, 'smp', 'SMTP аккаунт', 'SMTP аккаунт для отправки рассылки', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (14, 14, 'eml', 'Адресат рассылки', 'Адресат рассылки', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (18, 18, 'usr', 'Пользователь', 'Пользователь кластера', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (19, 19, 'upd', 'Точка обновления', 'Точка обновления по-умолчанию', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (51, 50, 'sch1', 'Ежедневное расписание', 'Расписание, одинаковое каждый день', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (52, 50, 'sch2', 'Еженедельное расписание', 'Расписание, одинаковое каждую неделю', 0, '', 0);

INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (28, 18, 'root', 'root', 'Администратор кластера', 0, '', 0);
SELECT setval('object__id_seq', 100);
--------------------
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 1, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 5, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 18, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 19, 0);

INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 51, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 52, 0);

INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 21, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 22, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 23, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 24, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 3, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 31, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 41, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 42, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 43, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 7, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 6, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 9, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 13, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 14, 0);
--------------------
DELETE FROM object_settings;
DELETE FROM object_settings_type;
--------------------

-- (obj_type, def_obj) --
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (0, 'Id', 'Ид', 'Идентификатор объекта', 'int', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (0, 'Name', 'Имя', 'Имя объекта', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (0, 'Descr', 'Описание', 'Описание объекта', 'string', '', '');
--- Server --- (1, 1)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (1, 'IP', 'IP адрес', 'IP адрес, под которым компоненты сервера будут доступны в системе', 'inet_address', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (1, 'Init', 'Инициализация', 'Инициализировать сервер стандартным набором сервисов', 'bool', 'Нет', 'Да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (1, 'Quiet', 'Молчун', 'Сервер и его компоненты не будут обновлять своё состояние в БД', 'bool', 'Нет', 'Да');
INSERT INTO object_settings(_object, key, value) VALUES (1, 'IP', '');
INSERT INTO object_settings(_object, key, value) VALUES (1, 'Init', '1');
INSERT INTO object_settings(_object, key, value) VALUES (1, 'Quiet', '1');
--- Store --- (3, 3)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (3, 'Path', 'Путь', 'Путь к объекту файловой системы, это может быть как файл, так и ссылка на диск', 'path', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (3, 'CellSize', 'Кластер', 'Размер ячейки хранилища', 'size', '16777216', '16777216');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (3, 'PageSize', 'Страница', 'Минимальный размер данных, для чтения/записи хранилища', 'size', '1048576', '1048576');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (3, 'Capacity', 'Размер хранилища', 'Размер хранилища в кластерах (как правило имеет смысл указывать только максимальное значение)', 'size', '200', 'max');
INSERT INTO object_settings(_object, key, value) VALUES (3, 'Path', '/mnt/store1/video.bin');
INSERT INTO object_settings(_object, key, value) VALUES (3, 'CellSize', '16777216');
INSERT INTO object_settings(_object, key, value) VALUES (3, 'PageSize', '1048576');
INSERT INTO object_settings(_object, key, value) VALUES (3, 'Capacity', '200');
--- Camera --- (20, 2x)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Uri', 'Uri', 'Адрес видео-источника', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Login', 'Логин', 'Имя учётной записи для авторизации на видео-источнике', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Password', 'Пароль', 'Пароль учётной записи для авторизации на видео-источнике', 'password', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Module', 'Модуль', 'Модуль захвата RTSP', 'bool', 'live555', 'ffmpeg');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Transport', 'Транспорт', 'Транспорт передачи данных', 'bool', 'TCP', 'UDP');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Resolution', 'Разрешение', 'Разрешение USB камеры', 'resolution', '320x240', '1920×1080');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Decode', 'Декодирование', 'Декодирование видео (аппаратное декодирование будет использоваться только если оно поддерживается)', 'bool', 'программное', 'аппаратное');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Fps', 'Max К/с', 'Максимальное кол-во кадров в секунду для декодирования и обработки (0 - использовать все кадры)', 'int', '0', '100');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Uri', 'rtsp://');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Login', 'admin');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Password', 'admin');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Module', '0');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Transport', '0');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Fps', '0');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Decode', '1');
INSERT INTO object_settings(_object, key, value) VALUES (22, 'Uri', 'usb://0');
INSERT INTO object_settings(_object, key, value) VALUES (22, 'Resolution', '640x480');
INSERT INTO object_settings(_object, key, value) VALUES (22, 'Fps', '0');
INSERT INTO object_settings(_object, key, value) VALUES (23, 'Uri', 'file://');
INSERT INTO object_settings(_object, key, value) VALUES (23, 'Fps', '0');
INSERT INTO object_settings(_object, key, value) VALUES (23, 'Decode', '1');
INSERT INTO object_settings(_object, key, value) VALUES (24, 'Uri', 'v4l://-1');
INSERT INTO object_settings(_object, key, value) VALUES (24, 'Resolution', '1600x1200');
INSERT INTO object_settings(_object, key, value) VALUES (24, 'Fps', '');
--- Anal --- (3x, 3x)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (31, 'Standby', 'Режим ожидания', 'Переход в режим ожидания при недостаточной освещённости', 'bool', 'Нет', 'Да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (31, 'ManSize', 'Размер человека', 'Примерный размер человека в пикселях на сцене (ориентировочные значения 40, 80, 120, 160)', 'int', '40', '320');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (31, 'ThresholdSens', 'Чувствительность', 'Чувствительность алгоритма в процентах', 'int', '0', '100');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (31, 'StatCreate', 'Создание статистики', 'Создавать ежедневную статистику в указанное время', 'time', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (31, 'Standby', '0');
INSERT INTO object_settings(_object, key, value) VALUES (31, 'ManSize', '180');
INSERT INTO object_settings(_object, key, value) VALUES (31, 'ThresholdSens', '50');
INSERT INTO object_settings(_object, key, value) VALUES (31, 'StatCreate', '23:59');
--- Portal --- (6, 6)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Port', 'Порт', 'Порт, который будет использовать web-сервер портала', 'int', '1', '65535');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Name', 'Название', 'Название портала', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Scheme', 'Персонализация', 'Персонализация внешнего вида портала (одна заглавная латинская буква)', 'text', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Setup', 'Настройка', 'Отображать настройку камер', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Events', 'События', 'Отображать статистику детектируемых событий', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'States', 'Состояния', 'Отображать статистику детектируемых состояний', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Maps', 'Карты', 'Отображать статистические карты', 'bool', 'нет', 'да');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Port', '80');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Name', 'Портал СНА');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Scheme', 'A');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Setup', '0');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Events', '1');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'States', '0');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Maps', '1');
--- Unite --- (7, 7)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Master', 'Центральный', 'Центральный сервис объединения', 'bool', 'выкл', 'вкл');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Port', 'Порт', 'Порт для подключения локальных сервисов объединения', 'int', '1', '65535');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Slave', 'Локальный', 'Локальный сервис объединения', 'bool', 'выкл', 'вкл');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Uri', 'URI', 'URI подключения к центральному сервису объединения', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Period', 'Период', 'Период работы (сек.)', 'int', '1', '36000');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Slave', '0');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Uri', 'http://<server>:<port>');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Period', '60');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Master', '0');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Port', '8084');

--- Arm --- (5, 5)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (5, 'ScaleBest', 'Сохранять пропорции', 'Сохранять пропорции при выводе видео с камеры', 'bool', 'Нет', 'Да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (5, 'ShowMouse', 'Показывать мышь', 'Показывать мышь над окном вывода с камеры', 'bool', 'Нет', 'Да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (5, 'AutoHideMouse', 'Прятать мышь', 'Автоматически прятать мышь через короткий промежуток её не активности', 'bool', 'Нет', 'Да');
INSERT INTO object_settings(_object, key, value) VALUES (5, 'ScaleBest', '1');
INSERT INTO object_settings(_object, key, value) VALUES (5, 'ShowMouse', '1');
INSERT INTO object_settings(_object, key, value) VALUES (5, 'AutoHideMouse', '1');

--- Schedule --- (50, 51-52)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Dayly', 'Ежедневно', 'Ежедневный период работы', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly0', 'Понедельники', 'Еженедельный период работы в понедельник', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly1', 'Вторник', 'Еженедельный период работы во вторник', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly2', 'Среда', 'Еженедельный период работы в среду', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly3', 'Четверг', 'Еженедельный период работы в четверг', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly4', 'Пятница', 'Еженедельный период работы в пятницу', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly5', 'Суббота', 'Еженедельный период работы в субботу', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly6', 'Воскресенье', 'Еженедельный период работы в воскресенье', 'time_range', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (51, 'Dayly', '0:00-24:00');

INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly0', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly1', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly2', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly3', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly4', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly5', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly6', '0:00-24:00');

--- User --- (18, 18|28)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (18, 'UserLogin', 'Логин', 'Логин пользователя', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (18, 'UserPassword', 'Пароль', 'Пароль к логину пользователя', 'string', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (18, 'UserLogin', 'root');
INSERT INTO object_settings(_object, key, value) VALUES (18, 'UserPassword', 'root');

INSERT INTO object_settings(_object, key, value) VALUES (28, 'UserLogin', 'root');
INSERT INTO object_settings(_object, key, value) VALUES (28, 'UserPassword', 'root');

--- Update --- (19, 19)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpUri', 'URI', 'URI ресурса, с которого можно произвести обновление', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpLogin', 'Логин', 'Логин доступа к ресурсу', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpPass', 'Пароль', 'Пароль доступа к ресурсу', 'password', '', '');
--INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpPrior', 'Приоритет', 'Приоритет данной точки (1: min, 100: max)', 'int', '1', '100');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpPeriod', 'Период', 'Периодичность проверки наличия обновления', 'time_period', '1ms', '30d');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'Ver', 'Доступная версия', 'Доступная на текущий момент для обновления версия', 'string', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpUri', 'file:///mnt/vica/.info');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpLogin', '');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpPass', '');
--INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpPrior', '50');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpPeriod', '5000');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'Ver', 'нет');

--- Reporter --- (9, 9)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Dayly', 'Ежедневно', 'Создавать ежедневные отчёты', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'PeriodD', 'Часы', 'Временной промежуток, за который создавать отчёты', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Weekly', 'Еженедельно', 'Создавать еженедельные отчёты', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'PeriodW', 'Дни', 'Дни недели, за которые создавать еженедельные отчёты', 'weekday_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Start', 'Начало', 'Дата, с которой начинать отправку отчётов', 'date', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Events', 'События', 'Создавать отчёт по детектируемым событиям', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Reporter', 'Имя сервиса', 'Имя от которого сервис будет осуществлять рассылку', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'States', 'Состояния', 'Создавать отчёт по детектируемым состояниям', 'bool', 'нет', 'да');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Dayly', '1');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'PeriodD', '06:00-23:00');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Weekly', '1');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'PeriodW', 'пн-пт');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Start', '13-01-2013');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Events', '1');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Reporter', 'VICA');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'States', '1');

--- SMTP account --- (13, 13)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Uri', 'SMTP адрес', 'Адрес SMTP сервера (напр.: smtp.gmail.com)', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Port', 'SMTP порт', 'Порт SMTP сервера (стандартные: 25, 587, 465)', 'int', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Type', 'Безопасность', 'Уровень безопасности соединени', 'bool', 'SSL', 'нет');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Username', 'Имя пользователя', 'Имя пользователя (без домена)', 'text', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Userpass', 'Пароль пользователя', 'Пароль пользователя', 'password', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Domain', 'Домен почты', 'Домен пользователя, используемый на сервере)', 'uri', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Uri', 'smtp.gmail.com');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Port', '465');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Type', '0');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Username', '<user>');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Userpass', '<password>');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Domain', 'gmail.com');

--- RCPT account --- (14, 14)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (14, 'Username', 'Имя', 'Имя адресата', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (14, 'Usermail', 'E-mail', 'E-mail адресата', 'e-mail', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (14, 'Delay', 'Ограничение', 'Ограничение по интервалу между отправками писем (0 - без ограничений)', 'time_period', '0', '1d');
INSERT INTO object_settings(_object, key, value) VALUES (14, 'Username', 'Пользователь VICA');
INSERT INTO object_settings(_object, key, value) VALUES (14, 'Usermail', '<user@domain.org>');
INSERT INTO object_settings(_object, key, value) VALUES (14, 'Delay', '60000');


INSERT INTO variables(_object, key, value) VALUES (NULL, 'VicaVer', 1);

-- AUTO generated script --

DO $$
DECLARE
 version_new_ CONSTANT integer := 0;-- new version number --
 ver_ integer;
BEGIN
  LOCK TABLE variables;
  SELECT version_is('Base') INTO ver_;
  IF ver_ < version_new_ THEN
    -- begin update --

    -- end update --
    PERFORM version_set('Base', version_new_);
  END IF;
END$$;DO $$
DECLARE
 version_new_ CONSTANT integer := 0;-- new version number --
 ver_ integer;
BEGIN
  LOCK TABLE variables;
  SELECT version_is('Vica') INTO ver_;
  IF ver_ < version_new_ THEN
    -- begin update --

    -- end update --
    PERFORM version_set('Vica', version_new_);
  END IF;
END$$;
DO $$
DECLARE
 version_new_ CONSTANT integer := 2;-- new version number --
 ver_ integer;
BEGIN
  LOCK TABLE variables;
  SELECT version_is('Vica') INTO ver_;
  IF ver_ < version_new_ THEN
    -- begin update --
INSERT INTO object_type (_id, name, descr) VALUES (15, 'bak', 'Сервис резервного копирования');
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (15, 15, 'bak', 'Сервис резервного копирования БД', 'Сервис резервного копирования БД по умолчанию', 0, '', 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 15, 0);

INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (15, 'Path', 'Путь', 'Путь, по которому создавать инфраструктуру для резервного сохранения данных', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (15, 'Time', 'Время', 'Создание резервных копий в указанное время', 'time', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (15, 'Period', 'Период', 'Создание резервных копий с указанной периодичностью', 'time_period', '-300000', '86400000');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (15, 'AutoRestore', 'Автовосстановление', 'Автоматически восстанавливать систему, если обнаружена резервная копия другой системы (иначе в резервной копии надо установить флаг восстановления)', 'bool', 'Нет', 'Да');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'Path', './Backup');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'Time', '01:00');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'Period', '');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'UseServer', '1');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'AutoRestore', '1');

INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Cert', 'Сертификат', 'Использовать HTTPS с указанным сертификатом (парой <name>.crt <name>.key)', 'text', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'CertPass', 'Пароль', 'Пароль к сертификату (если сертификат указан)', 'pass', '', '');
PERFORM objects_add_setting (6, 'Cert', 'localhost');
PERFORM objects_add_setting (6, 'CertPass', '');
WITH obj AS (SELECT _id FROM object WHERE _otype = 6)
UPDATE object_settings AS s SET value = '443'
  FROM obj
  WHERE s._object = obj._id AND s.key = 'Port' AND s.value = '80';

    -- end update --
    PERFORM version_set('Vica', version_new_);
  END IF;
END$$;
DO $$
DECLARE
 version_new_ CONSTANT integer := 3;-- new version number --
 ver_ integer;
BEGIN
  LOCK TABLE variables;
  SELECT version_is('Vica') INTO ver_;
  IF ver_ < version_new_ THEN
    -- begin update --
INSERT INTO object_type (_id, name, descr) VALUES (32, 'vaa', 'Видеоаналитика автомобилей');
INSERT INTO object_type (_id, name, descr) VALUES (44, 'arn', 'Детектор гос.рег.номера');
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (32, 32, 'vaa', 'Видеоаналитика автомобилей', 'Анализ видеопотока для выявления и отслеживания автомобилей', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (44, 44, 'arn', 'Детектор гос.рег.номера', 'Регистрация автомобилей и их номеров', 0, '', 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 32, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 44, 0);

INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (32, 'Standby', 'Режим ожидания', 'Переход в режим ожидания при недостаточной освещённости', 'bool', 'Нет', 'Да');
INSERT INTO object_settings(_object, key, value) VALUES (32, 'Standby', '1');

    -- end update --
    PERFORM version_set('Vica', version_new_);
  END IF;
END$$;
DO $$
DECLARE
 version_new_ CONSTANT integer := 4;-- new version number --
 ver_ integer;
BEGIN
  LOCK TABLE variables;
  SELECT version_is('Vica') INTO ver_;
  IF ver_ < version_new_ THEN
    -- begin update --
ALTER TABLE event_log
   ADD COLUMN _file bigint;

ALTER TABLE event_log
  ADD CONSTRAINT event_log__file_fkey FOREIGN KEY (_file) REFERENCES files (_id)
   ON UPDATE CASCADE ON DELETE SET NULL;

INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) 
  VALUES (31, 'UseScreenshots', 'Скриншоты', 'Создавать скриншоты срабатывания детектора', 'bool', 'Нет', 'Да');
PERFORM objects_add_setting (31, 'UseScreenshots', '0');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) 
  VALUES (41, 'RemoveCount', 'Лимит событий', 'Удалять события свыше указанного лимита', 'int', '0', '1000000');
PERFORM objects_add_setting (41, 'RemoveCount', '10000');

    -- end update --
    PERFORM version_set('Vica', version_new_);
  END IF;
END$$;

-- End of Install script --
END TRANSACTION;
