--------------------
DO $$
BEGIN
  CREATE ROLE usr
    NOSUPERUSER INHERIT NOCREATEDB NOCREATEROLE NOREPLICATION;
  EXCEPTION WHEN OTHERS THEN
  RAISE NOTICE 'Role usr already exists';
END
$$;
--------------------
DO $$
BEGIN
  CREATE ROLE su
    NOSUPERUSER INHERIT CREATEDB NOCREATEROLE NOREPLICATION;
  EXCEPTION WHEN OTHERS THEN
  RAISE NOTICE 'Role su already exists';
END
$$;
--------------------
GRANT usr TO vica1;
--------------------
GRANT usr TO vica2;
GRANT su TO vica2;
--------------------
