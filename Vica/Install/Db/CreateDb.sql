CREATE DATABASE vicadb
  WITH OWNER = su
       ENCODING = 'UTF8'
       TABLESPACE = pg_default
       LC_COLLATE = 'C'
       LC_CTYPE = 'C'
       TEMPLATE template0
       CONNECTION LIMIT = -1;
