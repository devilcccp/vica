#!/bin/bash

export ProjName="Vica"
export ProjAbbr="vica"

ThisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export ProjDir="$ThisDir"/..
export HeadDir="$ThisDir"/../..
export ScriptDir="$HeadDir"/Misc/Debian
export PackDir="$ThisDir"/Update
export PackScript="$ThisDir"/Script


source $ScriptDir/Common.sh

/bin/bash "$ThisDir/!DbPrepare.sh" || Fatal

/bin/bash "$ScriptDir/MakeUpdate.sh" || Fatal

Success
