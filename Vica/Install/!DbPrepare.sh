#!/bin/bash

ThisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export ProjDir="$ThisDir/.."
export HeadDir="$ThisDir/../.."
export ScriptDir="$HeadDir/Misc/Debian"
export UtilsDir="$ThisDir/../../Utils"
script=DbPrepare


source $ScriptDir/Common.sh

if [ ! -d "$UtilsDir" ]; then
  echo "Utils dir not found"
  Fail
  exit 1
fi

dbPreparePath=""
if [ -x "$UtilsDir/bin/debug/$script" ]; then
  dbPreparePath="$UtilsDir/bin/debug/$script"
elif [ -x "$UtilsDir/bin/release/$script" ]; then
  dbPreparePath="$UtilsDir/bin/release/$script"
else
  printf "$script not found building ... "
  if [ ! -d "$UtilsDir/Script/build" ]; then
    mkdir "$UtilsDir/Script/build"
  fi
  qmake "$UtilsDir/Script/Script.pro" -o "$UtilsDir/Script/build" || Fail
  make -C "$UtilsDir/Script/build" >> /dev/null || Fail

  dbPreparePath="$UtilsDir/bin/release/$script"
  Ok
fi

"$dbPreparePath" "$pwd" 2>> /dev/null
