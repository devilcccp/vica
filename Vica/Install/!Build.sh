#!/bin/bash

export ProjName="Vica"
export ProjAbbr="vica"

ThisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
export ProjDir="$ThisDir/.."
export HeadDir="$ThisDir/../.."
export ScriptDir="$HeadDir/Misc/Debian"


source $ScriptDir/Common.sh

/bin/bash "$ScriptDir/Build.sh" || Fatal
Success
