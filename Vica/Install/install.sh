#!/bin/bash

(systemctl status vica_srvd 1,2>/dev/null) && (echo 'server ok')&&(systemctl status vica_upd 1,2>/dev/null) && (echo 'update ok') && exit 0

if [ -d "/opt/vica" ]; then
 dpkg -r Vica
 rm -r /opt/vica
fi

echo Starting VICA station installation

for debFile in /home/vicaadmin/vica_*.deb
do
 if [ ! -f "$debFile" ]; then
  echo "No installation package"
  exit 1
 fi
 echo "Found $debFile"
done

echo "Installing from $debFile"

dpkg -i $debFile || apt install -f || exit 2

echo "Installation complete removing $debFile"
rm $debFile
systemctl disable vica_install.service
echo "Installation complete"
