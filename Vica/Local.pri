APP_PREFIX = vica
DEFINES += \
    PROGRAM_ABBR=vica \
    MONITORING_EVENTS

PROJECT_NAME=Vica

EXCLUDE_LIB += \
    Media
INCLUDE_LIB += \
    Analytics \
    Backup \
    DbBackup \
    Reporter \
    Unite \
    Install

win32: gcc {
 EXCLUDE_LIB += \
    Mfx \
    Live555
}
