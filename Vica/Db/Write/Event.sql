﻿INSERT INTO event_type(name, descr, icon, flag) VALUES ('in', 'вход', 'man_in.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('out', 'выход', 'man_out.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('qstart', 'очередь', 'queue_on.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('qend', 'норма', 'queue_off.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('zone', 'в зоне', 'man_zone.png', 1);
INSERT INTO event_type(name, descr, icon, flag) VALUES ('queue', 'очередь', 'queue_on.png', 2);

INSERT INTO va_stat_type(abbr, name, descr) VALUES ('mov', 'Тепловые карты', 'Тепловые карты показывают области наиболее интенсивного движения. Каждый движущийся объект при перемещении как бы отдаёт тепло, которое накапливается и отображается на карте. Таким образом, по карте можно судить об областях наиболее интенсивного движения. Нужно так же учитывать, что зеркальные поверхности будут собирать отражённое в них движение.');
