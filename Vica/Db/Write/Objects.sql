﻿--------------------
DELETE FROM object;
DELETE FROM object_type;
--------------------
INSERT INTO object_type (_id, name, descr) VALUES (0, 'tmp', 'Шаблон');
INSERT INTO object_type (_id, name, descr) VALUES (1, 'srv', 'Сервер');
INSERT INTO object_type (_id, name, descr) VALUES (20, 'cam', 'Камера');
INSERT INTO object_type (_id, name, descr) VALUES (3, 'rep', 'Хранилище');
INSERT INTO object_type (_id, name, descr) VALUES (5, 'arm', 'АРМ оператора');
INSERT INTO object_type (_id, name, descr) VALUES (6, 'www', 'Web-Портал');
INSERT INTO object_type (_id, name, descr) VALUES (7, 'uni', 'Сервис объединения');
INSERT INTO object_type (_id, name, descr) VALUES (8, 'lis', 'Сервис лицензирования');
INSERT INTO object_type (_id, name, descr) VALUES (9, 'rpt', 'Сервис отчётов');
INSERT INTO object_type (_id, name, descr) VALUES (13, 'smp', 'SMTP аккаунт');
INSERT INTO object_type (_id, name, descr) VALUES (14, 'eml', 'Адресат рассылки');
INSERT INTO object_type (_id, name, descr) VALUES (50, 'sch', 'Расписание');

INSERT INTO object_type (_id, name, descr) VALUES (31, 'vac', 'Видеоаналитика посетителей');
INSERT INTO object_type (_id, name, descr) VALUES (41, 'iod', 'Детектор входа/выхода');
INSERT INTO object_type (_id, name, descr) VALUES (42, 'ioo', 'Область входа/выхода');
INSERT INTO object_type (_id, name, descr) VALUES (43, 'ign', 'Область игнорирования');

INSERT INTO object_type (_id, name, descr) VALUES (11, 'sr_', 'Внешний сервер');
INSERT INTO object_type (_id, name, descr) VALUES (12, 'ca_', 'Внешняя камера');

INSERT INTO object_type (_id, name, descr) VALUES (18, 'usr', 'Пользователь');
INSERT INTO object_type (_id, name, descr) VALUES (19, 'upd', 'Точка обновления');
SELECT setval('object_type__id_seq', 100);
--------------------
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (0, 0, 'tmp', 'Шаблоны', 'Корневой объект для всех шаблонов', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (1, 1, 'srv', 'Сервер', 'Сервер по умолчанию', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (21, 20, 'cam', 'Камера', 'Камера по умолчанию', 0, 'tcp::', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (22, 20, 'usb', 'USB веб-камера', 'Веб-камера по умолчанию', 0, 'tcp::', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (24, 20, 'v4l', 'USB Video4Linux', 'USB или встроенная камера через Video4Linux по умолчанию', 0, 'tcp::', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (23, 20, 'file', 'Видеофайл', 'Видеофайл по умолчанию', 0, 'tcp::', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (3, 3, 'rep', 'Хранилище', 'Хранилище по умолчанию', 0, '', -1);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (5, 5, 'arm', 'АРМ оператора', 'АРМ оператора по умолчанию', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (31, 31, 'vac', 'Аналитика посетителей', 'Анализ видеопотока для выявления и отслеживания людей', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (41, 41, 'iod', 'Счётчик посещений', 'Подсчёт входящих и выходящих посетителей', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (42, 42, 'ioo', 'Область входа/выхода', 'Область, из которой могут выходить и входить объекты наблюдения', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (43, 43, 'ign', 'Область игнорирования', 'Область, которая исключается из анализа', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (7, 7, 'uni', 'Сервис объединения', 'Сервис объединения локальных узлов к центральному', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (6, 6, 'www', 'Web-Портал', 'Web-Портал для взаимодействия с пользователями', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (9, 9, 'rpt', 'Сервис отчётов', 'Сервис формирования и отправки отчётов', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (13, 13, 'smp', 'SMTP аккаунт', 'SMTP аккаунт для отправки рассылки', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (14, 14, 'eml', 'Адресат рассылки', 'Адресат рассылки', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (18, 18, 'usr', 'Пользователь', 'Пользователь кластера', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (19, 19, 'upd', 'Точка обновления', 'Точка обновления по-умолчанию', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (51, 50, 'sch1', 'Ежедневное расписание', 'Расписание, одинаковое каждый день', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (52, 50, 'sch2', 'Еженедельное расписание', 'Расписание, одинаковое каждую неделю', 0, '', 0);

INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (28, 18, 'root', 'root', 'Администратор кластера', 0, '', 0);
SELECT setval('object__id_seq', 100);
--------------------
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 1, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 5, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 18, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 19, 0);

INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 51, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 52, 0);

INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 21, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 22, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 23, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 24, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 3, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 31, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 41, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 42, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 43, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 7, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 6, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 9, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 13, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 14, 0);
