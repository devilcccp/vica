﻿--------------------
DELETE FROM object_settings;
DELETE FROM object_settings_type;
--------------------

-- (obj_type, def_obj) --
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (0, 'Id', 'Ид', 'Идентификатор объекта', 'int', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (0, 'Name', 'Имя', 'Имя объекта', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (0, 'Descr', 'Описание', 'Описание объекта', 'string', '', '');
--- Server --- (1, 1)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (1, 'IP', 'IP адрес', 'IP адрес, под которым компоненты сервера будут доступны в системе', 'inet_address', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (1, 'Init', 'Инициализация', 'Инициализировать сервер стандартным набором сервисов', 'bool', 'Нет', 'Да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (1, 'Quiet', 'Молчун', 'Сервер и его компоненты не будут обновлять своё состояние в БД', 'bool', 'Нет', 'Да');
INSERT INTO object_settings(_object, key, value) VALUES (1, 'IP', '');
INSERT INTO object_settings(_object, key, value) VALUES (1, 'Init', '1');
INSERT INTO object_settings(_object, key, value) VALUES (1, 'Quiet', '1');
--- Store --- (3, 3)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (3, 'Path', 'Путь', 'Путь к объекту файловой системы, это может быть как файл, так и ссылка на диск', 'path', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (3, 'CellSize', 'Кластер', 'Размер ячейки хранилища', 'size', '16777216', '16777216');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (3, 'PageSize', 'Страница', 'Минимальный размер данных, для чтения/записи хранилища', 'size', '1048576', '1048576');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (3, 'Capacity', 'Размер хранилища', 'Размер хранилища в кластерах (как правило имеет смысл указывать только максимальное значение)', 'size', '200', 'max');
INSERT INTO object_settings(_object, key, value) VALUES (3, 'Path', '/mnt/store1/video.bin');
INSERT INTO object_settings(_object, key, value) VALUES (3, 'CellSize', '16777216');
INSERT INTO object_settings(_object, key, value) VALUES (3, 'PageSize', '1048576');
INSERT INTO object_settings(_object, key, value) VALUES (3, 'Capacity', '200');
--- Camera --- (20, 2x)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Uri', 'Uri', 'Адрес видео-источника', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Login', 'Логин', 'Имя учётной записи для авторизации на видео-источнике', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Password', 'Пароль', 'Пароль учётной записи для авторизации на видео-источнике', 'password', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Module', 'Модуль', 'Модуль захвата RTSP', 'bool', 'live555', 'ffmpeg');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Transport', 'Транспорт', 'Транспорт передачи данных', 'bool', 'TCP', 'UDP');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Resolution', 'Разрешение', 'Разрешение USB камеры', 'resolution', '320x240', '1920×1080');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Decode', 'Декодирование', 'Декодирование видео (аппаратное декодирование будет использоваться только если оно поддерживается)', 'bool', 'программное', 'аппаратное');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (20, 'Fps', 'Max К/с', 'Максимальное кол-во кадров в секунду для декодирования и обработки (0 - использовать все кадры)', 'int', '0', '100');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Uri', 'rtsp://');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Login', 'admin');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Password', 'admin');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Module', '0');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Transport', '0');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Fps', '0');
INSERT INTO object_settings(_object, key, value) VALUES (21, 'Decode', '1');
INSERT INTO object_settings(_object, key, value) VALUES (22, 'Uri', 'usb://0');
INSERT INTO object_settings(_object, key, value) VALUES (22, 'Resolution', '640x480');
INSERT INTO object_settings(_object, key, value) VALUES (22, 'Fps', '0');
INSERT INTO object_settings(_object, key, value) VALUES (23, 'Uri', 'file://');
INSERT INTO object_settings(_object, key, value) VALUES (23, 'Fps', '0');
INSERT INTO object_settings(_object, key, value) VALUES (23, 'Decode', '1');
INSERT INTO object_settings(_object, key, value) VALUES (24, 'Uri', 'v4l://-1');
INSERT INTO object_settings(_object, key, value) VALUES (24, 'Resolution', '1600x1200');
INSERT INTO object_settings(_object, key, value) VALUES (24, 'Fps', '');
--- Anal --- (3x, 3x)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (31, 'Standby', 'Режим ожидания', 'Переход в режим ожидания при недостаточной освещённости', 'bool', 'Нет', 'Да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (31, 'ManSize', 'Размер человека', 'Примерный размер человека в пикселях на сцене (ориентировочные значения 40, 80, 120, 160)', 'int', '40', '320');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (31, 'ThresholdSens', 'Чувствительность', 'Чувствительность алгоритма в процентах', 'int', '0', '100');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (31, 'StatCreate', 'Создание статистики', 'Создавать ежедневную статистику в указанное время', 'time', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (31, 'Standby', '0');
INSERT INTO object_settings(_object, key, value) VALUES (31, 'ManSize', '180');
INSERT INTO object_settings(_object, key, value) VALUES (31, 'ThresholdSens', '50');
INSERT INTO object_settings(_object, key, value) VALUES (31, 'StatCreate', '23:59');
--- Portal --- (6, 6)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Port', 'Порт', 'Порт, который будет использовать web-сервер портала', 'int', '1', '65535');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Name', 'Название', 'Название портала', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Scheme', 'Персонализация', 'Персонализация внешнего вида портала (одна заглавная латинская буква)', 'text', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Setup', 'Настройка', 'Отображать настройку камер', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Events', 'События', 'Отображать статистику детектируемых событий', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'States', 'Состояния', 'Отображать статистику детектируемых состояний', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Maps', 'Карты', 'Отображать статистические карты', 'bool', 'нет', 'да');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Port', '80');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Name', 'Портал СНА');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Scheme', 'A');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Setup', '0');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Events', '1');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'States', '0');
INSERT INTO object_settings(_object, key, value) VALUES (6, 'Maps', '1');
--- Unite --- (7, 7)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Master', 'Центральный', 'Центральный сервис объединения', 'bool', 'выкл', 'вкл');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Port', 'Порт', 'Порт для подключения локальных сервисов объединения', 'int', '1', '65535');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Slave', 'Локальный', 'Локальный сервис объединения', 'bool', 'выкл', 'вкл');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Uri', 'URI', 'URI подключения к центральному сервису объединения', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (7, 'Period', 'Период', 'Период работы (сек.)', 'int', '1', '36000');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Slave', '0');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Uri', 'http://<server>:<port>');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Period', '60');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Master', '0');
INSERT INTO object_settings(_object, key, value) VALUES (7, 'Port', '8084');

--- Arm --- (5, 5)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (5, 'ScaleBest', 'Сохранять пропорции', 'Сохранять пропорции при выводе видео с камеры', 'bool', 'Нет', 'Да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (5, 'ShowMouse', 'Показывать мышь', 'Показывать мышь над окном вывода с камеры', 'bool', 'Нет', 'Да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (5, 'AutoHideMouse', 'Прятать мышь', 'Автоматически прятать мышь через короткий промежуток её не активности', 'bool', 'Нет', 'Да');
INSERT INTO object_settings(_object, key, value) VALUES (5, 'ScaleBest', '1');
INSERT INTO object_settings(_object, key, value) VALUES (5, 'ShowMouse', '1');
INSERT INTO object_settings(_object, key, value) VALUES (5, 'AutoHideMouse', '1');

--- Schedule --- (50, 51-52)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Dayly', 'Ежедневно', 'Ежедневный период работы', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly0', 'Понедельники', 'Еженедельный период работы в понедельник', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly1', 'Вторник', 'Еженедельный период работы во вторник', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly2', 'Среда', 'Еженедельный период работы в среду', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly3', 'Четверг', 'Еженедельный период работы в четверг', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly4', 'Пятница', 'Еженедельный период работы в пятницу', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly5', 'Суббота', 'Еженедельный период работы в субботу', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (50, 'Weekly6', 'Воскресенье', 'Еженедельный период работы в воскресенье', 'time_range', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (51, 'Dayly', '0:00-24:00');

INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly0', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly1', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly2', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly3', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly4', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly5', '0:00-24:00');
INSERT INTO object_settings(_object, key, value) VALUES (52, 'Weekly6', '0:00-24:00');

--- User --- (18, 18|28)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (18, 'UserLogin', 'Логин', 'Логин пользователя', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (18, 'UserPassword', 'Пароль', 'Пароль к логину пользователя', 'string', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (18, 'UserLogin', 'root');
INSERT INTO object_settings(_object, key, value) VALUES (18, 'UserPassword', 'root');

INSERT INTO object_settings(_object, key, value) VALUES (28, 'UserLogin', 'root');
INSERT INTO object_settings(_object, key, value) VALUES (28, 'UserPassword', 'root');

--- Update --- (19, 19)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpUri', 'URI', 'URI ресурса, с которого можно произвести обновление', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpLogin', 'Логин', 'Логин доступа к ресурсу', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpPass', 'Пароль', 'Пароль доступа к ресурсу', 'password', '', '');
--INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpPrior', 'Приоритет', 'Приоритет данной точки (1: min, 100: max)', 'int', '1', '100');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'UpPeriod', 'Период', 'Периодичность проверки наличия обновления', 'time_period', '1ms', '30d');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (19, 'Ver', 'Доступная версия', 'Доступная на текущий момент для обновления версия', 'string', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpUri', 'file:///mnt/vica/.info');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpLogin', '');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpPass', '');
--INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpPrior', '50');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'UpPeriod', '5000');
INSERT INTO object_settings(_object, key, value) VALUES (19, 'Ver', 'нет');

--- Reporter --- (9, 9)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Dayly', 'Ежедневно', 'Создавать ежедневные отчёты', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'PeriodD', 'Часы', 'Временной промежуток, за который создавать отчёты', 'time_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Weekly', 'Еженедельно', 'Создавать еженедельные отчёты', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'PeriodW', 'Дни', 'Дни недели, за которые создавать еженедельные отчёты', 'weekday_range', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Start', 'Начало', 'Дата, с которой начинать отправку отчётов', 'date', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Events', 'События', 'Создавать отчёт по детектируемым событиям', 'bool', 'нет', 'да');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'Reporter', 'Имя сервиса', 'Имя от которого сервис будет осуществлять рассылку', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (9, 'States', 'Состояния', 'Создавать отчёт по детектируемым состояниям', 'bool', 'нет', 'да');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Dayly', '1');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'PeriodD', '06:00-23:00');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Weekly', '1');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'PeriodW', 'пн-пт');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Start', '13-01-2013');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Events', '1');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'Reporter', 'VICA');
INSERT INTO object_settings(_object, key, value) VALUES (9, 'States', '1');

--- SMTP account --- (13, 13)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Uri', 'SMTP адрес', 'Адрес SMTP сервера (напр.: smtp.gmail.com)', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Port', 'SMTP порт', 'Порт SMTP сервера (стандартные: 25, 587, 465)', 'int', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Type', 'Безопасность', 'Уровень безопасности соединени', 'bool', 'SSL', 'нет');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Username', 'Имя пользователя', 'Имя пользователя (без домена)', 'text', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Userpass', 'Пароль пользователя', 'Пароль пользователя', 'password', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (13, 'Domain', 'Домен почты', 'Домен пользователя, используемый на сервере)', 'uri', '', '');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Uri', 'smtp.gmail.com');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Port', '465');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Type', '0');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Username', '<user>');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Userpass', '<password>');
INSERT INTO object_settings(_object, key, value) VALUES (13, 'Domain', 'gmail.com');

--- RCPT account --- (14, 14)
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (14, 'Username', 'Имя', 'Имя адресата', 'string', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (14, 'Usermail', 'E-mail', 'E-mail адресата', 'e-mail', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (14, 'Delay', 'Ограничение', 'Ограничение по интервалу между отправками писем (0 - без ограничений)', 'time_period', '0', '1d');
INSERT INTO object_settings(_object, key, value) VALUES (14, 'Username', 'Пользователь VICA');
INSERT INTO object_settings(_object, key, value) VALUES (14, 'Usermail', '<user@domain.org>');
INSERT INTO object_settings(_object, key, value) VALUES (14, 'Delay', '60000');


INSERT INTO variables(_object, key, value) VALUES (NULL, 'VicaVer', 1);
