﻿DO $$
DECLARE
 version_new_ CONSTANT integer := 2;-- new version number --
 ver_ integer;
BEGIN
  LOCK TABLE variables;
  SELECT version_is('Vica') INTO ver_;
  IF ver_ < version_new_ THEN
    -- begin update --
INSERT INTO object_type (_id, name, descr) VALUES (15, 'bak', 'Сервис резервного копирования');
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (15, 15, 'bak', 'Сервис резервного копирования БД', 'Сервис резервного копирования БД по умолчанию', 0, '', 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 15, 0);

INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (15, 'Path', 'Путь', 'Путь, по которому создавать инфраструктуру для резервного сохранения данных', 'uri', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (15, 'Time', 'Время', 'Создание резервных копий в указанное время', 'time', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (15, 'Period', 'Период', 'Создание резервных копий с указанной периодичностью', 'time_period', '-300000', '86400000');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (15, 'AutoRestore', 'Автовосстановление', 'Автоматически восстанавливать систему, если обнаружена резервная копия другой системы (иначе в резервной копии надо установить флаг восстановления)', 'bool', 'Нет', 'Да');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'Path', './Backup');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'Time', '01:00');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'Period', '');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'UseServer', '1');
INSERT INTO object_settings(_object, key, value) VALUES (15, 'AutoRestore', '1');

INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'Cert', 'Сертификат', 'Использовать HTTPS с указанным сертификатом (парой <name>.crt <name>.key)', 'text', '', '');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (6, 'CertPass', 'Пароль', 'Пароль к сертификату (если сертификат указан)', 'pass', '', '');
PERFORM objects_add_setting (6, 'Cert', 'localhost');
PERFORM objects_add_setting (6, 'CertPass', '');
WITH obj AS (SELECT _id FROM object WHERE _otype = 6)
UPDATE object_settings AS s SET value = '443'
  FROM obj
  WHERE s._object = obj._id AND s.key = 'Port' AND s.value = '80';

    -- end update --
    PERFORM version_set('Vica', version_new_);
  END IF;
END$$;
