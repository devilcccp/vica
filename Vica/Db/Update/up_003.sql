﻿DO $$
DECLARE
 version_new_ CONSTANT integer := 3;-- new version number --
 ver_ integer;
BEGIN
  LOCK TABLE variables;
  SELECT version_is('Vica') INTO ver_;
  IF ver_ < version_new_ THEN
    -- begin update --
INSERT INTO object_type (_id, name, descr) VALUES (32, 'vaa', 'Видеоаналитика автомобилей');
INSERT INTO object_type (_id, name, descr) VALUES (44, 'arn', 'Детектор гос.рег.номера');
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (32, 32, 'vaa', 'Видеоаналитика автомобилей', 'Анализ видеопотока для выявления и отслеживания автомобилей', 0, '', 0);
INSERT INTO object(_id, _otype, guid, name, descr, revision, uri, status) VALUES (44, 44, 'arn', 'Детектор гос.рег.номера', 'Регистрация автомобилей и их номеров', 0, '', 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 32, 0);
INSERT INTO object_connection(_omaster, _oslave, type) VALUES (0, 44, 0);

INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) VALUES (32, 'Standby', 'Режим ожидания', 'Переход в режим ожидания при недостаточной освещённости', 'bool', 'Нет', 'Да');
INSERT INTO object_settings(_object, key, value) VALUES (32, 'Standby', '1');

    -- end update --
    PERFORM version_set('Vica', version_new_);
  END IF;
END$$;
