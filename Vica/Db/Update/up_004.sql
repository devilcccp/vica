﻿DO $$
DECLARE
 version_new_ CONSTANT integer := 4;-- new version number --
 ver_ integer;
BEGIN
  LOCK TABLE variables;
  SELECT version_is('Vica') INTO ver_;
  IF ver_ < version_new_ THEN
    -- begin update --
ALTER TABLE event_log
   ADD COLUMN _file bigint;

ALTER TABLE event_log
  ADD CONSTRAINT event_log__file_fkey FOREIGN KEY (_file) REFERENCES files (_id)
   ON UPDATE CASCADE ON DELETE SET NULL;

INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) 
  VALUES (31, 'UseScreenshots', 'Скриншоты', 'Создавать скриншоты срабатывания детектора', 'bool', 'Нет', 'Да');
PERFORM objects_add_setting (31, 'UseScreenshots', '0');
INSERT INTO object_settings_type(_otype, key, name, descr, type, min_value, max_value) 
  VALUES (41, 'RemoveCount', 'Лимит событий', 'Удалять события свыше указанного лимита', 'int', '0', '1000000');
PERFORM objects_add_setting (41, 'RemoveCount', '10000');

    -- end update --
    PERFORM version_set('Vica', version_new_);
  END IF;
END$$;
