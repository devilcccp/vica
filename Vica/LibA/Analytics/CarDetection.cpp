#include "CarDetection.h"
#include "ImgAnalizer.h"


void CarDetection::LoadSettings(const SettingsAS& settings)
{
  Q_UNUSED(settings);
}

void CarDetection::SaveSettings(const SettingsAS& settings)
{
  Q_UNUSED(settings);
}

void CarDetection::Init(const ImageSrc<uchar>& source)
{
  Q_UNUSED(source);
}

void CarDetection::Analize(const ImageSrc<uchar>& source)
{
  mImgAnalizer->Init(source.Data(0, 0), source.Width(), source.Height(), source.Stride());
  mImgAnalizer->MakeUinPre();
}

void CarDetection::GetDbgUinPre(ImageSrc<uchar>& debug)
{
  Region<uchar> debugRegion;
  mImgAnalizer->GetUinPre(debugRegion);

  uchar maxValue = 0;
  for (int j = 0; j < debugRegion.Height(); j++) {
    const uchar* src = debugRegion.Line(j);
    for (int i = 0; i < debugRegion.Width(); i++) {
      maxValue = qMax(maxValue, *src);

      src++;
    }
  }

  uchar v1 = qMax(maxValue/2, 1);
  uchar v2 = qMax(maxValue/4, 1);
  uchar v3 = qMax(maxValue/8, 1);

  for (int j = 0; j < Height(); j++) {
    const uchar* src = debugRegion.Line(j);
    uchar* dbg = debug.Line(j);
    for (int i = 0; i < Width(); i++) {
      if (*src > 0) {
        if (*src > v1) {
          *dbg = 4;
        } else if (*src > v2) {
          *dbg = 3;
        } else if (*src > v3) {
          *dbg = 2;
        } else {
          *dbg = 1;
        }
      } else {
        *dbg = 0;
      }

      src++;
      dbg++;
    }
  }

  QRect rect;
  while (mImgAnalizer->NextUinPreRect(rect)) {
    debug.FillRect(4, rect);
  }
}


CarDetection::CarDetection(const AnalyticsB& _Analytics)
  : BlockSceneAnalizer(_Analytics)
  , mImgAnalizer(new ImgAnalizer())
{
}

