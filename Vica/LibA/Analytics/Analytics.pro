!include(../Common.pri) {
  error(Could not find the Common.pri file!)
}

QT += gui

CONFIG(debug, debug|release) {
 QT += widgets
 DEFINES += ANAL_DEBUG
}

SOURCES += \
    AnalyticsB.cpp \
    MacroMotion.cpp \
    DiffLayers.cpp \
    BlockObj.cpp \
    DlWorker.cpp \
    CarMotion.cpp \
    ImgAnalizer.cpp \
    CarDetection.cpp \
    CarTracker.cpp \
    Uin/Uin.cpp \
    Uin/UinPre.cpp

HEADERS += \
    AnalyticsB.h \
    Hyst.h \
    MacroMotion.h \
    DiffLayers.h \
    BlockScene.h \
    BlockSceneAnalizer.h \
    BlockObj.h \
    PointHst.h \
    DlWorker.h \
    Analytics.h \
    CarMotion.h \
    ImgAnalizer.h \
    CarDetection.h \
    CarTracker.h \
    CarDef.h \
    Uin/Uin.h \
    Uin/UinPre.h

LIBS += \
    -lVa \
    -lSettings \
    -lCommon \
    -lDb \
    -lLog

RESOURCES += \
    $$PROJECT_DIR/Local/Video.qrc \
    Analytics.qrc
