!include(../Common.pri) {
  error(Could not find the Common.pri file!)
}


QT += sql network

SOURCES += \
    ReporterV.cpp \
    Main.cpp

HEADERS += \
    ReporterV.h

DEPEND_LIBS = \
    Reporter \
    Dispatcher \
    Ctrl \
    Smtp \
    Net \
    Db \
    Settings \
    Log

!include($$PRI_DIR/Dependencies.pri) {
  error(Could not find the Dependencies.pri file!)
}

TARGET = $${APP_PREFIX}_report$$APP_EXTRA_EXTANTION

RESOURCES += \
    Reporter.qrc
