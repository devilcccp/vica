#pragma once

#include <QString>

#include <Lib/Reporter/ReporterA.h>
#include <Lib/Db/Db.h>


DefineClassS(ReporterV);
DefineClassS(Db);

class ReporterV: public ReporterA
{
  const Db&    mDb;
  ObjectTableS mObjectTable;
  EventTableS  mEventTable;

  QByteArray   mReportHeader;
  QByteArray   mReportTrailer;

  enum EType {
    eEvents,
    eStates
  };
  bool         mEvents;
  bool         mStates;

  typedef QPair<qreal, qreal> StatOne;
  typedef QMap<QDateTime, StatOne> StatMap;
  typedef QMap<int, StatMap> StatAll;
  bool         mGroupToDate;
  QByteArray*  mReportData;
  StatAll      mStatAll;

protected:
  /*override */virtual bool LoadSettings(SettingsA* settings) Q_DECL_OVERRIDE;

  /*override */virtual bool InitReport(QDateTime& startTime) Q_DECL_OVERRIDE;
  /*override */virtual bool CreateReport(EPeriodic periodic, const Ranges& ranges, QByteArray& reportData, QList<FilesS>& reportFiles) Q_DECL_OVERRIDE;

private:
  bool ReportRange(EType type, const Ranges& ranges);

  bool AddReportObject(int id);
  bool AddReportTime(const StatMap& timeMap, const QString& timeFormat);
  void GroupDay(const StatMap& timeMap, StatMap& dateMap);

public:
  ReporterV(const Db& _Db);
  /*override */virtual ~ReporterV();
};

