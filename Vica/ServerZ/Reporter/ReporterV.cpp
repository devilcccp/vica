#include <QLocale>

#include <Lib/Db/ObjectType.h>
#include <Lib/Db/Event.h>
#include <Lib/Dispatcher/Overseer.h>
#include <Lib/Log/Log.h>

#include "ReporterV.h"


bool ReporterV::LoadSettings(SettingsA* settings)
{
  if (!ReporterA::LoadSettings(settings)) {
    return false;
  }

  mEvents = settings->GetValue("Events", true).toBool();
  mStates = settings->GetValue("States", true).toBool();
  return true;
}

bool ReporterV::InitReport(QDateTime& startTime)
{
  startTime.setDate(QDate(2080, 0, 0));
  if (mEvents) {
    QDateTime ts;
    if (!mEventTable->GetFirstEventTs(ts)) {
      return false;
    }
    startTime = ts;
  }
  if (mStates) {
    QDateTime ts;
    if (!mEventTable->GetFirstStateTs(ts)) {
      return false;
    }
    if (!startTime.isValid() || ts < startTime) {
      startTime = ts;
    }
  }
  return true;
}

bool ReporterV::CreateReport(ReporterA::EPeriodic periodic, const ReporterA::Ranges& ranges, QByteArray& reportData, QList<FilesS>& reportFiles)
{
  Q_UNUSED(reportFiles);

  mGroupToDate = periodic == ReporterA::eWeekly;
  mReportData = &reportData;
  mReportData->clear();

  mReportData->append(mReportHeader);
  if (mEvents) {
    if (!ReportRange(eEvents, ranges)) {
      return false;
    }
  }
  if (mStates) {
    if (!ReportRange(eStates, ranges)) {
      return false;
    }
  }
  mReportData->append(mReportTrailer);
  return true;
}

bool ReporterV::ReportRange(EType type, const ReporterA::Ranges& ranges)
{
  mStatAll.clear();
  auto q = mDb.MakeQuery();
  const char* query = (type == eEvents)?
        "SELECT e._object, h.triggered_hour, MAX(h.value)"
        " FROM event_log_hours h"
        " INNER JOIN event e ON e._id = h._event"
        " WHERE triggered_hour >= ? AND triggered_hour < ?"
        " GROUP BY e._object, h.triggered_hour"
      :
        "SELECT e._object, s.triggered_hour, s.value, s.period"
        " FROM event_stat_hours s"
        " INNER JOIN event e ON e._id = s._event"
        " WHERE triggered_hour >= ? AND triggered_hour < ?";
  q->prepare(query);

  for (auto itr = ranges.begin(); itr != ranges.end(); itr++) {
    const ReporterA::Range& range = *itr;
    q->bindValue(0, range.From);
    q->bindValue(1, range.To);
    if (!mDb.ExecuteQuery(q)) {
      return false;
    }

    while (q->next()) {
      int       id = q->value(0).toInt();
      QDateTime ts = q->value(1).toDateTime();
      qreal     v1 = q->value(2).toReal();
      qreal     v2 = (type == eStates)? q->value(3).toReal(): 0;

      mStatAll[id][ts].first  = v1;
      mStatAll[id][ts].second = v2;
    }
  }

  QString typeText = (type == eEvents)? "События": "Состояния";
  mReportData->append(QString("<h2>%1 за отчётный период</h2>").arg(typeText).toUtf8());
  if (mStatAll.isEmpty()) {
    mReportData->append(QString("<p>%1 за отчётный период не зарегистрированы</p>").arg(typeText).toUtf8());
  }
  for (auto itr = mStatAll.begin(); itr != mStatAll.end(); itr++) {
    int id = itr.key();
    if (!AddReportObject(id)) {
      return false;
    }
    const StatMap& timeMap = itr.value();
    if (mGroupToDate) {
      StatMap dateMap;
      GroupDay(timeMap, dateMap);
      AddReportTime(dateMap, "dd(ddd)-MMM");
    } else {
      AddReportTime(timeMap, "dd-MMM hh");
    }
  }
  return true;
}

bool ReporterV::AddReportObject(int id)
{
  TableItemS item = mObjectTable->GetItem(id);
  const ObjectItem* obj = static_cast<const ObjectItem*>(item.data());
  if (!obj) {
    return false;
  }
  mReportData->append(QString("<h3>Детектор \"%1\"</h3>").arg(obj->Name).toUtf8());
  return true;
}

bool ReporterV::AddReportTime(const StatMap& timeMap, const QString& timeFormat)
{
  mReportData->append("<div class=\"results\" id=\"results\">\n<table>\n"
                      " <thead><tr style=\"height:30px\"><th>Время</th><th>Значение</th></tr></thead>\n"
                      " <tbody>\n");

  QLocale locale = QLocale(QLocale::Russian, QLocale::RussianFederation);
  for (auto itr = timeMap.begin(); itr != timeMap.end(); itr++) {
    const QDateTime& ts = itr.key();
    const StatOne&   v  = itr.value();
    QString     tsText = locale.toString(ts, timeFormat).toUtf8();
    QByteArray valText = (v.second)? QByteArray::number(v.first / v.second, 'f', 2): QByteArray::number(v.first, 'f', 0);
    mReportData->append("\n<tr><td>" + tsText + "</td><td>" + valText + "</td></tr>");
  }
  mReportData->append("\n</tbody></table>\n</div>\n");
  return true;
}

void ReporterV::GroupDay(const StatMap& timeMap, StatMap& dateMap)
{
  for (auto itr = timeMap.begin(); itr != timeMap.end(); itr++) {
    const QDateTime& ts = itr.key();
    const StatOne&   v  = itr.value();
    QDateTime newTs(ts.date(), QTime(0, 0));
    dateMap[newTs].first  += v.first;
    dateMap[newTs].second += v.second;
  }
}


ReporterV::ReporterV(const Db& _Db)
  : ReporterA(_Db)
  , mDb(_Db), mObjectTable(new ObjectTable(mDb)), mEventTable(new EventTable(mDb))
{
  Q_INIT_RESOURCE(Reporter);

  QFile file1(":/Src/Header.txt");
  if (file1.open(QFile::ReadOnly)) {
    mReportHeader = file1.readAll();
  }
  QFile file2(":/Src/Trailer.txt");
  if (file2.open(QFile::ReadOnly)) {
    mReportTrailer = file2.readAll();
  }
}

ReporterV::~ReporterV()
{
}
