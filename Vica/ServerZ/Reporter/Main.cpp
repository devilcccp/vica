#include <QSettings>

#include <Lib/Include/QtAppCon.h>
#include <Lib/Db/Db.h>
#include <Lib/Dispatcher/Overseer.h>
#include <Local/ModuleNames.h>

#include "ReporterV.h"


int qmain(int argc, char* argv[])
{
  DbS db;
  OverseerS overseer;
  if (int ret = Overseer::ParseMainWithDb(kServerDaemon, argc, argv, overseer, db)) {
    return ret;
  }

  ReporterVS reporter(new ReporterV(*db));
  overseer->RegisterWorker(reporter);

  return overseer->Run();
}

