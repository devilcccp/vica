!include(../Common.pri) {
  error(Could not find the Common.pri file!)
}

QT += sql network

SOURCES += \
    ServerLoader.cpp \
    Main.cpp

HEADERS += \
    ServerLoader.h

DEPEND_LIBS = \
    Ctrl \
    Dispatcher \
    Settings \
    Db \
    Log

!include($$PRI_DIR/Dependencies.pri) {
  error(Could not find the Dependencies.pri file!)
}

DAEMON_NAME = srv
DAEMON_PATH = $$PWD
!include($$PRI_DIR/Daemon.pri) {
  error(Could not find the Daemon.pri file!)
}

RC_FILE = Resource.rc
