#pragma once

#include <QList>

#include <Lib/Dispatcher/ModuleLoaderO.h>


class ServerLoader: public ModuleLoaderO
{
  bool mVeryQuiet;

  int  mObjTemplSchId;
  int  mObjTemplCamId;
  int  mObjTemplV4lId;
  int  mObjTemplVaId;
  int  mObjTemplIodId;
  int  mObjTemplWebId;
  int  mObjTemplBakId;

protected:
  /*override */virtual void SetDbScheme() Q_DECL_OVERRIDE;

  /*override */virtual bool InitializeExtra() Q_DECL_OVERRIDE;

  /*override */virtual bool IsNeedIp() Q_DECL_OVERRIDE;

private:
  bool SppInit();
  bool CameraInit(int index, bool v4l);
  bool WebInit();
  bool BakInit();

public:
  ServerLoader(SettingsAS& _Settings);
};

