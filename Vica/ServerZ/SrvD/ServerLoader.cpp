#include <Lib/Log/Log.h>
#include <Lib/Db/ObjectType.h>
#include <Lib/Settings/DbSettings.h>
#include <Local/ModuleNames.h>

#include "ServerLoader.h"


void ServerLoader::SetDbScheme()
{
  QStringList defaultParams;
  if (mVeryQuiet) {
    defaultParams << "--quiet";
  }
  RegisterDbModule("cam", kVideoAExe, 0, defaultParams);
  RegisterDbModule("rep", kStoreCreatorExe, 1);
  RegisterDbModule("www", kPortalExe, 0, defaultParams);
  RegisterDbModule("rpt", kReporterExe, 0, defaultParams);
  RegisterDbModule("uni", kUniteExe, 0, defaultParams);
  RegisterDbModule("bak", kBackupExe, 0, defaultParams);
}

bool ServerLoader::InitializeExtra()
{
  mVeryQuiet = getDbSettings()->GetValue("Quiet", true).toBool();
  setVeryQuiet(mVeryQuiet);

  bool init = getDbSettings()->GetValue("Init", true).toBool();
  if (init) {
    if (!SppInit()) {
      return false;
    }
    getDbSettings()->SetValue("Init", false);
    getDbSettings()->Sync();
  }

  return ModuleLoaderO::InitializeExtra();
}

bool ServerLoader::IsNeedIp()
{
  return true;
}

bool ServerLoader::SppInit()
{
  QList<int> slaves;
  if (!getObjectTable()->LoadSlaves(MainObject()->Id, slaves)) {
    return false;
  }
  foreach (int id, slaves) {
    if (!getObjectTable()->RemoveItem(id)) {
      return false;
    }
  }

  if (!getObjectTable()->GetObjectIdByGuid("sch2", mObjTemplSchId)) {
    return false;
  }
  if (!getObjectTable()->GetObjectIdByGuid("cam", mObjTemplCamId)) {
    return false;
  }
  if (!getObjectTable()->GetObjectIdByGuid("v4l", mObjTemplV4lId)) {
    Log.Warning(QString("Video4Linux template camera not found"));
  }
  if (!getObjectTable()->GetObjectIdByGuid("vac", mObjTemplVaId)) {
    return false;
  }
  if (!getObjectTable()->GetObjectIdByGuid("iod", mObjTemplIodId)) {
    return false;
  }
  if (!getObjectTable()->GetObjectIdByGuid("www", mObjTemplWebId)) {
    return false;
  }
  if (!getObjectTable()->GetObjectIdByGuid("bak", mObjTemplBakId)) {
    return false;
  }

  if (!mObjTemplCamId || !mObjTemplVaId || !mObjTemplIodId || !mObjTemplWebId || !mObjTemplBakId) {
    Log.Fatal("One of mandatory templates missed, init abort");
    return true;
  }

  return CameraInit(1, false) && CameraInit(2, true) && WebInit() && BakInit();
}

bool ServerLoader::CameraInit(int index, bool v4l)
{
  int camTemplId = v4l && mObjTemplV4lId? mObjTemplV4lId: mObjTemplCamId;
  int camId;
  if (!getObjectTable()->CreateSlaveObject(MainObject()->Id, camTemplId, QString("Камера %1").arg(index), &camId)) {
    Log.Warning(QString("Create camera %1 fail").arg(index));
    return false;
  }
  Log.Info(QString("Created camera %1").arg(index));

  if (mObjTemplSchId) {
    if (!getObjectTable()->CreateSlaveObject(camId, mObjTemplSchId, QString("Расписание %1").arg(index))) {
      Log.Warning(QString("Create schedule %1 fail").arg(index));
      return false;
    }
    Log.Info(QString("Created schedule %1").arg(index));
  }

  int vaId;
  if (!getObjectTable()->CreateSlaveObject(camId, mObjTemplVaId, QString("Аналитика %1").arg(index), &vaId)) {
    Log.Warning(QString("Create va %1 fail").arg(index));
    return false;
  }
  Log.Info(QString("Created va %1").arg(index));

  if (!getObjectTable()->CreateSlaveObject(vaId, mObjTemplIodId, QString("Счётчик %1").arg(index))) {
    Log.Warning(QString("Create in out %1 fail").arg(index));
    return false;
  }
  Log.Info(QString("Created in out %1").arg(index));

  return true;
}

bool ServerLoader::WebInit()
{
  int wwwId;
  if (!getObjectTable()->CreateSlaveObject(MainObject()->Id, mObjTemplWebId, QString("Портал"), &wwwId)) {
    Log.Warning(QString("Create www fail"));
    return false;
  }
  Log.Info(QString("Created www"));

  DbSettings wwwSettings(*getDb());
  if (!wwwSettings.Open(wwwId)) {
    return false;
  }

  wwwSettings.SetValue("Setup", 1);
  return wwwSettings.Sync();
}

bool ServerLoader::BakInit()
{
  if (!getObjectTable()->CreateSlaveObject(MainObject()->Id, mObjTemplBakId, QString("Сервис резервного копирования"))) {
    Log.Warning(QString("Create backup fail"));
    return false;
  }
  Log.Info(QString("Created backup"));

  return true;
}


ServerLoader::ServerLoader(SettingsAS &_Settings)
  : ModuleLoaderO(_Settings, "srv", 20000)
  , mVeryQuiet(true)
  , mObjTemplV4lId(0)
{
}
