#include <QSharedPointer>

#include <Lib/Include/QtAppService.h>
#include <Lib/Dispatcher/Dispatcher.h>
#include <Lib/Settings/FileSettings.h>
#include <Local/ModuleNames.h>

#include "ServerLoader.h"


bool ServiceInit(QString& _ServiceName, QString& _ServiceViewname, QString& _ServiceDescription)
{
  _ServiceName        = kServerDaemon;
  _ServiceViewname    = GetServerViewName();
  _ServiceDescription = GetServerDescription();
  return true;
}

DispatcherS CreateServiceDispatcher()
{
  SettingsAS settings(new FileSettings());
  if (!static_cast<FileSettings*>(settings.data())->OpenLocal()) {
    Log.Fatal("No settings");
    return DispatcherS();
  }

  return DispatcherS(new DispatcherDaemon<ServerLoader>(kServerDaemon, settings));
}
