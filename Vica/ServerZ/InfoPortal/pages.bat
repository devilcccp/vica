SET head=%1
SET head=%head:"=%
SET dst=%2
SET dst=%dst:"=%
SET dst=%dst:/=\%
SET cert=localhost.crt localhost.key
SET pages=template_ru.html template_en.html common.js jquery.js jquery-ui.js jquery.timepicker.min.js jquery.timepicker.css
SET images=excel.png man_in.png man_out.png man_zone.png queue_off.png queue_on.png
SET scheme_icons=favicon.png main_logo1.png sync.png vs.png vsrm.png style.css
SET scheme_jquery=jquery-ui.css jquery-ui.min.css jquery-ui.theme.css
SET scheme_jquery_img=ui-bg_flat_75_aaaaaa_40x100.png ui-bg_glass_100_f5f0e5_1x400.png ui-bg_glass_25_cb842e_1x400.png ui-bg_glass_70_ede4d4_1x400.png ui-bg_highlight-hard_100_f4f0ec_1x100.png ui-bg_highlight-hard_65_fee4bd_1x100.png ui-bg_highlight-hard_75_f5f5b5_1x100.png ui-bg_inset-soft_100_f4f0ec_1x100.png ui-icons_c47a23_256x240.png ui-icons_cb672b_256x240.png ui-icons_f08000_256x240.png ui-icons_f35f07_256x240.png ui-icons_ff7519_256x240.png ui-icons_ffffff_256x240.png


IF NOT EXIST "%dst%/Src" MD "%dst%/Src"
IF NOT EXIST "%dst%/Src/images" MD "%dst%/Src/images"
CALL "%head%/Lib/Include/deploy.bat" "src" "%dst%/Src" "%cert% %pages% %images%"

FOR %%s IN (SchemeA) DO (
 IF NOT EXIST "%dst%/Src/%%s" MD "%dst%/Src/%%s"
 IF NOT EXIST "%dst%/Src/%%s/images" MD "%dst%/Src/%%s/images"
 CALL "%head%/Lib/Include/deploy.bat" "src/%%s" "%dst%/Src/%%s" "%scheme_icons% %scheme_jquery%"
 CALL "%head%/Lib/Include/deploy.bat" "src/%%s/images" "%dst%/Src/%%s/images" "%scheme_jquery_img%"
)
