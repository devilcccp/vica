<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>QObject</name>
    <message>
        <location filename="PortalHandler.cpp" line="396"/>
        <location filename="PortalHandler.cpp" line="446"/>
        <location filename="PortalHandler.cpp" line="622"/>
        <location filename="PortalHandler.cpp" line="791"/>
        <source>Db connection failed</source>
        <translation>Ошибка подключения к БД</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="401"/>
        <location filename="PortalHandler.cpp" line="451"/>
        <location filename="PortalHandler.cpp" line="796"/>
        <source>Load cameras list failed</source>
        <translation>Ошибка получения списка камер</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="458"/>
        <location filename="PortalHandler.cpp" line="1470"/>
        <source>Camera %1</source>
        <translation>Камера %1</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="463"/>
        <location filename="PortalHandler.cpp" line="812"/>
        <source>Camera not found</source>
        <translation>Камера не найдена</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="639"/>
        <source>Load detectors list failed</source>
        <translation>Ошибка получения списка счётчиков</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="673"/>
        <source>Load data failed</source>
        <translation>Ошибка получения данных</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="787"/>
        <source>Camera bad index</source>
        <translation>Ошибочный индекс камеры</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="819"/>
        <source>Camera enable failed</source>
        <translation>Ошибка включения камеры</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="822"/>
        <source>Camera enabled; </source>
        <translation>Камера включена; </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="822"/>
        <source>Camera disabled; </source>
        <translation>Камера выключена; </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="831"/>
        <location filename="PortalHandler.cpp" line="854"/>
        <source>Camera setup failed; </source>
        <translation>Ошибка настройки камеры; </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="857"/>
        <source>Camera setup success; </source>
        <translation>Камера перенастроена; </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="861"/>
        <location filename="PortalHandler.cpp" line="877"/>
        <source>Schedule setup failed; </source>
        <translation>Ошибка настройки расписания; </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="880"/>
        <source>Schedule setup success; </source>
        <translation>Расписание перенастроено; </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="885"/>
        <location filename="PortalHandler.cpp" line="901"/>
        <source>Setup barrier failed; </source>
        <translation>Ошибка настройки барьера; </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="904"/>
        <source>Setup barrier success; </source>
        <translation>Барьер перенастроен; </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1055"/>
        <source>&lt;th&gt;Event&lt;/th&gt;&lt;th&gt;Time&lt;/th&gt;</source>
        <translation>&lt;th&gt;Событие&lt;/th&gt;&lt;th&gt;Время&lt;/th&gt;</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1057"/>
        <location filename="PortalHandler.cpp" line="1128"/>
        <location filename="PortalHandler.cpp" line="1213"/>
        <source>Update</source>
        <translation>Обновить</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1061"/>
        <source>Event;Time</source>
        <translation>Событие;Время</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1079"/>
        <location filename="PortalHandler.cpp" line="1084"/>
        <source>&lt;unknown&gt;</source>
        <translation>&lt;неизвестно&gt;</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1119"/>
        <source>&lt;th&gt;Time&lt;/th&gt;&lt;th&gt;Value&lt;/th&gt;</source>
        <translation>&lt;th&gt;Время&lt;/th&gt;&lt;th&gt;Значение&lt;/th&gt;</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1122"/>
        <source>&lt;th&gt;Period </source>
        <translation>&lt;th&gt;Период </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1123"/>
        <source>&lt;/th&gt;&lt;th&gt;Value&lt;/th&gt;&lt;th&gt;Difference&lt;/th&gt;&lt;th&gt;Percent&lt;/th&gt;</source>
        <translation>&lt;/th&gt;&lt;th&gt;Значение&lt;/th&gt;&lt;th&gt;Разница&lt;/th&gt;&lt;th&gt;Процент&lt;/th&gt;</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1126"/>
        <source>Export to csv</source>
        <translation>Экспортировать в csv</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1132"/>
        <source>Time;Value</source>
        <translation>Время;Значение</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1134"/>
        <source>;Period </source>
        <translation>;Период </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1134"/>
        <source>;Value;Difference;Percent</source>
        <translation>;Значение;Разница;Процент</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1211"/>
        <source>&lt;th&gt;State&lt;/th&gt;&lt;th&gt;Detector&lt;/th&gt;&lt;th&gt;Value&lt;/th&gt;</source>
        <translation>&lt;th&gt;Состояние&lt;/th&gt;&lt;th&gt;Детектор&lt;/th&gt;&lt;th&gt;Значение&lt;/th&gt;</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1217"/>
        <source>State;Detector;Value</source>
        <translation>Состояние;Детектор;Значение</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1253"/>
        <source>&lt;th&gt;Time&lt;/th&gt;&lt;th&gt;Map&lt;/th&gt;</source>
        <translation>&lt;th&gt;Время&lt;/th&gt;&lt;th&gt;Карта&lt;/th&gt;</translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1255"/>
        <source>&lt;th&gt;Time </source>
        <translation>&lt;th&gt;Время </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1255"/>
        <source>&lt;/th&gt;&lt;th&gt;Map </source>
        <translation>&lt;/th&gt;&lt;th&gt;Карта </translation>
    </message>
    <message>
        <location filename="PortalHandler.cpp" line="1479"/>
        <location filename="PortalHandler.cpp" line="1489"/>
        <location filename="PortalHandler.cpp" line="1497"/>
        <source>Detector %1</source>
        <translation>Детектор %1</translation>
    </message>
</context>
</TS>
