#pragma once

#include <QMutex>
#include <QElapsedTimer>
#include <QMap>
#include <QString>

#include <Lib/Db/Db.h>
#include <Lib/Settings/SettingsA.h>
#include <Lib/NetServer/HandlerManager.h>


DefineClassS(Overseer);
typedef QMap<QString, QPair<QString, QDateTime> > CredentialMap;

class Portal: public HandlerManager
{
  const QString    mName;
  Overseer*        mOverseer;
  bool             mUseSetup;
  bool             mUseEvents;
  bool             mUseStates;
  bool             mUseMaps;

  DbS              mDb;
  ObjectTypeTableS mObjectTypeTable;
  ObjectTableS     mObjectTable;
  EventTypeTableS  mEventTypeTable;
  VaStatTypeTableS mVaStatTypeTable;
  QMutex           mDbMutex;
  QElapsedTimer    mRefreshTimer;
  CredentialMap    mUserAuth;
  int              mUserTypeId;
  int              mCameraTypeId;
  int              mScheduleTypeId;
  int              mIodTypeId;

  QByteArray       mTemplatePageEn;
  QByteArray       mTemplateAboutEn;
  QByteArray       mTemplateHomeEn;
  QByteArray       mTemplateSetupEn;
  QByteArray       mTemplateEvents1En;
  QByteArray       mTemplateEvents2En;
  QByteArray       mTemplateEvents3En;
  QByteArray       mTemplateEventsEndEn;
  QByteArray       mTemplateStatesEn;
  QByteArray       mTemplateStatesEndEn;
  QByteArray       mTemplateMapsEn;
  QByteArray       mTemplateMapEn;

  QByteArray       mTemplatePageRu;
  QByteArray       mTemplateAboutRu;
  QByteArray       mTemplateHomeRu;
  QByteArray       mTemplateSetupRu;
  QByteArray       mTemplateEvents1Ru;
  QByteArray       mTemplateEvents2Ru;
  QByteArray       mTemplateEvents3Ru;
  QByteArray       mTemplateEventsEndRu;
  QByteArray       mTemplateStatesRu;
  QByteArray       mTemplateStatesEndRu;
  QByteArray       mTemplateMapsRu;
  QByteArray       mTemplateMapRu;

public:
  const QString& Name() const { return mName; }
  int GetUserType()     const { return mUserTypeId; }
  int GetCameraType()   const { return mCameraTypeId; }
  int GetScheduleType() const { return mScheduleTypeId; }
  int GetIodType()       const { return mIodTypeId; }

  bool UseEvents() const { return mUseEvents; }
  bool UseStates() const { return mUseStates; }
  bool UseMaps()   const { return mUseMaps; }
  const QByteArray& TemplatePage(bool ru)    { return ru? mTemplatePageRu: mTemplatePageEn; }
  const QByteArray& TemplateAbout(bool ru)   { return ru? mTemplateAboutRu: mTemplateAboutEn; }
  const QByteArray& TemplateHome(bool ru)    { return ru? mTemplateHomeRu: mTemplateHomeEn; }
  const QByteArray& TemplateSetup(bool ru)   { return ru? mTemplateSetupRu: mTemplateSetupEn; }
  const QByteArray& TemplateEvents1(bool ru) { return ru? mTemplateEvents1Ru: mTemplateEvents1En; }
  const QByteArray& TemplateEvents2(bool ru) { return ru? mTemplateEvents2Ru: mTemplateEvents2En; }
  const QByteArray& TemplateEvents3(bool ru) { return ru? mTemplateEvents3Ru: mTemplateEvents3En; }
  const QByteArray& TemplateEventsEnd(bool ru) { return ru? mTemplateEventsEndRu: mTemplateEventsEndEn; }
  const QByteArray& TemplateStates(bool ru)    { return ru? mTemplateStatesRu: mTemplateStatesEn; }
  const QByteArray& TemplateStatesEnd(bool ru) { return ru? mTemplateStatesEndRu: mTemplateStatesEndEn; }
  const QByteArray& TemplateMaps(bool ru)      { return ru? mTemplateMapsRu: mTemplateMapsEn; }
  const QByteArray& TemplateMap(bool ru)       { return ru? mTemplateMapRu: mTemplateMapEn; }

protected:
  /*override */virtual HandlerS NewHandler();

public:
  CtrlManager* GetManager();
  int GetPortalId();

  const ObjectItem* GetObject(int detectorId);
  const EventType* GetEventType(int eventTypeId);
  bool Authorized(const QByteArray& auth);
  void SelectLanguage(const QByteArray& auth);

private:
  void Refresh();
  bool LoadTextResource(const QByteArray& path, QByteArray& text);

public:
  Portal(DbS _Db, const QString& _Name, Overseer* _Overseer, SettingsA* _Settings);
  /*override */virtual ~Portal();
};
