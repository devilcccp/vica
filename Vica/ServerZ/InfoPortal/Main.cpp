#include <QSettings>
#include <QSslConfiguration>
#include <QSslKey>
#include <QSslCertificate>

#include <Lib/Include/QtAppCon.h>
#include <Lib/Db/Db.h>
#include <Lib/Settings/DbSettings.h>
#include <Lib/Dispatcher/OverseerState.h>
#include <Lib/NetServer/NetServer.h>
#include <Local/ModuleNames.h>

#include "Portal.h"
#include "PortalHandler.h"


typedef HandlerManagerC<PortalHandler, Portal> PortalManager;
typedef QSharedPointer<PortalManager> PortalManagerS;

int qmain(int argc, char* argv[])
{
  DbS db;
  OverseerStateS overseer;
  if (int ret = OverseerState::ParseMainWithDb(kServerDaemon, argc, argv, overseer, db)) {
    return ret;
  }

  DbSettings webSettings(*db);
  if (!webSettings.Open(QString::number(overseer->Id()))) {
    return -2100;
  }

  int port = webSettings.GetValue("Port", 8080).toInt();
  QString _Name       = webSettings.GetValue("Name", QString::fromUtf8("Портал СНА")).toString();
  QString certPath    = webSettings.GetValue("Cert", "test").toString();
  QByteArray certPass = webSettings.GetValue("CertPass", "").toString().toUtf8();
  QString schemeType  = webSettings.GetValue("Scheme", "A").toString();
  QString schemeName  = QString("Scheme%1").arg(schemeType);
  if (!QFile::exists(qApp->applicationDirPath() + "/Src/" + schemeName)) {
    return -2201;
  }

  int copy = 0;
  int fail = 0;
  QStringList allFiles = QString("favicon.png style.css jquery-ui.css jquery-ui.min.css jquery-ui.theme.css main_logo1.png sync.png vs.png vsrm.png").split(' ');
  for (auto itr = allFiles.begin(); itr != allFiles.end(); itr++) {
    const QString& filename = *itr;
    QString srcFilepath = qApp->applicationDirPath() + "/Src/" + schemeName + "/" + filename;
    QString dstFilepath = qApp->applicationDirPath() + "/Src/" + filename;
    QFile::remove(dstFilepath);
    QFile::copy(srcFilepath, dstFilepath)? copy++: fail++;
  }

  QStringList imgFiles = QString("ui-bg_flat_75_aaaaaa_40x100.png ui-bg_glass_100_f5f0e5_1x400.png ui-bg_glass_25_cb842e_1x400.png"
                                 " ui-bg_glass_70_ede4d4_1x400.png ui-bg_highlight-hard_100_f4f0ec_1x100.png"
                                 " ui-bg_highlight-hard_65_fee4bd_1x100.png ui-bg_highlight-hard_75_f5f5b5_1x100.png"
                                 " ui-bg_inset-soft_100_f4f0ec_1x100.png ui-icons_c47a23_256x240.png"
                                 " ui-icons_cb672b_256x240.png ui-icons_f08000_256x240.png ui-icons_f35f07_256x240.png"
                                 " ui-icons_ff7519_256x240.png ui-icons_ffffff_256x240.png").split(' ');
  for (auto itr = imgFiles.begin(); itr != imgFiles.end(); itr++) {
    const QString& filename = *itr;
    QString srcFilepath = qApp->applicationDirPath() + "/Src/" + schemeName + "/images/" + filename;
    QString dstFilepath = qApp->applicationDirPath() + "/Src/images/" + filename;
    QFile::remove(dstFilepath);
    QFile::copy(srcFilepath, dstFilepath)? copy++: fail++;
  }
  Log.Info(QString("Use scheme %1 (copy: %2, fail: %3)").arg(schemeName).arg(copy).arg(fail));

  Portal portal(db, _Name, overseer.data(), &webSettings);
  NetServerS server = NetServerS(new NetServer(port, PortalManagerS(new PortalManager(&portal))));
  QSslConfiguration sslConfiguration;
  if (!certPath.isEmpty()) {
    QFile sslKeyFile(qApp->applicationDirPath() + "/Src/" + certPath + ".key");
    QFile sslCertFile(qApp->applicationDirPath() + "/Src/" + certPath + ".crt");
    if (sslKeyFile.open(QFile::ReadOnly) && sslCertFile.open(QFile::ReadOnly)) {
      QByteArray sslKeyData = sslKeyFile.readAll();
      QByteArray sslCertData = sslCertFile.readAll();
      Log.Info(QString("Using SSL key (path: '%1', size: %2)").arg(sslKeyFile.fileName()).arg(sslKeyData.size()));
      Log.Info(QString("Using SSL cert (path: '%1', size: %2)").arg(sslCertFile.fileName()).arg(sslCertData.size()));
      QSslKey sslKey(sslKeyData, QSsl::Rsa, QSsl::Pem, QSsl::PrivateKey, certPass);
      QSslCertificate sslCert(sslCertData, QSsl::Pem);
      sslConfiguration.setPeerVerifyMode(QSslSocket::VerifyNone);
      sslConfiguration.setLocalCertificate(sslCert);
      sslConfiguration.setPrivateKey(sslKey);
      sslConfiguration.setProtocol(QSsl::TlsV1SslV3);
      server->SetSsl(&sslConfiguration);
    } else {
      Log.Warning(QString("SSL key/cert not found ('%1'/'%2')").arg(sslKeyFile.fileName()).arg(sslCertFile.fileName()));
    }
  }

  if (!overseer->Quiet()) {
    server->SetNotifier(overseer->GetNotifier());
  }
  overseer->RegisterWorker(server);

  return overseer->Run();
}

