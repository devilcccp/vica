!include(../Common.pri) {
  error(Could not find the Common.pri file!)
}

QT += network sql

SOURCES += \
    Main.cpp \
    PortalHandler.cpp \
    Portal.cpp

HEADERS += \
    PortalHandler.h \
    Portal.h

DEPEND_LIBS = \
    Dispatcher \
    Ctrl \
    NetServer \
    Net \
    Db \
    Settings \
    Log

!include($$PRI_DIR/Dependencies.pri) {
  error(Could not find the Dependencies.pri file!)
}

TARGET = $${APP_PREFIX}_portal$$APP_EXTRA_EXTANTION

OTHER_FILES += \
    src/navBase.txt \
    src/navEvents.txt \
    src/navMapOne.txt \
    src/navMaps.txt \
    src/navSetup.txt \
    src/navStates.txt \
    src/pageAbout.txt \
    src/pageEvents.txt \
    src/pageHome.txt \
    src/pageMap.txt \
    src/pageMapOne.txt \
    src/pageMaps.txt \
    src/pageSetup.txt \
    src/pageStates.txt

win32 {
  QMAKE_POST_LINK  = $$PWD/pages.bat $$shell_quote($$HEAD_DIR) $$shell_quote($$DESTDIR)

 OTHER_FILES += \
    pages.bat
} linux {
  QMAKE_POST_LINK  = /bin/bash $$PWD/pages.sh $$shell_quote($$HEAD_DIR) $$shell_quote($$DESTDIR)

 OTHER_FILES += \
    pages.sh
}

TRANSLATIONS += Portal_ru.ts
#"/usr/lib/qt5/bin/lrelease" './InfoPortal.pro'

RESOURCES += \
    PageFrames.qrc

