#!/bin/bash

head=$1
dst=$2
src="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cert="localhost.crt localhost.key"
pages="template_ru.html template_en.html common_ru.js common_en.js jquery.js jquery-ui.js jquery.timepicker.min.js jquery.timepicker.css"
images="excel.png man_in.png man_out.png man_zone.png queue_off.png queue_on.png rus.png eng.png"
scheme_icons="favicon.png main_logo1.png sync.png vs.png vsrm.png style.css"
scheme_jquery="jquery-ui.css jquery-ui.min.css jquery-ui.theme.css"
scheme_jquery_img="ui-bg_flat_75_aaaaaa_40x100.png ui-bg_glass_100_f5f0e5_1x400.png ui-bg_glass_25_cb842e_1x400.png ui-bg_glass_70_ede4d4_1x400.png ui-bg_highlight-hard_100_f4f0ec_1x100.png ui-bg_highlight-hard_65_fee4bd_1x100.png ui-bg_highlight-hard_75_f5f5b5_1x100.png ui-bg_inset-soft_100_f4f0ec_1x100.png ui-icons_c47a23_256x240.png ui-icons_cb672b_256x240.png ui-icons_f08000_256x240.png ui-icons_f35f07_256x240.png ui-icons_ff7519_256x240.png ui-icons_ffffff_256x240.png"


if [ ! -d "$dst/Src" ]; then
 mkdir "$dst/Src"
fi
if [ ! -d "$dst/Src/images" ]; then
 mkdir "$dst/Src/images"
fi
/bin/bash "$head/Lib/Include/deploy.sh" "$src/src" "$dst/Src" "$cert $pages $images"

for scheme in SchemeA
do
 /bin/bash "$head/Lib/Include/deploy.sh" "$src/src/$scheme" "$dst/Src/$scheme" "$scheme_icons $scheme_jquery"
 /bin/bash "$head/Lib/Include/deploy.sh" "$src/src/$scheme/images" "$dst/Src/$scheme/images" "$scheme_jquery_img"
done

