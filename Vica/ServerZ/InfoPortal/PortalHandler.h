#pragma once

#include <QFile>
#include <QVector>

#include <Lib/Db/Db.h>
#include <Lib/NetServer/HttpHandler.h>
#include <Lib/NetServer/NetServer.h>


DefineClassS(PortalHandler);
DefineClassS(Portal);
class QTranslator;

class PortalHandler: public HttpHandler
{
  static const QByteArray kHomeBody;
  static const QByteArray kAboutBody;
  static const QByteArray kEventBody1;
  static const QByteArray kEventBody2;
  static const QByteArray kEventBody3;

  struct CameraInfo {
    int     Id;
    QString Name;
    QString Uri;
    int     State;
    int     Schedule;
    int     Barier;
  };

  struct DetectorInfo {
    int     Id;
    QString Name;
    int     Flag;
  };

  enum EStatType {
    eEventLog  = 0x1 << 0,
    eEventStat = 0x1 << 1,
    eEventMap = 0x1 << 2
  };

  DbS                   mDb;
  FilesTableS           mFilesTable;
  Portal*               mPortal;

  int                   mUtc;
  int                   mSelectedMap;
  QVector<DetectorInfo> mLogDetectors;
  QVector<DetectorInfo> mStatDetectors;
  QVector<DetectorInfo> mStatMaps;
  QVector<CameraInfo>   mCameras;

  bool                  mIsRu;
  QByteArray            mLanguage;
  QTranslator*          mEnTranslator;
  QTranslator*          mRuTranslator;

protected:
  /*override */virtual bool Get(const QString& path, const QList<QByteArray>& params) Q_DECL_OVERRIDE;
  /*override */virtual bool Post(const QString& path, const QList<QByteArray>& params, const QList<File>& files) Q_DECL_OVERRIDE;
//  /*override */virtual void OnDisconnected() Q_DECL_OVERRIDE;

private:
  bool Authorize();

  bool LoadFile(const QString& filename, const char* contentType, int cache = 360);
  bool LoadSnapshot(const QString& path);
  bool LoadDbFile(const qint64& id);

  bool OpenTemplatePage(const QByteArray& body);
  bool OpenHome(const QList<QByteArray>& params);
  bool OpenCameraSetup(int index, const QList<QByteArray>& params = QList<QByteArray>());
  bool OpenEvents(const QList<QByteArray>& params, EStatType statType, int hours);
  bool OpenMaps();
  bool OpenAbout();

  bool ChangeOptions(const QList<QByteArray>& params);

  bool LoadCameras();
  bool LoadCamerasEx();
  bool LoadDetectors();
  bool LoadAnals();
  bool AddLogs(bool exportExcel, int iodId, const QDateTime& ts1, const QDateTime& ts2, QByteArray& result);
  bool AddLogStats(EStatType statType, bool exportExcel, int iodId, const QList<int>& iodIds, const QDateTime& ts1
                   , int periodh, const QList<QDateTime>& ts4, int hours, QByteArray& result);
  bool AddStats(bool exportExcel, QByteArray& result);
  bool AddMaps(int iodId, const QList<int>& iodIds, const QDateTime& ts1
               , int periodh, const QList<QDateTime>& ts4, QByteArray& result);
  bool GetPeriodLogs(int iodId, const QDateTime& ts1, int periodh, int hours, QVector<qreal>& values);
  bool GetPeriodStatLogs(int iodId, const QDateTime& ts1, int periodh, int hours, QVector<qreal>& values);
  bool GetPeriodMaps(int iodId, const QDateTime& ts1, int periodh, QVector<qint64>& values);
  void GetCamCombo(QByteArray& combo);
  void GetIodCombo(QByteArray& combo);
  void GetStateCombo(QByteArray& combo);
  void GetMapCombo(QByteArray& combo);

  bool OpenDb();
  void SelectLanguage();
  bool TakeLanguage(QString& path);
  void ApplyLanguage(bool isRu);
  QDateTime ReadDateTimeParam(const QByteArray& text);

public:
  PortalHandler(Portal* _Portal);
  /*override */virtual ~PortalHandler();
};
