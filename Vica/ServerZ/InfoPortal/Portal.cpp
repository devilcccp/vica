#include <QMutexLocker>

#include <Lib/Db/ObjectType.h>
#include <Lib/Db/Event.h>
#include <Lib/Db/VaStatType.h>
#include <Lib/Log/Log.h>
#include <Lib/Dispatcher/Overseer.h>

#include "Portal.h"
#include "PortalHandler.h"


const qint64 kAutoRefreshPeriodMs = 60000;

HandlerS Portal::NewHandler()
{
  return HandlerS(new PortalHandler(this));
}

CtrlManager* Portal::GetManager()
{
  return mOverseer;
}

int Portal::GetPortalId()
{
  return mOverseer->Id();
}

const ObjectItem* Portal::GetObject(int detectorId)
{
  QMutexLocker lock(&mDbMutex);
  Refresh();
  return static_cast<const ObjectItem*>(mObjectTable->GetItem(detectorId).data());
}

const EventType* Portal::GetEventType(int eventTypeId)
{
  QMutexLocker lock(&mDbMutex);
  Refresh();
  return static_cast<const EventType*>(mEventTypeTable->GetItem(eventTypeId).data());
}

bool Portal::Authorized(const QByteArray& auth)
{
  QByteArray authBase64;
  if (auth.startsWith("Basic")) {
    authBase64 = auth.mid(5).trimmed();
  } else {
    authBase64 = auth;
  }
  QList<QByteArray> userPass = QByteArray::fromBase64(authBase64).split(':');
  if (userPass.size() == 2) {
    QString name = QString::fromUtf8(userPass[0]);
    QString pass = QString::fromUtf8(userPass[1]);
    auto itr = mUserAuth.find(name);
    if (itr != mUserAuth.end()) {
      if (pass == itr.value().first && QDateTime::currentDateTime() < itr.value().second) {
        return true;
      }
    }
    Log.Info(QString("Authorize user (login: '%1', pass: '%2')").arg(name).arg(pass));
    QMutexLocker lock(&mDbMutex);
    const ObjectItem* objItem = static_cast<const ObjectItem*>(mObjectTable->GetItemByName(name));
    if (objItem && objItem->Type == mUserTypeId && pass == mObjectTable->GetSetting(objItem->Id, "UserPassword")) {
      mUserAuth[name] = qMakePair(pass, QDateTime::currentDateTime().addSecs(5 * 60));
      return true;
    }
  }
  return false;
}

void Portal::Refresh()
{
  if (mRefreshTimer.elapsed() > kAutoRefreshPeriodMs) {
    mObjectTable->Reload();
    mEventTypeTable->Reload();
    mRefreshTimer.restart();
  }
}

bool Portal::LoadTextResource(const QByteArray& path, QByteArray& text)
{
  QFile pageAbout(path);
  if (pageAbout.open(QFile::ReadOnly)) {
    text = pageAbout.readAll();
    return true;
  }
  return false;
}


Portal::Portal(DbS _Db, const QString& _Name, Overseer* _Overseer, SettingsA* _Settings)
  : mName(_Name), mOverseer(_Overseer), mDb(_Db)
  , mObjectTypeTable(new ObjectTypeTable(*_Db)), mObjectTable(new ObjectTable(*_Db)), mEventTypeTable(new EventTypeTable(*_Db))
  , mVaStatTypeTable(new VaStatTypeTable(*_Db))
  , mCameraTypeId(0), mScheduleTypeId(0), mIodTypeId(0)
{
  mObjectTypeTable->Reload();
  if (const NamedItem* usrItem = mObjectTypeTable->GetItemByName("usr")) {
    mUserTypeId = usrItem->Id;
  } else {
    Log.Fatal(QString("User type Id not loaded"), true);
  }
  if (const NamedItem* item = mObjectTypeTable->GetItemByName("cam")) {
    mCameraTypeId = item->Id;
  } else {
    Log.Fatal(QString("Camera type Id not loaded"), true);
  }
  if (const NamedItem* item = mObjectTypeTable->GetItemByName("sch")) {
    mScheduleTypeId = item->Id;
  } else {
    Log.Error(QString("Schedule type Id not loaded"));
  }
  if (const NamedItem* item = mObjectTypeTable->GetItemByName("iod")) {
    mIodTypeId = item->Id;
  } else {
    Log.Error(QString("Iod type Id not loaded"));
  }
  mObjectTable->Reload();
  mEventTypeTable->Reload();
  mRefreshTimer.start();
  mUseSetup  = _Settings->GetValue("Setup", false).toBool();
  mUseEvents = _Settings->GetValue("Events", true).toBool();
  mUseStates = _Settings->GetValue("States", true).toBool();
  mUseMaps   = _Settings->GetValue("Maps", true).toBool();

  LoadTextResource(":/strings/src/pageAbout_en.txt", mTemplateAboutEn);
  LoadTextResource(":/strings/src/pageHome_en.txt",  mTemplateHomeEn);
  LoadTextResource(":/strings/src/pageSetup_en.txt",  mTemplateSetupEn);

  LoadTextResource(":/strings/src/pageAbout_ru.txt", mTemplateAboutRu);
  LoadTextResource(":/strings/src/pageHome_ru.txt",  mTemplateHomeRu);
  LoadTextResource(":/strings/src/pageSetup_ru.txt",  mTemplateSetupRu);

  QByteArray events;
  LoadTextResource(":/strings/src/pageEvents_en.txt", events);
  int p1 = events.indexOf("%%%");
  int p2 = events.indexOf("%%%", p1 + 3);
  int p3 = events.indexOf("%%%", p2 + 3);
  mTemplateEvents1En = events.mid(0, p1);
  mTemplateEvents2En = events.mid(p1 + 3, p2 - (p1 + 3));
  mTemplateEvents3En = events.mid(p2 + 3, p3 - (p2 + 3));
  mTemplateEventsEndEn = events.mid(p3 + 3);

  LoadTextResource(":/strings/src/pageEvents_ru.txt", events);
  p1 = events.indexOf("%%%");
  p2 = events.indexOf("%%%", p1 + 3);
  p3 = events.indexOf("%%%", p2 + 3);
  mTemplateEvents1Ru = events.mid(0, p1);
  mTemplateEvents2Ru = events.mid(p1 + 3, p2 - (p1 + 3));
  mTemplateEvents3Ru = events.mid(p2 + 3, p3 - (p2 + 3));
  mTemplateEventsEndRu = events.mid(p3 + 3);

  LoadTextResource(":/strings/src/pageStates_en.txt", events);
  p1 = events.indexOf("%%%");
  mTemplateStatesEn = events.mid(0, p1);
  mTemplateStatesEndEn = events.mid(p1 + 3);

  LoadTextResource(":/strings/src/pageStates_ru.txt", events);
  p1 = events.indexOf("%%%");
  mTemplateStatesRu = events.mid(0, p1);
  mTemplateStatesEndRu = events.mid(p1 + 3);

  QFile templFileEn(qApp->applicationDirPath() + "/Src/template_en.html");
  if (templFileEn.open(QFile::ReadOnly)) {
    mTemplatePageEn = templFileEn.readAll();
  } else {
    Log.Fatal(QString("Main page open fail"), true);
  }

  QFile templFileRu(qApp->applicationDirPath() + "/Src/template_ru.html");
  if (templFileRu.open(QFile::ReadOnly)) {
    mTemplatePageRu = templFileRu.readAll();
  } else {
    Log.Fatal(QString("Main page open fail"), true);
  }

  QByteArray navBase;
  QByteArray navSetup;
  QByteArray navEvents;
  QByteArray navStates;
  QByteArray navMaps;
  LoadTextResource(":/strings/src/navBase_en.txt", navBase);
  if (mUseSetup) {
    LoadTextResource(":/strings/src/navSetup_en.txt", navSetup);
  }
  if (mUseEvents) {
    LoadTextResource(":/strings/src/navEvents_en.txt", navEvents);
  }
  if (mUseStates) {
    LoadTextResource(":/strings/src/navStates_en.txt", navStates);
  }
  if (mUseMaps) {
    LoadTextResource(":/strings/src/pageMaps_en.txt", mTemplateMapsEn);
    LoadTextResource(":/strings/src/navMaps_en.txt", navMaps);
    mVaStatTypeTable->Reload();
    const QMap<int, DbItemS>& items = mVaStatTypeTable->Items();
    if (!items.isEmpty()) {
      QByteArray navMapAll;
      QByteArray navMapOne;
      LoadTextResource(":/strings/src/navMapOne_en.txt", navMapOne);
      QByteArray pageMapOne;
      LoadTextResource(":/strings/src/pageMapOne_en.txt", pageMapOne);
      for (auto itr = items.begin(); itr != items.end(); itr++) {
        const VaStatType* item = dynamic_cast<const VaStatType*>(itr.value().data());
        QByteArray name("unknown");
        if (item->Abbr == "mov") {
          name = "Heatmaps";
        }
        navMapAll.append(navMapOne.replace("%INDEX%", QByteArray::number(item->Id))
                         .replace("%NAME%", name));
        mTemplateMapsEn.append(pageMapOne.replace("%INDEX%", QByteArray::number(item->Id))
                             .replace("%NAME%", name));
      }
      navMaps = navMaps.replace("%LINES%", navMapAll);
    } else {
      navMaps.clear();
    }
  }
  navBase = navBase.replace("%SETUP%", navSetup).replace("%EVENTS%", navEvents).replace("%STATES%", navStates).replace("%MAPS%", navMaps);
  mTemplatePageEn = mTemplatePageEn.replace("%NAV%", navBase);

  LoadTextResource(":/strings/src/navBase_ru.txt", navBase);
  if (mUseSetup) {
    LoadTextResource(":/strings/src/navSetup_ru.txt", navSetup);
  }
  if (mUseEvents) {
    LoadTextResource(":/strings/src/navEvents_ru.txt", navEvents);
  }
  if (mUseStates) {
    LoadTextResource(":/strings/src/navStates_ru.txt", navStates);
  }
  if (mUseMaps) {
    LoadTextResource(":/strings/src/pageMaps_ru.txt", mTemplateMapsRu);
    LoadTextResource(":/strings/src/navMaps_ru.txt", navMaps);
    mVaStatTypeTable->Reload();
    const QMap<int, DbItemS>& items = mVaStatTypeTable->Items();
    if (!items.isEmpty()) {
      QByteArray navMapAll;
      QByteArray navMapOne;
      LoadTextResource(":/strings/src/navMapOne_ru.txt", navMapOne);
      QByteArray pageMapOne;
      LoadTextResource(":/strings/src/pageMapOne_ru.txt", pageMapOne);
      for (auto itr = items.begin(); itr != items.end(); itr++) {
        const VaStatType* item = dynamic_cast<const VaStatType*>(itr.value().data());
        QByteArray name("unknown");
        if (item->Abbr == "mov") {
          name = "Heatmaps";
        }
        navMapAll.append(navMapOne.replace("%INDEX%", QByteArray::number(item->Id))
                         .replace("%NAME%", name));
        mTemplateMapsRu.append(pageMapOne.replace("%INDEX%", QByteArray::number(item->Id))
                             .replace("%NAME%", name));
      }
      navMaps = navMaps.replace("%LINES%", navMapAll);
    } else {
      navMaps.clear();
    }
  }
  navBase = navBase.replace("%SETUP%", navSetup).replace("%EVENTS%", navEvents).replace("%STATES%", navStates).replace("%MAPS%", navMaps);
  mTemplatePageRu = mTemplatePageRu.replace("%NAV%", navBase);
}

Portal::~Portal()
{
}
