#include <QCoreApplication>
#include <QVector>
#include <QByteArray>
#include <QList>
#include <QLocale>
#include <QTranslator>
#include <QScopedPointer>

#include <Lib/Log/Log.h>
#include <Lib/Db/ObjectType.h>
#include <Lib/Db/Event.h>
#include <Lib/Db/Files.h>
#include <Lib/Settings/DbSettings.h>
#include <Lib/Net/Chater.h>
#include <Lib/Net/NetMessage.h>
#include <LibV/Include/VideoMsg.h>

#include "PortalHandler.h"
#include "Portal.h"


const QString kTimeFormat("yyyy-MM-dd HH:mm");
const QString kTimeFormatUrl("yyyy-MM-dd+HH:mm");
const int kMinStatPeriodMs = 2 * 60 * 1000;

const QStringList& ControlImages()
{
  static QStringList gImages;

  if (gImages.isEmpty()) {
    gImages.append("ui-bg_flat_75_aaaaaa_40x100.png");
    gImages.append("ui-bg_glass_100_f5f0e5_1x400.png");
    gImages.append("ui-bg_glass_25_cb842e_1x400.png");
    gImages.append("ui-bg_glass_70_ede4d4_1x400.png");
    gImages.append("ui-bg_highlight-hard_100_f4f0ec_1x100.png");
    gImages.append("ui-bg_highlight-hard_65_fee4bd_1x100.png");
    gImages.append("ui-bg_highlight-hard_75_f5f5b5_1x100.png");
    gImages.append("ui-bg_inset-soft_100_f4f0ec_1x100.png");
    gImages.append("ui-icons_c47a23_256x240.png");
    gImages.append("ui-icons_cb672b_256x240.png");
    gImages.append("ui-icons_f08000_256x240.png");
    gImages.append("ui-icons_f35f07_256x240.png");
    gImages.append("ui-icons_ff7519_256x240.png");
    gImages.append("ui-icons_ffffff_256x240.png");
  }
  return gImages;
}

class ChaterCloser
{
  ChaterS mChater;

public:
  ChaterCloser(ChaterS& _Chater): mChater(_Chater) { }
  ~ChaterCloser() { mChater->Close(); }
};

bool PortalHandler::Get(const QString& path, const QList<QByteArray>& params)
{
  if (!Authorize()) {
    return true;
  }

  if (path.size() >= 1 && path[0].toLatin1() == '/') {
    if (path.size() == 1) {
      SelectLanguage();
      return HttpRedirect(QByteArray("/") + mLanguage + "/home");
    } else {
      QString url = path;
      if (!TakeLanguage(url)) {
        SelectLanguage();
      }
      switch (url[1].toLatin1()) {
      case 'a':
        if (url == "/about") {
          return OpenAbout();
        }
        break;

      case 'c':
        if (url == "/common_en.js") {
          return LoadFile(url, "application/javascript");
        } else if (url == "/common_ru.js") {
          return LoadFile(url, "application/javascript");
        } else if (url == "/common.js") {
          return LoadFile(url, "application/javascript");
        }
        break;

      case 'e':
        if (url.startsWith("/events")) {
          const int kLength = 7;
          if (url.size() <= kLength || url[kLength] == 'h') {
            return OpenEvents(params, eEventLog, 1);
          } else if (url[kLength] == 'd') {
            return OpenEvents(params, eEventLog, 24);
          } else if (url[kLength] == 'w') {
            return OpenEvents(params, eEventLog, 24*7);
          }
        } else if (url == "/excel.png") {
          return LoadFile(url, "image/png");
        } else if (url == "/eng.png") {
          return LoadFile(url, "image/png");
        }
        break;

      case 'f':
        if (url == "/favicon.ico" || url == "/favicon.png") {
          return LoadFile("/favicon.png", "image/png");
        } else if (url.startsWith("/file_") && url.endsWith(".jpg")) {
          QString fileIdText = url.mid(6, url.size() - 10);
          bool ok;
          qint64 id = fileIdText.toLongLong(&ok);
          if (ok) {
            return LoadDbFile(id);
          }
        }
        break;

      case 'h':
        if (url == "/home") {
          return OpenHome(params);
        }
        break;

      case 'i':
        if (url.startsWith("/images/") && ControlImages().contains(url.mid(8))) {
          return LoadFile(url, "image/png");
        }
        break;

      case 'j':
        if (url == "/jquery.js") {
          return LoadFile(url, "application/javascript");
        } else if (url == "/jquery-ui.js") {
          return LoadFile(url, "application/javascript");
        } else if (url == "/jquery.timepicker.min.js") {
          return LoadFile(url, "application/javascript");
        } else if (url == "/jquery-ui.css") {
          return LoadFile(url, "text/css");
        } else if (url == "/jquery-ui.theme.css") {
          return LoadFile(url, "text/css");
        } else if (url == "/jquery.timepicker.css") {
          return LoadFile(url, "text/css");
        }
        break;

      case 'l':
        if (url == "/logs") {
          return OpenEvents(params, eEventLog, 0);
        }
        break;

      case 'm':
        if (url.startsWith("/maps")) {
          QString tail = url.mid(5);
          bool ok = false;
          mSelectedMap = tail.toInt(&ok);
          if (ok) {
            return OpenEvents(params, eEventMap, 24);
          } else {
            return HttpRedirect(QByteArray("/") + mLanguage + "/maps1");
          }
        } else if (url == "/main_logo1.png") {
          return LoadFile(url, "image/png");
        } else if (url == "/man_in.png") {
          return LoadFile(url, "image/png");
        } else if (url == "/man_out.png") {
          return LoadFile(url, "image/png");
        }
        break;

      case 'r':
        if (url == "/rus.png") {
          return LoadFile(url, "image/png");
        }
        break;

      case 's':
        if (url.startsWith("/snapshot_") && url.endsWith(".jpg")) {
          int from = QString("/snapshot_").size();
          int size = url.size() - from - QString(".jpg").size();
          return LoadSnapshot(url.mid(from, size));
        } else if (url.startsWith("/stats")) {
          const int kLength = 6;
          if (url.size() <= kLength || url[kLength] == 'c') {
            return OpenEvents(params, eEventStat, 0);
          } else if (url[kLength] == 'h') {
            return OpenEvents(params, eEventStat, 1);
          } else if (url[kLength] == 'd') {
            return OpenEvents(params, eEventStat, 24);
          } else if (url[kLength] == 'w') {
            return OpenEvents(params, eEventStat, 24*7);
          }
        } else if (url == "/style.css") {
          return LoadFile(url, "text/css");
        } else if (url == "/sync.png") {
          return LoadFile(url, "image/png");
        } else if (url == "/setup") {
          return HttpRedirect(QByteArray("/") + mLanguage + "/setup1");
        } else if (url == "/setup1") {
          return OpenCameraSetup(0, params);
        } else if (url == "/setup2") {
          return OpenCameraSetup(1, params);
        }
        break;

      case 'q':
        if (url == "/queue_on.png") {
          return LoadFile(url, "image/png");
        } else if (url == "/queue_off.png") {
          return LoadFile(url, "image/png");
        }
        break;

      case 'v':
        if (url == "/vs.png") {
          return LoadFile(url, "image/png");
        } else if (url == "/vsrm.png") {
          return LoadFile(url, "image/png");
        }
        break;

      default:
        break;
      }
    }
  }

  Log.Info(QString("GET (path: '%1', params: %2)").arg(path).arg(params.size()));
  return HttpResultBadRequest(true);
}

bool PortalHandler::Post(const QString& path, const QList<QByteArray>& params, const QList<File>& files)
{
  if (!Authorize()) {
    return true;
  }

  if (path.size() > 1 && path[0].toLatin1() == '/' && files.size() == 1) {
    QString url = path;
    if (!TakeLanguage(url)) {
      SelectLanguage();
    }
    QList<QByteArray> params_ = files.at(0).Data.split('&');
    switch (url[1].toLatin1()) {
    case 'a':
      if (url == "/setup") {
        return HttpRedirect(QByteArray("/") + mLanguage + "/setup1");
      } else if (url == "/setup1") {
        return OpenCameraSetup(0, params_);
      } else if (url == "/setup2") {
        return OpenCameraSetup(1, params_);
      }
      break;

    case 'o':
      if (url == "/opt_change") {
        return ChangeOptions(params_);
      }
      break;

    default:
      break;
    }
  }

  Log.Info(QString("POST (path: '%1', params: %2)").arg(path).arg(params.size()));
  return HttpResultBadRequest(true);
}

bool PortalHandler::Authorize()
{
  if (mPortal->Authorized(Header("Authorization"))) {
    return true;
  }

  HttpResultUnauthorized("VICA", true);
  return false;
}

bool PortalHandler::LoadFile(const QString& filename, const char* contentType, int cache)
{
  QFile file(qApp->applicationDirPath() + "/Src" + filename);
  if (file.open(QFile::ReadOnly)) {
    QByteArray fileData = file.readAll();
    Answer() = QByteArray("HTTP/1.1 200 Ok\r\n")
        + "Connection: Keep-Alive\r\n"
        + "Content-Type: " + contentType + ";\r\n"
        + "Content-Length: " + QByteArray::number(fileData.size()) + "\r\n"
        + "Cache-Control: " + "max-age=" + QByteArray::number(cache) + "\r\n""\r\n"
        + fileData;
    return true;
  }

  return HttpResultNotFound(true);
}

bool PortalHandler::LoadSnapshot(const QString& path)
{
  do {
    if (mCameras.isEmpty()) {
      if (!OpenDb() || !LoadCameras()) {
        break;
      }
    }
    int index = path.toInt();

    QString uri;
    if (index >= 0 && index < mCameras.size()) {
      uri = mCameras[index].Uri;
    }

    if (uri.isEmpty()) {
      Log.Warning(QString("Can't get info camera"));
      break;
    }

    ChaterS chater = Chater::CreateChater(mPortal->GetManager(), Uri::FromString(uri));
    ChaterCloser closer(chater);
    NetMessageS respondMsg;
    if (!chater->SendSimpleRequest(eMsgThumbnail, respondMsg, 5000)) {
      Log.Warning(QString("Can't get Thumbnail (camera: '%1')").arg(uri));
      break;
    }

    if (respondMsg->GetMessageType() != eMsgThumbnailOk) {
      if (respondMsg->GetMessageType() == eMsgThumbnailNo) {
        Log.Info(QString("No Thumbnail (camera: '%1')").arg(uri));
      } else if (respondMsg->GetMessageType() == eMsgOneFrame) {
        Log.Info(QString("Unimplemented Thumbnail (camera: '%1')").arg(uri));
      } else {
        Log.Warning(QString("Get invalid Thumbnail answer (camera: '%1', rspCode: %2)").arg(uri).arg(respondMsg->GetMessageType()));
      }
      break;
    }

    QByteArray img(respondMsg->GetMessageConstData(), respondMsg->GetMessageDataSize());
    chater->Close();
    Answer() = QByteArray("HTTP/1.1 200 Ok\r\n")
        + "Connection: Keep-Alive\r\n"
        + "Content-Type: image/jpeg;\r\n"
        + "Content-Length: " + QByteArray::number(img.size()) + "\r\n""\r\n"
        + img;
    return true;
  } while (false);

  return HttpResultNotFound(true);
}

bool PortalHandler::LoadDbFile(const qint64& id)
{
  do {
    if (!OpenDb()) {
      break;
    }

    if (!mFilesTable) {
      mFilesTable.reset(new FilesTable(*mDb));
    }

    FilesS file;
    if (!mFilesTable->SelectById(id, file)) {
      break;
    }

    Answer() = QByteArray("HTTP/1.1 200 Ok\r\n")
        + "Connection: Keep-Alive\r\n"
        + "Content-Type: image/jpeg;\r\n"
        + "Content-Length: " + QByteArray::number(file->Data.size()) + "\r\n""\r\n"
        + file->Data;
    return true;
  } while (false);

  return HttpResultNotFound(true);
}

bool PortalHandler::OpenTemplatePage(const QByteArray& body)
{
  Answer() = QByteArray(GetHtmlHeader(false));
  QByteArray pageText = mPortal->TemplatePage(mIsRu);
  pageText.replace("%BODY%", body);
  Answer().append("Content-Length: " + QByteArray::number(pageText.size()) + "\r\n""\r\n");
  Answer().append(pageText);
  return true;
}

bool PortalHandler::OpenHome(const QList<QByteArray>& params)
{
  QByteArray home = mPortal->TemplateHome(mIsRu);
  int ind1 = home.indexOf("%%%", 0);
  int ind2 = home.indexOf("%%%", ind1 + 3);

  do {
    if (!OpenDb()) {
      home.replace(ind1, ind2 - ind1 + 3, QByteArray("<p><font color=\"red\">") + QObject::tr("Db connection failed").toUtf8() + "</font></p>\n");
      break;
    }
    bool ok = LoadCameras();
    if (!ok) {
      home.replace(ind1, ind2 - ind1 + 3, QByteArray("<p><font color=\"red\">") + QObject::tr("Load cameras list failed").toUtf8() + "</font></p>\n");
      break;
    }

    QByteArray combo;
    GetCamCombo(combo);
    home.replace(ind2, 3, QByteArray(""));
    home.replace(ind1, 3, QByteArray(""));
    home.replace("%%CAM_COMBO%%", combo);

    QByteArray camIndex;
    for (auto itr = params.begin(); itr != params.end(); itr++) {
      int s = itr->indexOf('=');
      if (s > 0) {
        QByteArray key = itr->mid(0, s);
        QByteArray value = itr->mid(s+1);
        if (key == "cam") {
          camIndex = value;
        }
      }
    }
    home.replace("%%ID%%", camIndex);
  } while (false);

  return OpenTemplatePage(home);
}

bool PortalHandler::OpenCameraSetup(int index, const QList<QByteArray>& params)
{
  QByteArray setupPage = mPortal->TemplateSetup(mIsRu);
  int cameraId = 0;
  int scheduleId = 0;
  int barierId = 0;
  QByteArray cameraName;
  QByteArray cameraStatus;
  QByteArray cameraRtsp;
  QByteArray cameraLogin;
  QByteArray cameraPassword;
  QByteArray schedule[7];
  QByteArray from[7], to[7];
  QByteArray px[2], py[2];
  bool resultOk = false;
  QByteArray actionResult;
  do {
    if (!OpenDb()) {
      actionResult = QObject::tr("Db connection failed").toUtf8();
      break;
    }
    bool ok = LoadCamerasEx();
    if (!ok) {
      actionResult = QObject::tr("Load cameras list failed").toUtf8();
      break;
    }

    if (index < mCameras.size()) {
      const CameraInfo& cameraInfo = mCameras.at(index);
      cameraId     = cameraInfo.Id;
      cameraName   = QObject::tr("Camera %1").arg(index + 1).toUtf8();
      cameraStatus = (cameraInfo.State == 0)? "checked": "";
      barierId     = cameraInfo.Barier;
      scheduleId   = cameraInfo.Schedule;
    } else {
      actionResult = QObject::tr("Camera not found").toUtf8();
      break;
    }

    DbSettings settings(*mDb);
    if (cameraId && settings.Open(cameraId)) {
      cameraRtsp = settings.GetValue("Uri", "rtsp://srv:554/Video1").toString().toUtf8();
      cameraLogin = settings.GetValue("Login", "admin").toString().toUtf8();
      cameraPassword = settings.GetValue("Password", "admin").toString().toUtf8();
    }

    if (scheduleId && settings.Open(QString::number(scheduleId))) {
      for (int i = 0; i < 7; i++) {
        QString key = QString("Weekly") + QString::number(i);
        QString value = settings.GetValue(key, "").toString();
        QStringList values = value.split(QChar('-'), QString::SkipEmptyParts);
        if (values.size() == 2) {
          schedule[i] = QByteArray("checked");
          from[i] = values.at(0).toLatin1();
          to[i]   = values.at(1).toLatin1();
        }
      }
    }

    if (barierId && settings.Open(barierId)) {
      for (int i = 0; i < 2; i++) {
        QString key = QString("Point #") + QString::number(i);
        QString value = settings.GetValue(key, "").toString();
        QStringList values = value.split(QChar(','), QString::SkipEmptyParts);
        if (values.size() == 2) {
          px[i] = QByteArray::number(values.at(0).toDouble() * 100.0);
          py[i] = QByteArray::number(values.at(1).toDouble() * 100.0);
        }
      }
    }

    for (auto itr = params.begin(); itr != params.end(); itr++) {
      int s = itr->indexOf('=');
      if (s > 0) {
        QByteArray key = itr->mid(0, s);
        QByteArray value = itr->mid(s+1);
        if (key == "result") {
          actionResult = QUrl::fromPercentEncoding(value.replace('+', ' ')).toUtf8();
        } else if (key == "status") {
          resultOk = (value == "ok");
        }
      }
    }

  } while (false);

  if (!actionResult.isEmpty()) {
    if (resultOk) {
      setupPage.replace("%%RESULT%%", QByteArray("<div id=\"action_result\" style = \"background-color: lightgreen;\">") + actionResult + "</div>\n");
    } else {
      setupPage.replace("%%RESULT%%", QByteArray("<div id=\"action_result\" style = \"background-color: pink;\">") + actionResult + "</div>\n");
    }
  } else {
    setupPage.replace("%%RESULT%%", "");
  }

  setupPage.replace("%%CAMERA_NAME%%", cameraName).replace("%%ID%%", QByteArray::number(index))
      .replace("%%POWER%%", cameraStatus).replace("%%RTSP%%", cameraRtsp).replace("%%LOGIN%%", cameraLogin).replace("%%PASSWD%%", cameraPassword)
      .replace("%%PX1%%", px[0]).replace("%%PY1%%", py[0]).replace("%%PX2%%", px[1]).replace("%%PY2%%", py[1]);
  for (int i = 0; i < 7; i++) {
    setupPage.replace(QString("%%SCH%1%%").arg(i), schedule[i]);
    setupPage.replace(QString("%%F%1%%").arg(i), from[i]);
    setupPage.replace(QString("%%T%1%%").arg(i), to[i]);
  }
  return OpenTemplatePage(setupPage);
}

bool PortalHandler::OpenEvents(const QList<QByteArray>& params, PortalHandler::EStatType statType, int hours)
{
  bool exportExcel = false;
  QDateTime ts1;
  QDateTime ts2;
  bool ts4has = false;
  QVector<QDateTime> ts4(4, QDateTime());
  int iod = 0;
  QVector<int> iod4(4, 0);
  mUtc = 0;
  for (auto itr = params.begin(); itr != params.end(); itr++) {
    int s = itr->indexOf('=');
    if (s > 0) {
      QByteArray key = itr->mid(0, s);
      QByteArray value = itr->mid(s+1);
      if (key.startsWith("from")) {
        QByteArray suffix = key.mid(4);
        if (suffix.isEmpty()) {
          ts1 = ReadDateTimeParam(value);
        } else {
          int ind = suffix.toInt();
          if (ind > 0 && ind <= ts4.size()) {
            ts4[ind - 1] = ReadDateTimeParam(value);
            if (ts4[ind - 1].isValid()) {
              ts4has = true;
            }
          }
        }
      } else if (key.startsWith("iod")) {
        QByteArray suffix = key.mid(3);
        if (suffix.isEmpty()) {
          iod = value.toInt();
        } else {
          int ind = suffix.toInt();
          if (ind > 0 && ind <= iod4.size()) {
            iod4[ind - 1] = value.toInt();
          }
        }
      } else if (key == "to") {
        ts2 = ReadDateTimeParam(value);
      } else if (key == "utc") {
        QByteArray value = itr->mid(s+1);
        mUtc = value.toInt();
      } else if (key == "export" || key == "export.x" || key == "export.y") {
        exportExcel = true;
      }
    }
  }

  QByteArray result;

  QList<int> iodList;
  QList<QDateTime> ts4list;
  if (ts4has) {
    result.append("<script>\n");
    for (int i = 0; i < ts4.size(); i++) {
      if (ts4[i].isValid()) {
        result.append("AddCompare();\n");
        result.append("document.getElementById(\"from" + QByteArray::number(ts4list.size()+1) + "\").value=\""
                      + ts4[i].toString(kTimeFormat).toLatin1() + "\"\n");
        iodList.append(iod4[i]);
        ts4list.append(ts4[i].addSecs(mUtc * 60));
      }
    }
    result.append("</script>\n");
  }

  if (!ts1.isValid() || !ts2.isValid()) {
    QDate today = QDate::currentDate();
    if (hours < 24) {
      ts1 = QDateTime(today, QTime(0, 0, 0), Qt::UTC);
      ts2 = QDateTime(today, QTime(23, 59, 0), Qt::UTC);
    } else if (hours < 7*24) {
      ts1 = QDateTime(today.addDays(1 - today.dayOfWeek()), QTime(0, 0, 0), Qt::UTC);
      ts2 = QDateTime(today.addDays(7 - today.dayOfWeek()), QTime(23, 59, 0), Qt::UTC);
    } else {
      ts1 = QDateTime(today.addDays(1 - today.day()), QTime(0, 0, 0), Qt::UTC);
      ts2 = QDateTime(today.addDays(today.daysInMonth() - today.day()), QTime(23, 59, 0), Qt::UTC);
    }
    ts4.clear();
  }

  QByteArray combo;
  result.append("<div class=\"results\" id=\"results\" style=\"width: " + QByteArray::number(480 + 280 * ts4list.size()) + "px;\">\n");
  do {
    if (!OpenDb()) {
      result.append("<p><font color=\"red\">");
      result.append(QObject::tr("Db connection failed").toUtf8());
      result.append("</font></p>\n</div>\n");
      break;
    }
    int periodh = qMax(hours, (int)((ts1.secsTo(ts2) + 30*60) / 3600));
    bool ok = false;
    switch (statType) {
    case eEventLog:
    case eEventStat:
      ok = LoadDetectors();
      break;
    case eEventMap:
      ok = LoadAnals();
      break;
    }
    if (!ok) {
      result.append("<p><font color=\"red\">");
      result.append(QObject::tr("Load detectors list failed").toUtf8());
      result.append("</font></p>\n</div>\n");
      break;
    }

    switch (statType) {
    case eEventLog:
      if (hours) {
        ok = AddLogStats(statType, exportExcel, iod, iodList, ts1.addSecs(mUtc * 60), periodh, ts4list, hours, result);
      } else {
        ok = AddLogs(exportExcel, iod, ts1.addSecs(mUtc * 60), ts2.addSecs(mUtc * 60), result);
      }
      break;

    case eEventStat:
      if (hours) {
        ok = AddLogStats(statType, exportExcel, iod, iodList, ts1.addSecs(mUtc * 60), periodh, ts4list, hours, result);
      } else {
        ok = AddStats(exportExcel, result);
      }
      break;

    case eEventMap:
      ok = AddMaps(iod, iodList, ts1.addSecs(mUtc * 60), periodh, ts4list, result);
      break;

    default:
      ok = AddLogs(exportExcel, iod, ts1.addSecs(mUtc * 60), ts2.addSecs(mUtc * 60), result);
      LOG_ERROR_ONCE("Illegal statType case");
      break;
    }

    if (!ok) {
      result.append("<p><font color=\"red\">");
      result.append(QObject::tr("Load data failed").toUtf8());
      result.append("</font></p>\n</div>\n");
      break;
    }
    if (exportExcel) {
      Answer() = QByteArray(QByteArray("HTTP/1.1 200 Ok\r\n")
                            + "Content-Type: text/csv; charset=utf-8\r\n"
                            + "Content-Disposition: attachment; filename=\"export.csv\"\r\n"
                            + "Content-Length: " + QByteArray::number(result.size()) + "\r\n""\r\n");
      Answer().append(result);
      return true;
    }
    result.append("</div>\n");

    switch (statType) {
    case eEventLog:  GetIodCombo(combo); break;
    case eEventStat: GetStateCombo(combo); break;
    case eEventMap:  GetMapCombo(combo); break;
    }
  } while (false);

  if (statType == eEventStat && hours <= 0) {
    return OpenTemplatePage(mPortal->TemplateStates(mIsRu) + result + mPortal->TemplateStatesEnd(mIsRu));
  }

  QByteArray script;
  if (iod > 0) {
    script.append(QByteArray("document.getElementById(\"iod\").selectedIndex = \"" + QByteArray::number(iod) + "\";\n"));
  }
  for (int i = 0; i < iod4.size(); i++) {
    if (iod4[i]) {
      script.append(QByteArray("document.getElementById(\"iod" + QByteArray::number(i + 1)
                               + "\").selectedIndex = \"" + QByteArray::number(iod4[i]) + "\";\n"));
    }
  }
  if (!script.isEmpty()) {
    script = QByteArray("<script>" + script + "</script>");
  }

  return OpenTemplatePage(mPortal->TemplateEvents1(mIsRu) + ts1.toString(kTimeFormat).toLatin1()
                          + mPortal->TemplateEvents2(mIsRu) + ts2.toString(kTimeFormat).toLatin1()
                          + QByteArray(mPortal->TemplateEvents3(mIsRu)).replace("%%IOD_LIST%%", combo)
                          + script + result + mPortal->TemplateEventsEnd(mIsRu));
}

bool PortalHandler::OpenMaps()
{
  return OpenTemplatePage(mPortal->TemplateMaps(mIsRu));
}

bool PortalHandler::OpenAbout()
{
  return OpenTemplatePage(mPortal->TemplateAbout(mIsRu));
}

bool PortalHandler::ChangeOptions(const QList<QByteArray>& params)
{
  int index = -1;
  bool power = false;
  QString uri, login, password;
  qreal px[2] = { 0, 0 };
  qreal py[2] = { 0, 0 };
  bool sch[7] = { false, false, false, false, false, false, false };
  QString from[7], to[7];
  for (auto itr = params.begin(); itr != params.end(); itr++) {
    int s = itr->indexOf('=');
    if (s > 0) {
      QByteArray key = itr->mid(0, s);
      QByteArray value = itr->mid(s+1);
      if (key == "index") {
        index = value.toInt();
      } else if (key == "power") {
        power = value == "on";
      } else if (key == "uri") {
        uri = QUrl::fromPercentEncoding(value.replace('+', ' '));
      } else if (key == "login") {
        login = QUrl::fromPercentEncoding(value.replace('+', ' '));
      } else if (key == "password") {
        password = QUrl::fromPercentEncoding(value.replace('+', ' '));
      } else if (key == "px1") {
        px[0] = value.toDouble();
      } else if (key == "py1") {
        py[0] = value.toDouble();
      } else if (key == "px2") {
        px[1] = value.toDouble();
      } else if (key == "py2") {
        py[1] = value.toDouble();
      } else if (key.startsWith("sch")) {
        bool ok;
        int ind = key.mid(3).toInt(&ok);
        if (ok && ind >= 0 && ind < 7) {
          sch[ind] = value == "on";
        }
      } else if (key.startsWith("f")) {
        bool ok;
        int ind = key.mid(1).toInt(&ok);
        if (ok && ind >= 0 && ind < 7) {
          from[ind] = QUrl::fromPercentEncoding(value.replace('+', ' '));
        }
      } else if (key.startsWith("t")) {
        bool ok;
        int ind = key.mid(1).toInt(&ok);
        if (ok && ind >= 0 && ind < 7) {
          to[ind] = QUrl::fromPercentEncoding(value.replace('+', ' '));
        }
      }
    }
  }

  QString actionResult;
  bool resultOk = false;

  do {
    if (index < 0) {
      actionResult = QObject::tr("Camera bad index").toUtf8();
      break;
    }
    if (!OpenDb()) {
      actionResult = QObject::tr("Db connection failed").toUtf8();
      break;
    }
    bool ok = LoadCamerasEx();
    if (!ok) {
      actionResult = QObject::tr("Load cameras list failed").toUtf8();
      break;
    }

    int cameraId = 0;
    bool cameraStatus;
    int scheduleId;
    int barierId;
    if (index < mCameras.size()) {
      const CameraInfo& cameraInfo = mCameras.at(index);
      cameraId = cameraInfo.Id;
      cameraStatus = (cameraInfo.State == 0);
      scheduleId = cameraInfo.Schedule;
      barierId = cameraInfo.Barier;
    }
    if (!cameraId) {
      actionResult = QObject::tr("Camera not found").toUtf8();
      break;
    }

    if (cameraStatus != power) {
      ObjectTable objectTable(*mDb);
      if (!objectTable.UpdateState(cameraId, power? 0: 1)) {
        actionResult = QObject::tr("Camera enable failed").toUtf8();
        break;
      }
      actionResult.append(power? QObject::tr("Camera enabled; ").toUtf8(): QObject::tr("Camera disabled; ").toUtf8());
    }
    if (!power) {
      resultOk = true;
      break;
    }

    DbSettings settings(*mDb);
    if (!settings.Open(QString::number(cameraId))) {
      actionResult.append(QObject::tr("Camera setup failed; ").toUtf8());
      break;
    }

    bool allOk = false;
    do {
      settings.SetValue("Uri", uri);
      if (settings.HasError()) {
        break;
      }
      settings.SetValue("Login", login);
      if (settings.HasError()) {
        break;
      }
      settings.SetValue("Password", password);
      if (settings.HasError()) {
        break;
      }

      allOk = true;
    } while (false);

    if (!allOk) {
      actionResult.append(QObject::tr("Camera setup failed; ").toUtf8());
      break;
    }
    actionResult.append(QObject::tr("Camera setup success; ").toUtf8());

    if (scheduleId) {
      if (!settings.Open(QString::number(scheduleId))) {
        actionResult.append(QObject::tr("Schedule setup failed; ").toUtf8());
        break;
      }

      bool allOk2 = true;
      for (int i = 0; i < 7; i++) {
        QString key = QString("Weekly") + QString::number(i);
        QString value = sch[i]? QString("%1-%2").arg(from[i]).arg(to[i]): QString();
        settings.SetValue(key, value);
        if (settings.HasError()) {
          allOk2 = false;
          break;
        }
      }

      if (!allOk2) {
        actionResult.append(QObject::tr("Schedule setup failed; ").toUtf8());
        break;
      }
      actionResult.append(QObject::tr("Schedule setup success; ").toUtf8());
    }

    if (barierId) {
      if (!settings.Open(QString::number(barierId))) {
        actionResult.append(QObject::tr("Setup barrier failed; ").toUtf8());
        break;
      }

      bool allOk2 = true;
      for (int i = 0; i < 2; i++) {
        QString key = QString("Point #") + QString::number(i);
        QString value = QString("%1,%2").arg(px[i] * 0.01).arg(py[i] * 0.01);
        settings.SetValue(key, value);
        if (settings.HasError()) {
          allOk2 = false;
          break;
        }
      }

      if (!allOk2) {
        actionResult.append(QObject::tr("Setup barrier failed; ").toUtf8());
        break;
      }
      actionResult.append(QObject::tr("Setup barrier success; ").toUtf8());
    }

    resultOk = true;
  } while (false);

  HttpResult(301, "Redirect", false);
  QByteArray redirect = QByteArray("/") + mLanguage + ("/setup");
  if (index >= 0 && index < 2) {
    redirect.append(QByteArray::number(index + 1));
  }
  redirect.append("?result=" + QUrl::toPercentEncoding(actionResult.toUtf8())
             + "&status=" + (resultOk? "ok": "fail"));
  return HttpRedirect(redirect);
}

bool PortalHandler::LoadCameras()
{
  mCameras.clear();
  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT o._id, o.name, o.uri, o.status FROM object_connection c1"
                     " JOIN object_connection c2 ON c2._omaster = c1._omaster"
                     " JOIN object o ON o._id = c2._oslave"
                     " WHERE c1._oslave = %1 AND o._otype = %2;").arg(mPortal->GetPortalId()).arg(mPortal->GetCameraType()));
  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  while (q->next()) {
    CameraInfo camera;
    int index = 0;
    camera.Id    = q->value(index++).toInt();
    camera.Name  = q->value(index++).toString();
    camera.Uri   = q->value(index++).toString();
    camera.State = q->value(index++).toInt();
    mCameras.append(camera);
  }
  return true;
}

bool PortalHandler::LoadCamerasEx()
{
  mCameras.clear();
  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT o._id, o.name, o.uri, o.status, os1._id, os1._otype, os2._id, os2._otype FROM object_connection c1"
                     " JOIN object_connection c2 ON c2._omaster = c1._omaster"
                     " JOIN object o ON o._id = c2._oslave"
                     " JOIN object_connection c3 ON c3._omaster = o._id"
                     " JOIN object os1 ON os1._id = c3._oslave"
                     " LEFT JOIN object_connection c4 ON c4._omaster = c3._oslave"
                     " LEFT JOIN object os2 ON os2._id = c4._oslave"
                     " WHERE c1._oslave = %1 AND o._otype = %2;").arg(mPortal->GetPortalId()).arg(mPortal->GetCameraType()));
  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  QMap<int, CameraInfo> cameraMap;
  while (q->next()) {
    int index = 0;
    int id        = q->value(index++).toInt();
    CameraInfo* camera = &cameraMap[id];
    camera->Id    = id;
    camera->Name  = q->value(index++).toString();
    camera->Uri   = q->value(index++).toString();
    camera->State = q->value(index++).toInt();

    int slave1Id   = q->value(index++).toInt();
    int slave1Type = q->value(index++).toInt();
    int slave2Id   = q->value(index++).toInt();
    int slave2Type = q->value(index++).toInt();

    if (slave1Id && slave1Type && slave1Type == mPortal->GetScheduleType()) {
      camera->Schedule = slave1Id;
    } if (slave2Id && slave2Type && slave2Type == mPortal->GetIodType()) {
      camera->Barier = slave2Id;
    }
  }
  foreach (const CameraInfo& cameraInfo, cameraMap.values()) {
    mCameras.append(cameraInfo);
  }
  return true;
}

bool PortalHandler::LoadDetectors()
{
  mLogDetectors.clear();
  mStatDetectors.clear();
  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT DISTINCT o._id, o.name, et.flag FROM event e"
                     " INNER JOIN object o ON o._id = e._object"
                     " INNER JOIN event_type et ON et._id = e._etype"
                     " ORDER BY o._id;"));
  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  while (q->next()) {
    DetectorInfo detector;
    detector.Id   = q->value(0).toInt();
    detector.Name = q->value(1).toString();
    detector.Flag = q->value(2).toInt();
    if (detector.Flag & (int)eEventLog) {
      mLogDetectors.append(detector);
    } else if (detector.Flag & (int)eEventStat) {
      mStatDetectors.append(detector);
    }
  }
  return true;
}

bool PortalHandler::LoadAnals()
{
  mStatMaps.clear();
  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT DISTINCT o._id, o.name FROM va_stat s"
                     " JOIN object o ON o._id = s._object"
                     " WHERE s._vstype = %1"
                     " ORDER BY o._id;").arg(mSelectedMap));
  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  while (q->next()) {
    DetectorInfo detector;
    detector.Id   = q->value(0).toInt();
    detector.Name = q->value(1).toString();
    detector.Flag = 0;
    mStatMaps.append(detector);
  }
  return true;
}

bool PortalHandler::AddLogs(bool exportExcel, int iodId, const QDateTime& ts1, const QDateTime& ts2, QByteArray& result)
{
  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT e._object, e._etype, l.triggered_time at TIME ZONE 'UTC', l.value"
                     " FROM event_log l"
                     " INNER JOIN event e ON e._id = l._event"
                     " WHERE e._object = :obj AND triggered_time >= :ts1 AND triggered_time <= :ts2"
                     " ORDER BY triggered_time;"));
  q->bindValue(":obj", iodId);
  q->bindValue(":ts1", ts1);
  q->bindValue(":ts2", ts2);

  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  if (!exportExcel) {
    result.append("<table align=\"left\">\n"
                  "<tr style=\"height:30px\"><th></th>");
    result.append(QObject::tr("<th>Event</th><th>Time</th>").toUtf8());
    result.append("<td><input type=\"image\" src=\"/excel.png\" alt=\"Submit\" width=\"16\" height=\"16\" name = \"export\" title=\"");
    result.append(QObject::tr("Update").toUtf8());
    result.append("\" value=\"excel\"></td></tr>"
                  "<tr><td></td></tr>");
  } else {
    result = QByteArray(QByteArray("\xEF\xBB\xBF") + QObject::tr("Event;Time").toUtf8() + "\n");
  }

  while (q->next()) {
//    int detectorId = q->value(0).toInt();
    int eventTypeId = q->value(1).toInt();
    QDateTime ts = q->value(2).toDateTime().addSecs(-mUtc * 60);
    QString tsStr = ts.toString("dd-MMM hh:mm");
    int value = q->value(3).toInt();

//    const ObjectItem* detector = mPortal->GetObject(detectorId);
    const EventType* eventType = mPortal->GetEventType(eventTypeId);
    if (!exportExcel) {
      result.append("<tr><td><img src=\"");
      result.append(eventType? eventType->Icon.toUtf8(): "");
      result.append("\"></td><td>");
      result.append(eventType
                    ? ((value == 1)? eventType->Descr.toUtf8(): QString("%1: %2").arg(eventType->Descr).arg(value).toUtf8())
                    : QObject::tr("<unknown>"));
      result.append("</td><td>" + tsStr.toUtf8() + "</td></tr>\n");
    } else {
      result.append(eventType
                    ? ((value == 1)? eventType->Descr.toUtf8(): QString("%1 (%2)").arg(eventType->Descr).arg(value).toUtf8())
                    : QObject::tr("<unknown>").toUtf8());
      result.append(";" + tsStr.toUtf8() + "\n");
    }
  }

  if (!exportExcel) {
    result.append("</table>\n");
  }

  return true;
}

bool PortalHandler::AddLogStats(EStatType statType, bool exportExcel, int iodId, const QList<int>& iodIds, const QDateTime& ts1
                                , int periodh, const QList<QDateTime>& ts4, int hours, QByteArray& result)
{
  QVector<qreal> statsBase;
  QVector<QVector<qreal> > statsVs;
  bool ok = (statType == eEventLog)? GetPeriodLogs(iodId, ts1, periodh, hours, statsBase)
                                   : GetPeriodStatLogs(iodId, ts1, periodh, hours, statsBase);
  int prec = (statType == eEventLog)? 0: 2;
  if (!ok) {
    return false;
  }
  for (int i = 0; i < iodIds.size(); i++) {
    statsVs.append(QVector<qreal>());
    bool ok = (statType == eEventLog)? GetPeriodLogs(iodIds[i], ts4[i], periodh, hours, statsVs.back())
                                     : GetPeriodStatLogs(iodIds[i], ts4[i], periodh, hours, statsVs.back());
    if (!ok) {
      return false;
    }
  }

  if (!exportExcel) {
    result.append("<table align=\"left\"><thead>\n"
                  "<tr style=\"height:30px\">");
    result.append(QObject::tr("<th>Time</th><th>Value</th>").toUtf8());
        result.append("\n");
    for (int i = 0; i < statsVs.size(); i++) {
      result.append(QObject::tr("<th>Period ").toUtf8() + QByteArray::number(i+1)
                    + QObject::tr("</th><th>Value</th><th>Difference</th><th>Percent</th>").toUtf8());
    }
    result.append("<td><input type=\"image\" title=\"");
    result.append(QObject::tr("Export to csv").toUtf8());
        result.append("\" src=\"/excel.png\" alt=\"Submit\" width=\"16\" height=\"16\" title=\"");
    result.append(QObject::tr("Update").toUtf8());
        result.append("\" name = \"export\" value=\"excel\"></td>"
                      "</tr>\n<tr><td></td></tr></thead><tbody>\n");
  } else {
    result = QByteArray("\xEF\xBB\xBF") + QObject::tr("Time;Value").toUtf8();
    for (int i = 0; i < statsVs.size(); i++) {
      result.append(QObject::tr(";Period ").toUtf8() + QByteArray::number(i+1) + QObject::tr(";Value;Difference;Percent").toUtf8());
    }
    result.append("\n");
  }

  QString timeFormat;
  if (hours < 24) {
    timeFormat = "dd-MMM(ddd) hh";
  } else if (hours < 24 * 7) {
    timeFormat = "dd-MMM(ddd)";
  } else {
    timeFormat = "dd-MMM";
  }
  QLocale locale = mIsRu? QLocale(QLocale::Russian, QLocale::RussianFederation): QLocale(QLocale::English, QLocale::UnitedStates);
  for (int i = 0; i < statsBase.size(); i++) {
    QDateTime ts = ts1.addSecs(i * hours * 3600 - mUtc * 60);
    if (statsBase[i] == 0) {
      bool skipLine = true;
      for (int j = 0; j < statsVs.size(); j++) {
        if (statsVs[j][i] > 0) {
          skipLine = false;
          break;
        }
      }
      if (skipLine) {
        continue;
      }
    }

    QByteArray val = QByteArray::number(statsBase[i], 'f', prec);
    if (!exportExcel) {
      result.append("<tr><td>" + locale.toString(ts, timeFormat).toUtf8() + "</td><td>" + val + "</td>");
    } else {
      result.append(locale.toString(ts, timeFormat).toUtf8() + ";" + val);
    }

    for (int j = 0; j < statsVs.size(); j++) {
      QDateTime ts = ts4[j].addSecs(i * hours * 3600 - mUtc * 60);
      QByteArray tsText = locale.toString(ts, timeFormat).toUtf8();

      QByteArray base = QByteArray::number(statsVs[j][i], 'f', prec);
      QByteArray diff = QByteArray::number(statsVs[j][i] - statsBase[i], 'f', prec);
      QByteArray perc = (statsBase[i] > 0)? QByteArray::number(100.0 * statsVs[j][i] / statsBase[i], 'f', 0) + "%": "---";
      if (!exportExcel) {
        result.append("\n<td>" + tsText + "</td><td>" + base + "</td><td>" + diff + "</td><td>" + perc + "</td>");
      } else {
        result.append(QByteArray(";") + tsText + ";" + base + ";" + diff + ";" + perc);
      }
    }

    if (!exportExcel) {
      result.append("</tr>\n");
    } else {
      result.append("\n");
    }
  }

  if (!exportExcel) {
    result.append("</tbody></table>\n");
  }
  return true;
}

bool PortalHandler::AddStats(bool exportExcel, QByteArray& result)
{
  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT e._object, e._etype, s.value / s.period"
                     " FROM event_stat s INNER JOIN event e ON e._id = s._event"
                     " ORDER BY triggered_time;"));

  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  if (!exportExcel) {
    result.append("<table align=\"left\">\n"
                  "<tr style=\"height:30px\"><th></th>");
    result.append(QObject::tr("<th>State</th><th>Detector</th><th>Value</th>").toUtf8());
    result.append("<td><input type=\"image\" src=\"/excel.png\" alt=\"Submit\" width=\"16\" height=\"16\" title=\"");
    result.append(QObject::tr("Update").toUtf8());
    result.append("\" name = \"export\" value=\"excel\"></td></tr>"
                  "<tr><td></td></tr>");
  } else {
    result = QByteArray("\xEF\xBB\xBF") + QObject::tr("State;Detector;Value").toUtf8() + "\n";
  }

  while (q->next()) {
    int detectorId = q->value(0).toInt();
    int eventTypeId = q->value(1).toInt();
    qreal value = q->value(2).toReal();

    const ObjectItem* detector = mPortal->GetObject(detectorId);
    const EventType* eventType = mPortal->GetEventType(eventTypeId);

    QByteArray stateText = eventType->Descr.toUtf8();
    QByteArray detectorText = detector->Name.toUtf8();
    QByteArray valueText = QString("%1").arg(value, 0, 'f', 2).toLatin1();
    if (!exportExcel) {
      result.append("<tr><td><img src=\""
                    + (eventType? eventType->Icon.toUtf8(): "") + "\"></td><td>"
                    + stateText + "</td><td>"
                    + detectorText + "</td><td>"
                    + valueText + "</td></tr>\n");
    } else {
      result.append(stateText + ";" + detectorText + ";" + valueText + "\n");
    }
  }

  if (!exportExcel) {
    result.append("</table>\n");
  }

  return true;
}

bool PortalHandler::AddMaps(int iodId, const QList<int>& iodIds, const QDateTime& ts1, int periodh, const QList<QDateTime>& ts4, QByteArray& result)
{
  result.append("<table align=\"left\">\n"
                "<tr style=\"height:30px\">");
  result.append(QObject::tr("<th>Time</th><th>Map</th>").toUtf8());
  for (int i = 0; i < iodIds.size(); i++) {
    result.append(QObject::tr("<th>Time ").toUtf8() + QByteArray::number(i+1) + QObject::tr("</th><th>Map ").toUtf8() + QByteArray::number(i+1) + "</th>");
  }
  result.append("</tr><tr><td></td><td></td></tr>\n");

  QVector<qint64> mapsBase;
  QVector<QVector<qint64> > mapsVs;
  bool ok = GetPeriodMaps(iodId, ts1, periodh, mapsBase);
  if (!ok) {
    return false;
  }
  for (int i = 0; i < iodIds.size(); i++) {
    mapsVs.append(QVector<qint64>());
    bool ok = GetPeriodMaps(iodIds[i], ts4[i], periodh, mapsVs.back());
    if (!ok) {
      return false;
    }
  }

  int hours = 24;
  QString timeFormat("dd(ddd)-MMM");
  QLocale locale = mIsRu? QLocale(QLocale::Russian, QLocale::RussianFederation): QLocale(QLocale::English, QLocale::UnitedStates);
  for (int i = 0; i < mapsBase.size(); i++) {
    QDateTime ts = ts1.addSecs(i * hours * 3600 - mUtc * 60);
    if (mapsBase[i] == 0) {
      bool skipLine = true;
      for (int j = 0; j < mapsVs.size(); j++) {
        if (mapsVs[j][i] > 0) {
          skipLine = false;
          break;
        }
      }
      if (skipLine) {
        continue;
      }
    }

    QByteArray tsText = locale.toString(ts, timeFormat).toUtf8();
    result.append("<tr><td>" + tsText + "</td>");
    if (mapsBase[i]) {
      QByteArray fileId = QByteArray::number(mapsBase[i]);
      result.append("<td><img src=\"file_" + fileId + ".jpg\"></td>");
    } else {
      result.append("<td></td>");
    }

    for (int j = 0; j < mapsVs.size(); j++) {
      QDateTime ts = ts4[j].addSecs(i * hours * 3600 - mUtc * 60);
      QByteArray tsText = locale.toString(ts, timeFormat).toUtf8();
      result.append("<td>" + tsText + "</td>");
      if (mapsVs[j][i]) {
        QByteArray fileId = QByteArray::number(mapsVs[j][i]);
        result.append("<td><img src=\"file_" + fileId + ".jpg\"></td>");
      } else {
        result.append("<td></td>");
      }
    }
    result.append("</tr>\n");
  }

  result.append("</table>\n");
  return true;
}

bool PortalHandler::GetPeriodLogs(int iodId, const QDateTime& ts1, int periodh, int hours, QVector<qreal>& values)
{
  if (iodId >= 0 && iodId < mLogDetectors.size()) {
    iodId = mLogDetectors[iodId].Id;
  } else {
    return true;
  }

  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT h.triggered_hour at TIME ZONE 'UTC', h.value"
                     " FROM event_log_hours h"
                     " INNER JOIN event e ON e._id = h._event"
                     " WHERE e._object = :obj AND"
                     " triggered_hour >= :ts1 AND triggered_hour <= :ts2"
                     " ORDER BY triggered_hour;"));
  q->bindValue(":ts1", ts1);
  q->bindValue(":ts2", ts1.addSecs(periodh * 3600));
  q->bindValue(":obj", iodId);

  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  QMap<QDateTime, int> stats;
  while (q->next()) {
    QDateTime ts = q->value(0).toDateTime();
    ts.setOffsetFromUtc(0);
    int value = q->value(1).toReal();

    stats[ts] = qMax(stats.value(ts, 0), value);
  }

  values.fill(0, periodh / hours);
  for (auto itr = stats.begin(); itr != stats.end(); itr++) {
    const QDateTime& ts = itr.key();
    int count = itr.value();

    int len = ts1.secsTo(ts);
    if (len >= 0) {
      int part = len / (3600 * hours);
      if (part >= 0 && part < values.size()) {
        values[part] += count;
      }
    } else {
      LOG_WARNING_ONCE(QString("Timezones invalid behaviour (from: %1, ts: %2)").arg(ts1.toString()).arg(ts.toString()));
    }
  }
  return true;
}

bool PortalHandler::GetPeriodStatLogs(int iodId, const QDateTime& ts1, int periodh, int hours, QVector<qreal>& values)
{
  if (iodId >= 0 && iodId < mStatDetectors.size()) {
    iodId = mStatDetectors[iodId].Id;
  } else {
    return true;
  }

  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT h.triggered_hour at TIME ZONE 'UTC', h.value, h.period"
                     " FROM event_stat_hours h"
                     " INNER JOIN event e ON e._id = h._event"
                     " WHERE e._object = :obj AND"
                     " triggered_hour >= :ts1 AND triggered_hour <= :ts2"
                     " ORDER BY triggered_hour;"));
  q->bindValue(":ts1", ts1);
  q->bindValue(":ts2", ts1.addSecs(periodh * 3600));
  q->bindValue(":obj", iodId);

  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  QMap<QDateTime, QPair<qreal, qreal> > stats;
  while (q->next()) {
    QDateTime ts = q->value(0).toDateTime();
    ts.setOffsetFromUtc(0);
    qreal value = q->value(1).toReal();
    qreal period = q->value(2).toReal();

    stats[ts] = qMakePair(value, period);
  }

  QVector<QPair<qreal, qreal> > sum;
  sum.fill(qMakePair(0, 0), periodh / hours);
  for (auto itr = stats.begin(); itr != stats.end(); itr++) {
    const QDateTime& ts = itr.key();
    const QPair<qreal, qreal>& statPair = itr.value();

    int len = ts1.secsTo(ts);
    if (len >= 0) {
      int part = len / (3600 * hours);
      if (part >= 0 && part < sum.size()) {
        sum[part].first  += statPair.first;
        sum[part].second += statPair.second;
      }
    } else {
      LOG_WARNING_ONCE(QString("Timezones invalid behaviour (from: %1, ts: %2)").arg(ts1.toString()).arg(ts.toString()));
    }
  }
  values.resize(sum.size());
  for (int i = 0; i < sum.size(); i++) {
    const QPair<qreal, qreal>& sumPair = sum.at(i);
    values[i] = (sumPair.second > kMinStatPeriodMs)? sumPair.first / sumPair.second: 0;
  }
  return true;
}

bool PortalHandler::GetPeriodMaps(int iodId, const QDateTime& ts1, int periodh, QVector<qint64>& values)
{
  if (iodId >= 0 && iodId < mStatMaps.size()) {
    iodId = mStatMaps[iodId].Id;
  } else {
    return true;
  }

  auto q = mDb->MakeQuery();
  q->prepare(QString("SELECT l.day at TIME ZONE 'UTC', l._fimage FROM va_stat_days l"
                     " JOIN va_stat s ON s._id = l._vstat"
                     " WHERE s._object = :obj AND s._vstype = :type"
                     " AND l.day >= :ts1 AND l.day <= :ts2"
                     " ORDER BY l.day;"));
  q->bindValue(":ts1", ts1);
  q->bindValue(":ts2", ts1.addSecs(periodh * 3600));
  q->bindValue(":obj", iodId);
  q->bindValue(":type", mSelectedMap);

  if (!mDb->ExecuteQuery(q)) {
    return false;
  }

  int hours = 24;
  values.fill(0, periodh / hours);
  while (q->next()) {
    QDateTime ts = q->value(0).toDateTime();
    ts.setOffsetFromUtc(0);
    int len = ts1.secsTo(ts);
    qint64 value = q->value(1).toLongLong();

    if (len >= 0) {
      int part = len / (3600 * hours);
      if (part >= 0 && part < values.size()) {
        values[part] = value;
      }
    }
  }
  return true;
}

void PortalHandler::GetCamCombo(QByteArray& combo)
{
  for (int i = 0; i < mCameras.size(); i++) {
    combo.append(QByteArray("<option value=\"") + QByteArray::number(i) + "\">" + QObject::tr("Camera %1").arg(i + 1).toUtf8() + "</option>\n");
  }
}

void PortalHandler::GetIodCombo(QByteArray& combo)
{
  for (int i = 0; i < mLogDetectors.size(); i++) {
    const DetectorInfo& info = mLogDetectors.at(i);
    if (info.Flag & 1) {
      combo.append(QByteArray("<option value=\"") + QByteArray::number(i) + "\">" + QObject::tr("Detector %1").arg(i + 1).toUtf8() + "</option>\n");
    }
  }
}

void PortalHandler::GetStateCombo(QByteArray& combo)
{
  for (int i = 0; i < mStatDetectors.size(); i++) {
    const DetectorInfo& info = mStatDetectors.at(i);
    if (info.Flag & 2) {
      combo.append(QByteArray("<option value=\"") + QByteArray::number(i) + "\">" + QObject::tr("Detector %1").arg(i + 1).toUtf8() + "</option>\n");
    }
  }
}

void PortalHandler::GetMapCombo(QByteArray& combo)
{
  for (int i = 0; i < mStatMaps.size(); i++) {
    combo.append(QByteArray("<option value=\"") + QByteArray::number(i) + "\">" + QObject::tr("Detector %1").arg(i + 1).toUtf8() + "</option>\n");
  }
}

bool PortalHandler::OpenDb()
{
  if (mDb) {
    return true;
  }

  mDb = DbS(new Db());
  if (!mDb->OpenDefault()) {
    return false;
  }
  if (!mDb->Connect()) {
    return false;
  }
  return true;
}

void PortalHandler::SelectLanguage()
{
  const QByteArray& langInfo = Header("Accept-Language");
  int posRu = langInfo.indexOf("ru");
  int posEn = langInfo.indexOf("en");
  if (posRu >= 0 && (posRu < posEn || posEn < 0)) {
    ApplyLanguage(true);
  } else {
    ApplyLanguage(false);
  }
}

bool PortalHandler::TakeLanguage(QString& path)
{
  if (path.startsWith("/en")) {
    ApplyLanguage(false);
    path = path.mid(3);
    return true;
  } else if (path.startsWith("/ru")) {
    ApplyLanguage(true);
    path = path.mid(3);
    return true;
  }
  return false;
}

void PortalHandler::ApplyLanguage(bool isRu)
{
  mIsRu = isRu;
  if (mIsRu) {
    mLanguage = "ru";
    if (!mRuTranslator) {
      mRuTranslator = new QTranslator();
      mRuTranslator->load(":/Tr/Portal_ru.qm");
    }
    QCoreApplication::instance()->installTranslator(mRuTranslator);
  } else {
    mLanguage = "en";
    if (mRuTranslator) {
      QCoreApplication::instance()->removeTranslator(mRuTranslator);
    }
  }
}

QDateTime PortalHandler::ReadDateTimeParam(const QByteArray& text)
{
  QString textNormal = QString::fromLatin1(QByteArray::fromPercentEncoding(text));
  QDateTime ts = QDateTime::fromString(textNormal, kTimeFormatUrl);
  ts.setOffsetFromUtc(0);
  return ts;
}


PortalHandler::PortalHandler(Portal* _Portal)
  : mPortal(_Portal)
  , mIsRu(false), mEnTranslator(nullptr), mRuTranslator(nullptr)
{
}

PortalHandler::~PortalHandler()
{
  if (mEnTranslator) {
    delete mEnTranslator;
  }
  if (mRuTranslator) {
    delete mRuTranslator;
  }
}
