#pragma once

#include <QDateTime>

#include <Lib/Backup/BackupA.h>


DefineClassS(BackupY);

class BackupY: public BackupA
{
protected:
  /*override */virtual int GetVersion() Q_DECL_OVERRIDE;
  /*override */virtual void GetDaemons(QStringList& daemonsNames, QStringList& daemonsExes) Q_DECL_OVERRIDE;
  /*override */virtual void GetStaticTables(int version, QStringList& tables) Q_DECL_OVERRIDE;
  /*override */virtual void GetLiveTables(int version, QStringList& tables) Q_DECL_OVERRIDE;
//  /*override */virtual bool AfterBackup() Q_DECL_OVERRIDE;
  /*override */virtual bool AfterRestore() Q_DECL_OVERRIDE;

public:
  BackupY(const Db& _Db);
  /*override */virtual ~BackupY();
};

