#include <QSettings>

#include <Lib/Include/QtAppCon.h>
#include <Lib/Db/Db.h>
#include <Lib/Dispatcher/Overseer.h>
#include <Local/ModuleNames.h>

#include "BackupY.h"


int qmain(int argc, char* argv[])
{
  DbS db;
  OverseerS overseer;
  if (int ret = Overseer::ParseMainWithDb(kServerDaemon, argc, argv, overseer, db)) {
    return ret;
  }

  BackupYS backup(new BackupY(*db));
  if (!overseer->Params().isEmpty()) {
    bool ok = backup->Restore(overseer->Params());
    return ok? 0: -1;
  }
  overseer->RegisterWorker(backup);

  return overseer->Run();
}

