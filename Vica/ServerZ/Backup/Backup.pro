!include(../Common.pri) {
  error(Could not find the Common.pri file!)
}


QT += sql network

SOURCES += \
    Main.cpp \
    BackupY.cpp

HEADERS += \
    BackupY.h

DEPEND_LIBS = \
    Backup \
    Updater \
    Dispatcher \
    Ctrl \
    Db \
    Settings \
    Log

!include($$PRI_DIR/Dependencies.pri) {
  error(Could not find the Dependencies.pri file!)
}

TARGET = $${APP_PREFIX}_bak$$APP_EXTRA_EXTANTION

