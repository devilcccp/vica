#include <Lib/Common/Var.h>
#include <Lib/Settings/FileSettings.h>
#include <Lib/Log/Log.h>
#include <Local/ModuleNames.h>

#include "BackupY.h"


int BackupY::GetVersion()
{
  return 1;
}

void BackupY::GetDaemons(QStringList& daemonsNames, QStringList& daemonsExes)
{
  daemonsNames = DAEMONS_NAMES_LIST;
  daemonsExes  = DAEMONS_EXE_LIST;
}

void BackupY::GetStaticTables(int version, QStringList& tables)
{
  if (version > 0) {
    tables << DefaultStaticTables() << EventStaticTables();
  }
}

void BackupY::GetLiveTables(int version, QStringList& tables)
{
  if (version > 0) {
    tables << DefaultLiveTables() << EventLiveTables();
  }
}

bool BackupY::AfterRestore()
{
  FileSettings serverSettings;
  if (serverSettings.Open(GetVarFile(kServerDaemon))) {
    serverSettings.SetValue("Updated", false);
    if (!serverSettings.Sync()) {
      Log.Warning(QString("Change server settings fail"));
    }
  } else {
    Log.Warning(QString("Open server settings fail"));
  }

  return true;
}


BackupY::BackupY(const Db& _Db)
  : BackupA(_Db)
{
}

BackupY::~BackupY()
{
}
