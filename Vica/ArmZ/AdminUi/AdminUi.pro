!include(../Common.pri) {
  error(Could not find the Common.pri file!)
}


QT += sql network core gui
greaterThan(QT_MAJOR_VERSION, 4) {
  QT += widgets
}

CONFIG(release, debug|release) {
  CONFIG -= console
}

SOURCES += \
    Main.cpp \
    MainWindowY.cpp

HEADERS += \
    MainWindowY.h

contains(DEFINES, VIDEO_ARM) {
DEPEND_LIBS = \
    VideoUi \
    DbUi \
    Updater \
    Ui \
    Net \
    Decoder \
    Settings \
    Db \
    Dispatcher \
    Ctrl \
    Log
} else {
DEFINES += NO_VIDEO_ARM

DEPEND_LIBS = \
    DbUi \
    Updater \
    Ui \
    Net \
    Settings \
    Db \
    Dispatcher \
    Ctrl \
    Log
}

!include($$PRI_DIR/Dependencies.pri) {
  error(Could not find the Dependencies.pri file!)
}

TARGET = $${APP_PREFIX}_admin$$APP_EXTRA_EXTANTION

RC_FILE = Resource.rc

RESOURCES += \
    Admin.qrc

