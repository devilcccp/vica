SRC_LOCAL=$$shell_path($$PWD/Vica/Local.pri)
DST_LOCAL=$$shell_path($$OUT_PWD/Local.pri)
system($$QMAKE_COPY_FILE $$SRC_LOCAL $$DST_LOCAL)


TEMPLATE = subdirs

# build must be last:
CONFIG += ordered

SUBDIRS += Lib
SUBDIRS += LibV
SUBDIRS += Vica/LibA

SUBDIRS += Test

SUBDIRS += ArmV
SUBDIRS += Vica/ArmZ

SUBDIRS += Server
SUBDIRS += ServerV
SUBDIRS += Vica/ServerZ
